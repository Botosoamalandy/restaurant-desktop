var lien='http://localhost:8087/';
var login = angular.module('login', []);
login.controller("loginController",function ($scope,$http){
	//initialisation des données
	$scope.messageModale='';
	$scope.affichageErreur=false;
	var tokenCuisine=localStorage.getItem('tokenCuisine');
	var testLogin=false;
	
	//condition
	if((tokenCuisine!=null || tokenCuisine!='') &&  testLogin==false){
		localStorage.removeItem('token');
		localStorage.removeItem('tokenCuisine');
		var testLogin=true;
    }
	$scope.login=function(){
		if($scope.nom!='' && $scope.nom!=null && $scope.motDePasse!='' && $scope.motDePasse!=null){
			var data={ nom : $scope.nom, motDePasse : $scope.motDePasse };		 
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'login',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	var tokenCuisine=response.data;
            	if(tokenCuisine.numeroToken!=''){
            		localStorage.setItem('tokenCuisine',tokenCuisine.numeroToken);
            		document.location.href="pageCuisine";
            	}else{
            		$scope.messageModale='Verifiez votre champs ou vous n\'avez pas l\' autorization requise';
            		$scope.affichageErreur=true;
            	}
                console.log(response.data);
            },function erreur(response){
            	$scope.messageModale='Une erreur c\'est produit';
            	$scope.affichageErreur=true;
				console.log(response);
            });
		}else{
			$scope.messageModale='Il y a un champs vide';
			$scope.affichageErreur=true;
			console.log('Il y a un champs vide');
		}
	}
});

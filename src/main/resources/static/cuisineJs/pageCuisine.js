var lien='http://localhost:8087/';
var app = angular.module('principales', ['ngRoute','datatables']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/pageCuisine', {
            templateUrl: 'listeCommandeCuisine',
            controller : 'listeCommandeCuisineController'
        }).when('/detailCommandeCuisine', {
            templateUrl: 'detailCommandeCuisine',
            controller : 'detailCommandeCuisineController'
        }).when('/listePreparationCuisine', {
            templateUrl: 'listePreparationCuisine',
            controller : 'listePreparationCuisineController'
        }).when('/listeCommandePretCuisine', {
            templateUrl: 'listeCommandePretCuisine',
            controller : 'listeCommandePretCuisineController'
        }).when('/platsCuisine', {
            templateUrl: 'platsCuisine',
            controller : 'platsCuisineController'
        }).otherwise({
            redirectTo : '/pageCuisine'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
});

app.controller('pageCuisineControlleur', function($scope, $route, $routeParams, $location){
	$scope.url_head ='menuCuisine';
});
app.controller('headController', function($scope){
	
});
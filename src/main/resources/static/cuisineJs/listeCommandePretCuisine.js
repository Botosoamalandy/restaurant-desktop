app.controller('listeCommandePretCuisineController',function ($scope,$http,$location){
	//initialisation des données 
	$scope.preparations=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getPreparation(){
		$http({
	        method:'GET',
	        url:lien+'listePreparationPret'
	      }).then(function success(response){
	    	  $scope.preparations=response.data;
	      },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
			console.log(response);
	      });
	}
	//initalisation des fonctions
	getPreparation();
	
	//ng-click
	$scope.redirectionDetailCommande=function(idCommande){
        $location.url('detailCommandeCuisine?idCommande='+idCommande);
    }
	$scope.supprimer=function(idPreparation){
		$http({
	        method:'GET',
	        url:lien+'supprimerPreparationCuisine?idPreparation='+idPreparation
	      }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
	    	getPreparation();
	      },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
			console.log(response);
	      });
	}
	
});
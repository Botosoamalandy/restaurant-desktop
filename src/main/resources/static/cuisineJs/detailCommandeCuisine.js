app.controller('detailCommandeCuisineController',function ($scope,$http,$location){
	//initialisation des données
	$scope.selecteTout=false;
	$scope.prixTotal=0;
	$scope.etat=0;
	$scope.commandeParTable=[];
	$scope.dataTemporaire={
			idCommande :'',
			numeroCommande : '',
			nombreDePlats : 0,
			numeroTables : '',
			idUtilisateur :''
	};
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction 
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getEtatPreparation(idCommandes){
	    $http({
	        method:'GET',
	        url:lien+'etatPreparation?idCommande='+idCommandes
	      }).then(function success(response){
	    	  $scope.etat=response.data.etat;
	      },function erreur(response){
	        	$scope.messageModale='Il y a une erreur';
	        	$scope.openModale();
	        	console.log(response);
	      });
	}
	
	var paramametres=window.location.search.slice(1,window.location.search.length);
    var first=paramametres.split("&");
	$scope.message='page detail commande';
	if(first.length==1){
		var valuesParametre=paramametres.split('=');
		var valeur=valuesParametre[1];
		$scope.message=valeur;
		if(valeur!=null && valeur!=''){
			$http({
				method:'GET',
				url:lien+'commandeParTableByIdCommande?idCommande='+valeur
			}).then(function success(response){
				$scope.commandeParTable=response.data;
				var size=response.data.length;
				$scope.dataTemporaire.nombreDePlats=size;
				for (let index = 0; index < size; index++) {
					getEtatPreparation(response.data[index].idCommande);
					$scope.dataTemporaire.idCommande=response.data[index].idCommande;
					$scope.dataTemporaire.numeroCommande=response.data[index].numeroCommande;
					$scope.dataTemporaire.numeroTables=response.data[index].numeroTables;
					$scope.dataTemporaire.idUtilisateur=response.data[index].idUtilisateur;
					$scope.prixTotal=$scope.prixTotal+(response.data[index].nombreDeProduit*response.data[index].prixUnitaire);
				}
			},function erreur(response){
	        	$scope.messageModale='Il y a une erreur';
	        	$scope.openModale();
	        	console.log(response);
			});
		}else{
        	$scope.messageModale='cette commande n\'existe pas';
        	$scope.openModale();
		}
	}else{
    	$scope.messageModale='cette commande n\'existe pas';
    	$scope.openModale();
	}
	
	function verificationQuantiterPlatsSiEquale(valeurInput,valeurBase){//$scope.produitInsuffisant=function()
		var valeurI=parseInt(valeurInput);var valeurB=parseInt(valeurBase);
		if(valeurI==valeurB){
			return true;
		}else if(valeurI>valeurB){
			return true;
		}
		return false;
	}
	
	function checkBoxCheckedOrNo(valeur){//$scope.selectTout=function()
		var size=$scope.commandeParTable.length;
		for (var i = 0; i < size; i++) {
			var idElement='check'+$scope.commandeParTable[i].idProduit;
			document.getElementById(idElement).checked=valeur;
		}
	}
	
	function getInsertionInsuffisantCuisine(data){//produitInsuffisant=function()
		$http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'insertionInsuffisantCuisine',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
	}
	//ng-click
	$scope.insertionPreparation=function(){
		if($scope.dataTemporaire.idCommande!=null && $scope.dataTemporaire.idCommande!='' && $scope.dataTemporaire.numeroCommande!=null && $scope.dataTemporaire.numeroCommande!='' && $scope.dataTemporaire.nombreDePlats>0){
			$http({
	            method: 'POST',
	            data:JSON.stringify($scope.dataTemporaire),
	            url: lien+'insertionPreparation',
	            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
	        }).then(function success(response){
	        	$scope.messageModale=''+response.data.message;
	        	$scope.openModale();
	            getEtatPreparation($scope.dataTemporaire.idCommande);
	        },function erreur(response){
	        	$scope.messageModale='Il y a une erreur';
	        	$scope.openModale();
	        	console.log(response);
	        });
			
		}else{
        	$scope.messageModale='Il n\'a pas de plats à preparé';
        	$scope.openModale();
		}	
	}
	$scope.selectTout=function(){
		if($scope.selecteTout==false){
			checkBoxCheckedOrNo(true);
			$scope.selecteTout=true;
		}else{
			checkBoxCheckedOrNo(false);
			$scope.selecteTout=false;
		}
	}
	$scope.produitInsuffisant=function(){
		//donnée
		var idTousLesIdProduit='';
		var test=false;
		//------//
		var size=$scope.commandeParTable.length;
		for (var i = 0; i < size; i++) {
			var idElement='check'+$scope.commandeParTable[i].idProduit;
			var idElementProduit='nombre'+$scope.commandeParTable[i].idProduit;
			var valeur=document.getElementById(idElementProduit).value;
			var check=document.getElementById(idElement).checked;
			console.log(idElement);
			if(check==true && verificationQuantiterPlatsSiEquale(valeur,''+$scope.commandeParTable[i].nombreDeProduit)==false){
				idTousLesIdProduit=idTousLesIdProduit+(valeur+'£'+$scope.commandeParTable[i].idProduit)+'#';
				test=true;
			}else if(check==true && verificationQuantiterPlatsSiEquale(valeur,''+$scope.commandeParTable[i].nombreDeProduit)==true){
	        	$scope.messageModale='Votre saisi est egale ou superieux au quantite. Veuillez resaisir : '+$scope.commandeParTable[i].nomProduit;
	        	$scope.openModale();
				test=false;
				break;
			}
		}
		
		if(test==true && idTousLesIdProduit!='' && $scope.dataTemporaire.idCommande!=null && $scope.dataTemporaire.idUtilisateur!=null && $scope.dataTemporaire.numeroCommande!=null){
			var data={
				idProduit : idTousLesIdProduit,
				idCommande : $scope.dataTemporaire.idCommande,
				idUtilisateur : $scope.dataTemporaire.idUtilisateur,
				numeroCommande : $scope.dataTemporaire.numeroCommande
			}
			getInsertionInsuffisantCuisine(data);
			$location.url('pageCuisine');
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
});
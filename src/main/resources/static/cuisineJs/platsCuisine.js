app.controller('platsCuisineController',function ($scope,$http,$location){
	//initialisation des données 
	$scope.plats=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getPlats(){
		$http({
	        method:'GET',
	        url:lien+'produit'
	      }).then(function success(response){
	    	  $scope.plats=response.data;
	      },function erreur(response){
	        	$scope.messageModale='Une erreur c\'est produit';
	        	$scope.openModale();
				console.log(response);
	      });
	}
	
	function setProduit(data){
		$http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'updateQuantiteEtDescriptionProduitService',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
            getPlats();
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
			console.log(response);
        });
	}
	//initialisation des fonctions
	getPlats();
	
	$scope.modificationQuanitePlats=function(plats){
		var idQuantita='quantite'+plats.idProduit;
		var idDescription='description'+plats.idProduit;
		var quantite=document.getElementById(idQuantita).value;
		var description=document.getElementById(idDescription).value;
		if(quantite!=null && quantite!='' && quantite!=plats.quantite && description!=null){
			if(description==''){
				description='Description vide';
			}
			var data={
				quantite : quantite,
				idProduit : plats.idProduit,
				description : description
			};
			setProduit(data);
		}else{
        	$scope.messageModale='Verifiez votre champs si il n\'y a pas de changement ou un champs vide';
        	$scope.openModale();
		}
	}
	$scope.actualiserLaColone=function(plats){
		var idQuantita='quantite'+plats.idProduit;
		var idDescription='description'+plats.idProduit;
		document.getElementById(idQuantita).value=plats.quantite;
		document.getElementById(idDescription).value=plats.descriptions;
	}
	
});
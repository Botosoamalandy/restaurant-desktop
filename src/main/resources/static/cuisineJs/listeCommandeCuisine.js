app.controller('listeCommandeCuisineController',function ($scope,$http,$location){
	//initialisation des données
	$scope.commandes=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getCommandes(){
	    $http({
	        method:'GET',
	        url:lien+'commandesCompletNonValiderTemporaire'
	      }).then(function success(response){
	        $scope.commandes=response.data;
	      },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
			console.log(response);
	      });
	}
	
	//initialisation des fonctions
	getCommandes();
	
	//ng-click
	$scope.redirectionDetailCommande=function(idCommande){
        $location.url('detailCommandeCuisine?idCommande='+idCommande);
    }
	$scope.preparationCommande=function(commande){
		if(commande.numeroOriginale>0){
			var data={
				idCommande : commande.idCommande,
				numeroCommande : commande.numeroCommande,
				nombreDePlats : commande.numeroOriginale, //quanitite plats commande
				numeroTables : commande.numeroTables
			};
			$http({
	            method: 'POST',
	            data:JSON.stringify(data),
	            url: lien+'insertionPreparation',
	            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
	        }).then(function success(response){
	        	$scope.messageModale=''+response.data.message;
	        	$scope.openModale();
	            getCommandes();
	        },function erreur(response){
	        	$scope.messageModale='Une erreur c\'est produit';
	        	$scope.openModale();
				console.log(response);
	        });
			
		}else{
        	$scope.messageModale='Il n\' y a pas de plats à preparé';
        	$scope.openModale();
		}
	}
	
});
	var valeur=false;
	function getVerificationLenghtWords(password){
	    if(password.length<8){
	        return false;
	    }
	    return true;
	}
	function getIfNumberExist(password){
	    if(password.search(/[0123456789]/)=== -1){
	        return false;
	    }
	    return true;
	}
	function getIfAlphabSmallWordExist(password){
	    if(password.search(/[a-z]/)==-1){
	        return false;
	    }
	    return true;
	}
	function getIfAlphabExist(password){
	    if(password.search(/[A-Z]/)==-1){
	        return false;
	    }
	    return true;
	}
	function getIfWordsSpecialExist(password){
	    if(password.search(/[\[~!@#|\"$%;^'\\[=&(:?`/<>.,)£ù*§\]]/)==-1){
	        return false;
	    }
	    return true;
	}
	function assembleLesLettre(valeur,mot){
	    if(valeur==false){
	        return mot;
	    }
	    return '';
	}
	function verificationPassword(){
	    var passwords=document.getElementById('password').value;
	    let texts1='Aux moins 8 caractère';let texts2=',un majuscule';let texts3=',une minuscule';let texts4=',un chiffre';let texts5=',un caractère speciaux';
	    if(getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
	        document.getElementById('textValidation').innerHTML='mot de passe correct';
	        document.getElementById('textValidation').style.color="green";
	    }else{
	        document.getElementById('textValidation').style.color="gray";
	        document.getElementById('textValidation').innerHTML=''+assembleLesLettre(getVerificationLenghtWords(passwords),texts1)+assembleLesLettre(getIfAlphabExist(passwords),texts2)+
	        assembleLesLettre(getIfAlphabSmallWordExist(passwords),texts3)+assembleLesLettre(getIfNumberExist(passwords),texts4)+
	        assembleLesLettre(getIfWordsSpecialExist(passwords),texts5);
	    }
	}
	function confirmationPassword(){
	    var passwords=document.getElementById('password').value;var confirmer=document.getElementById('confirmation').value;
	    if(passwords==confirmer && getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
	        document.getElementById('textConfirmation').innerHTML='cofirmé';
	        document.getElementById('textConfirmation').style.color="green";
	        document.getElementById('bouton').disabled=false;valeur=true;
	    }else{
	        document.getElementById('textConfirmation').innerHTML='Non confirmer';
	        document.getElementById('textConfirmation').style.color="gray";
	        document.getElementById('bouton').disabled=true;valeur=false;
	    }
	}

var lien='http://localhost:8087/';
app.controller('ajoutUtilisateurBackController',function ($scope,$http){
	//initialisation des données
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	
	//ng-click
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.inscription=function(){
		if(valeur==true){
			if($scope.nom!=null && $scope.nom!='' && $scope.etatUtilisateur!=null && $scope.etatUtilisateur!='' && $scope.motDePasse!=null && $scope.motDePasse!=''){
				var data={ nom : $scope.nom, etatUtilisateur : $scope.etatUtilisateur, motDePasse : $scope.motDePasse };
				$http({
					method: 'POST',
					data:JSON.stringify(data),
					url: lien+'inscripition',
					headers: {'Content-Type': 'application/json','Accept': 'application/json'}
				}).then(function success(response){
		        	$scope.messageModale=''+response.data.message;
		        	$scope.openModale();
				},function erreur(response){
		        	$scope.messageModale='Il y a une erreur';
		        	$scope.openModale();
					console.log(response);
				});
			}else{
	        	$scope.messageModale='Il y a une champs incorrecte';
	        	$scope.openModale();
			}
		}else{
        	$scope.messageModale='Verifiez votre champs';
        	$scope.openModale();
		}
	}

});
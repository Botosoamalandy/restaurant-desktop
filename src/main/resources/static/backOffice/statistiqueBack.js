var lien='http://localhost:8087/';
var apps = angular.module('app', []);
apps.controller('statistiqueBackControllers',function ($scope,$http,$location){
	//initialisation des données
	$scope.typeDeClassement='';
	$scope.classement='';
	$scope.tableau='Bar';
	$scope.annee='';
	$scope.mois='';
	$scope.annees=[];
	$scope.dataTableauStatistique=[];
	$scope.typeTableauStatistiques=[{ name : 'Bar'},{ name : 'Pie'},{ name : 'Point'}];
	$scope.classementTableaus=[{ name : 'Années'},{ name : 'Mois'},{ name : 'Jours'}];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
		
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function creerData(data){
		var size=data.length;
		var newData=[size];
		for (var i = 0; i < size; i++) {
			newData[i]={
				x: new Date(data[i].dates), y: data[i].valeur ,label : data[i].libelle
			};
		}
		return newData;
	}
	function creerDataSansLabel(data){
		var size=data.length;
		var newData=[];
		for (var i = 0; i < size; i++) {
			newData[i]={
				x: data[i].x, y: data[i].y
			};
		}
		return newData;
	}
	function creerDataAvecLabel(data){
		var size=data.length;
		var newData=[];
		for (var i = 0; i < size; i++) {
			newData[i]={
				label: data[i].label, y: data[i].y
			};
		}
		return newData;
	}
	function setData(data){
		if($scope.typeDeClassement=='Plats'){
			console.log(creerDataAvecLabel(data));
			return creerDataAvecLabel(data);
		}else{
			if($scope.tableau='Pie'){
				return data;
			}else{
				return creerDataSansLabel(data);
			}
		}
	}
	//pie
	function graphePie(datas,titre) {
		var dataTemporaire=setData(datas);
		var chart = new CanvasJS.Chart("chartContainer", {
			theme: "light2", // "light1", "light2", "dark1", "dark2"
			exportEnabled: true,
			animationEnabled: true,
			title: {
				text: titre
			},
			data: [{
				type: "pie",
				startAngle: 25,
				toolTipContent: "<b>{label}</b>: {y}%",
				showInLegend: "true",
				legendText: "{label}",
				indexLabelFontSize: 16,
				indexLabel: "{label} - {y}%",
				dataPoints: dataTemporaire
			}]
		});
		chart.render();
	}
	//Point
	function graphePoint(datas,titre) {
		var dataTemporaire=setData(datas);
		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			exportEnabled: true,
			title:{
				text: titre
			},
			axisX:{
				valueFormatString: "DD MMM",
				crosshair: {
					enabled: true,
					snapToDataPoint: true
				}
			},
			axisY: {
				title: "",
				includeZero: false
			},
			data: [{
				type: "area",
				xValueFormatString: "DD MMM",
				yValueFormatString: "Ariary##0.00",
				dataPoints: dataTemporaire
			}]
		});
		chart.render();
	}
	//bar
	function grapheBar(datas,titre) {
		var dataTemporaire=setData(datas);
		var chart = new CanvasJS.Chart("chartContainer", {
			animationEnabled: true,
			exportEnabled: true,
			theme: "light1", // "light1", "light2", "dark1", "dark2"
			title:{
				text: titre
			},
			data: [{
				type: "column", //change type to bar, line, area, pie, etc
				//indexLabel: "{y}", //Shows y value on all Data Points
				indexLabelFontColor: "#5A5757",
				indexLabelPlacement: "outside",
				dataPoints: dataTemporaire
			}]
		});
		chart.render();
	}
	 
	
	function getAnnees(){
		var data=[];var annee=2010;
		for (var i =0; i < 50; i++) {
			data[i]=annee+i;
		}
		return data;
	}
	function postAndGetInformation(data){
		$http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'tableauStatistique',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.dataTableauStatistique=creerData(response.data);
        	$scope.setTableau($scope.dataTableauStatistique,$scope.tableau);
        },function erreur(response){
			alert('Une erreur c\'est produit');
			console.log(response);
        });
	}
	//intialisation fonction
	$scope.annees=getAnnees();
	
	//window.onload=graphePie(data,"Test si ça marche");
	//window.onload=graphePoint(data,"Test si ça marche");
	window.onload=grapheBar($scope.dataTableauStatistique,"Tableau statistique ");
	
	//ng-click
	$scope.changerTableau=function(name){
		$scope.tableau=name;
		if(name=='Pie'){
			window.onload=graphePie($scope.dataTableauStatistique,"Tableau statistique");
		}else if(name=='Point'){
			window.onload=graphePoint($scope.dataTableauStatistique,"Tableau statistique");
		}else{
			window.onload=grapheBar($scope.dataTableauStatistique,"Tableau statistique");
		}
	}
	$scope.setTableau=function(data,name){
		$scope.tableau=name;
		if(name=='Pie'){
			window.onload=graphePie(data,"Tableau statistique");
		}else if(name=='Point'){
			window.onload=graphePoint(data,"Tableau statistique");
		}else{
			window.onload=grapheBar(data,"Tableau statistique");
		}
	}
	$scope.classementpar=function(valeur){
		if(valeur=='Années'){
			$scope.annee='';
			$scope.mois='';
		}
	}
	
	$scope.validerTableauStatistique=function(){
		if($scope.typeDeClassement!='' && $scope.classement=='Années'){
			var data={ typeDeClassement : $scope.typeDeClassement , classement : 'annee' , annee : '0' , mois : '0'};
			postAndGetInformation(data);
		}else if($scope.typeDeClassement!='' && $scope.classement=='Mois' && $scope.annee!=''){
			var data={ typeDeClassement : $scope.typeDeClassement , classement : 'mois' , annee : $scope.annee , mois : '0'};
			postAndGetInformation(data);
		}
		else if($scope.typeDeClassement!='' && $scope.classement=='Jours' && $scope.annee!='' && $scope.mois!=''){
			var data={ typeDeClassement : $scope.typeDeClassement , classement : 'jours' , annee : $scope.annee , mois : $scope.mois};
			postAndGetInformation(data);
		}else{
        	$scope.messageModale='Il y a une champs vide';
        	$scope.openModale();
		}
	}
	
	$scope.retour=function(){
		document.location.href="pageBackOffice";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
});
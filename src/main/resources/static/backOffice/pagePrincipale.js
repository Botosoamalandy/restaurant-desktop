var app = angular.module('principales', ['ngRoute','angularUtils.directives.dirPagination','datatables']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/pageBackOffice', {
            templateUrl: 'listeUtilisateurBack',
            controller : 'listeUtilisateurBackController'
        }).when('/ajoutUtilisateurBack', {
            templateUrl: 'ajoutUtilisateurBack',
            controller : 'ajoutUtilisateurBackController'
        }).when('/listeCommandeBack', {
            templateUrl: 'listeCommandeBack',
            controller : 'listeCommandeBackController'
        }).when('/detailCommandeBack', {
            templateUrl: 'detailCommandeBack',
            controller : 'detailCommandeBackController'
        }).when('/listePayementBack', {
            templateUrl: 'listePayementBack',
            controller : 'listePayementBackController'
        }).when('/listeFactureBack', {
            templateUrl: 'listeFactureBack',
            controller : 'listeFactureBackController'
        }).when('/factureBack', {
            templateUrl: 'factureBack',
            controller : 'factureBackController'
        }).otherwise({
            redirectTo : '/pageBackOffice'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
});

app.controller('principalesBackOfficeController', function($scope, $route, $routeParams, $location){
	$scope.url_head ='menuback';
});
app.controller('headController', function($scope,$location,$window){
	$scope.tableauStatistique=function(){
		$window.location.href="statistiqueBack";
	}
});
var lien='http://localhost:8087/';
app.controller('listeCommandeBackController',function ($scope,$http,$location){
	//initialisation des données
	$scope.commandes=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getCommandes(){
		$http({
            method:'GET',
            url:lien+'commandesCompletAdministrateur'
        }).then(function success(response){
            $scope.commandes=response.data;
            console.log(response.data);
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
	}
	
	//initialisation des fonctions
	getCommandes();
	
	//ng-click
	$scope.deleteCommande=function(idCommande){
		$http({
            method:'GET',
            url:lien+'SupprimerCommandeAdministrateur?idCommande='+idCommande
        }).then(function success(response){
        	getCommandes();
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
	}
	
	$scope.redirectionDetail=function(idCommande){
		$location.url('detailCommandeBack?idCommande='+idCommande);
	}
	
	
});
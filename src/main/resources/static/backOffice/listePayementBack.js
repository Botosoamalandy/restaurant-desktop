var lien='http://localhost:8087/';
app.controller('listePayementBackController',function ($scope,$http,$location){
	//initalisation des données
    $scope.payements=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");

    //fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	function getPayments() {
        $http({
            method:'GET',
            url:lien+'listePayementAdministrateur'
        }).then(function success(response){
        	$scope.payements=response.data;
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
    }
    //initialisation des fonctions
    getPayments();

    //ng-click
    $scope.suppression=function(idPayement){
    	$http({
            method:'GET',
            url:lien+'supprimerPayementAdministrateur?idPayement='+idPayement
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        	getPayments();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
    }
    
    $scope.voirDetail=function(idPayement){
    	$http({
            method:'GET',
            url:lien+'informationDUnPayement?idPayement='+idPayement
        }).then(function success(response){
        	var idCommande=''+response.data.message;
        	$location.url('detailCommandeBack?idCommande='+idCommande);
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
    }
});
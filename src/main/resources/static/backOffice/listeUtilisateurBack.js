var lien='http://localhost:8087/';
app.controller('listeUtilisateurBackController',function ($scope,$http){
	//initialisation des données
	$scope.utilisateurs=[];
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
	
	//fonction
	function getListeUtilisateur(){
		$http({
            method:'GET',
            url:lien+'listeUtilisateur'
        }).then(function success(response){
            $scope.utilisateurs=response.data;
        },function erreur(response){
          console.log(response);
        });
	}
	//initialisation des fonctions
	getListeUtilisateur();
	
	//ng-click
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.deleteUtilisateur=function(idUtilisateur){
		var token=localStorage.getItem('tokenBackOffice');
		$http({
            method:'GET',
            url:lien+'supprimerUtilisateur?idUtilisateur='+idUtilisateur+'&numeroToken='+token
        }).then(function success(response){
        	getListeUtilisateur();
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
	}
	
});
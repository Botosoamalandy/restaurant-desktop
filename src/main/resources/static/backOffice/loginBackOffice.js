var lien='http://localhost:8087/';
var login = angular.module('login', []);
login.controller("loginController",function ($scope,$http,$location){
	//initialisation des données
	$scope.nom='';
	$scope.motDePasse='';
	$scope.messageModale='';
	$scope.affichageErreur=false;
	var testLogin=1;
	var token=localStorage.getItem('token');
	var tokenBackOffice=localStorage.getItem('tokenBackOffice');
	
	//condition 
	if(testLogin==1 || token!=null || token!='' || tokenBackOffice!=null || tokenBackOffice!=''){
    	localStorage.removeItem('token');
    	localStorage.removeItem('tokenBackOffice');
    	testLogin=0;
	}
	
	//ng-click
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.login=function(){
		if($scope.nom!='' && $scope.nom!=null && $scope.motDePasse!='' && $scope.motDePasse!=null){
			var data={ nom : $scope.nom, motDePasse : $scope.motDePasse };		 
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'loginAmdinstrateur',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	var token=response.data;
            	if(token.numeroToken!=''){
            		localStorage.setItem('tokenBackOffice',token.numeroToken);
            		document.location.href="pageBackOffice";
            	}else{
    	        	$scope.messageModale='Verifiez votre champs ou vous n\'avez pas l\' autorization requise';
    	        	$scope.affichageErreur=true;
            	}
                console.log(response.data);
            },function erreur(response){
	        	$scope.messageModale='Il y a une erreur';
	        	$scope.affichageErreur=true;
				console.log(response);
            });
		}else{
        	$scope.messageModale='Il y a un champs vide';
        	$scope.affichageErreur=true;
        	console.log('Il y a un champs vide');
		}
	}
});

//mot de passe

	var valeur=false;
	function getVerificationLenghtWords(password){
	    if(password.length<8){
	        return false;
	    }
	    return true;
	}
	function getIfNumberExist(password){
	    if(password.search(/[0=9]/)=== -1){
	        return false;
	    }
	    return true;
	}
	function getIfAlphabSmallWordExist(password){
	    if(password.search(/[a-z]/)==-1){
	        return false;
	    }
	    return true;
	}
	function getIfAlphabExist(password){
	    if(password.search(/[A-Z]/)==-1){
	        return false;
	    }
	    return true;
	}
	function getIfWordsSpecialExist(password){
	    if(password.search(/[\[~!@#|\"$%;^'\\[=&(:?`/<>.,)£ù*§\]]/)==-1){
	        return false;
	    }
	    return true;
	}
	function assembleLesLettre(valeur,mot){
	    if(valeur==false){
	        return mot;
	    }
	    return '';
	}
	function verificationPassword(){
	    var passwords=document.getElementById('password').value;
	    let texts1='Aux moins 8 caractère';let texts2=',un majuscule';let texts3=',une minuscule';let texts4=',un chiffre';let texts5=',un caractère speciaux';
	    if(getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
	        document.getElementById('textValidation').innerHTML='Mot de passe correct';
	        document.getElementById('textValidation').style.color="green";
	    }else{
	        document.getElementById('textValidation').style.color="gray";
	        document.getElementById('textValidation').innerHTML=''+assembleLesLettre(getVerificationLenghtWords(passwords),texts1)+assembleLesLettre(getIfAlphabExist(passwords),texts2)+
	        assembleLesLettre(getIfAlphabSmallWordExist(passwords),texts3)+assembleLesLettre(getIfNumberExist(passwords),texts4)+
	        assembleLesLettre(getIfWordsSpecialExist(passwords),texts5);
	    }
	}
	function confirmationPassword(){
	    var passwords=document.getElementById('password').value;var confirmer=document.getElementById('confirmation').value;
	    if(passwords==confirmer && getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
	        document.getElementById('textConfirmation').innerHTML='Confirmé';
	        document.getElementById('textConfirmation').style.color="green";
	        document.getElementById('bouton').disabled=false;valeur=true;
	    }else{
	        document.getElementById('textConfirmation').innerHTML='Non confirmer';
	        document.getElementById('textConfirmation').style.color="gray";
	        if(!document.getElementById('checkName').checked){
	        	document.getElementById('bouton').disabled=true;
	        }
	        valeur=false;
	    }
	}
	
//-------------//

var lien='http://localhost:8087/';
app.controller('moncompteController',function ($scope,$http){
	//initialisation des données
	$scope.information={};
	$scope.boutonModification=false;
	$scope.labelle='Modification';
	$scope.numeroToken=localStorage.getItem('token');
	$scope.nom='';$scope.nouveauMotDePasse='';
	$scope.messageModale='';
	
	//fonctions
	function getInformationUtilisateur(){
		$http({
            method:'GET',
            url:lien+'informationUtilisateur?numeroToken='+$scope.numeroToken
        }).then(function success(response){
        	$scope.information=response.data;
        },function erreur(response){
          console.log(response);
        });
	}
	function updateUtilisateur(data){
		$http({
			method: 'POST',
			data:JSON.stringify(data),
			url: lien+'updateUtilisateur',
			headers: {'Content-Type': 'application/json','Accept': 'application/json'}
		}).then(function success(response){
			$scope.messageModale=''+response.data.message;
	    	$scope.openModale();
			getInformationUtilisateur();
		},function erreur(response){
			$scope.messageModale='Une erreur c\'est produit';
	    	$scope.openModale();
			console.log(response);
		});
	}
	
	//initialisation des fonctions
	getInformationUtilisateur();
	
	//ng-click
    var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.boutonModifier=function(){
		if($scope.boutonModification==true){
			$scope.boutonModification=false;
			$scope.labelle="Modification";
		}else{
			$scope.labelle='Annuler';
			$scope.boutonModification=true;
		}
	}
	$scope.modificationInformation=function(){
		var nom=document.getElementById('nomUtilisateur').value;
		var passwords=document.getElementById('password').value;
		var motDePasseActuel=document.getElementById('motDePasseActuel').value;
		var check=document.getElementById('checkName').checked;
		var confirmer=document.getElementById('confirmation').value
		if(check==true){
			var data ={ 
				nom : nom ,
				motDePasse : $scope.information.password ,
				motDePasseActuel : motDePasseActuel,
				numeroToken : $scope.numeroToken 
			}
			updateUtilisateur(data);
		}else if(nom!=null && nom!='' && passwords!=null && passwords!='' && confirmer==passwords){
			var data ={ 
					nom : nom ,
					motDePasse : passwords ,
					motDePasseActuel : motDePasseActuel ,
					numeroToken : $scope.numeroToken 
			}
			updateUtilisateur(data);
		}else if(confirmer!=passwords){
			$scope.messageModale='Confirmez votre mot de passe';
	    	$scope.openModale();
		}else{	
			$scope.messageModale='Une erreur c\'est produit';
	    	$scope.openModale();
		}
	}
	$scope.check=function(){
		var check=document.getElementById('checkName').checked;
		if(check==true){
			document.getElementById('bouton').disabled=false;
		}else if(check==false && valeur==false){
			document.getElementById('bouton').disabled=true;
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
});
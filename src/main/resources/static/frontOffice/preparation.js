var lien='http://localhost:8087/';
app.controller('preparationController',function ($scope,$http,$location){
	//initialisation des données
	$scope.preparations=[];
	$scope.messageModale='';
	
	//fonction
	function getPreparation(){
		$http({
	        method:'GET',
	        url:lien+'preparationNonSupprimer'
	    }).then(function success(response){
	        $scope.preparations=response.data;
	    },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
      	  	$scope.openModale();
      	  	console.log(response);
	    });
	}
	
	//initialisation des fonctions
	getPreparation();
	
	//ng-click
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.voirDetail=function(idCommande){
		$location.url('detailCommande?idCommande='+idCommande);
	}
	$scope.suppression=function(idPreparation){
		$http({
	        method:'GET',
	        url:lien+'supprimerPreparationCuisine?idPreparation='+idPreparation
	    }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
      	  	$scope.openModale();
	        getPreparation();
	    },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
      	  	$scope.openModale();
      	  	console.log(response);
	    });
	}
	
});
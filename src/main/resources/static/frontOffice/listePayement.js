var lien='http://localhost:8087/';
app.controller('listePayementController',function ($scope,$http,$location){
    //initalisation des données
    $scope.payements=[];
    $scope.messageModale='';

    //fonction
	function getPayments() {
        $http({
            method:'GET',
            url:lien+'tousLesPayement'
        }).then(function success(response){
        	$scope.payements=response.data;
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
          console.log(response);
        });
    }
    //initialisation des fonctions
    getPayments();

    //ng-click
    var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.annulation=function(idPayement){
    	$http({
            method:'GET',
            url:lien+'annulationPayementParCommandeOuParPayement?idPayement='+idPayement+'&typeDAnnulation=false'
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
	    	$scope.openModale();
        	getPayments();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
          console.log(response);
        });
    }
    
    $scope.suppression=function(idPayement){
    	$http({
            method:'GET',
            url:lien+'caissierSupprimerPayement?idPayement='+idPayement
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
	    	$scope.openModale();
        	getPayments();
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
          console.log(response);
        });
    }
    
    $scope.voirDetail=function(idPayement){
    	$http({
            method:'GET',
            url:lien+'informationDUnPayement?idPayement='+idPayement
        }).then(function success(response){
        	var idCommande=''+response.data.message;
        	$location.url('detailCommande?idCommande='+idCommande);
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
          console.log(response);
        });
    }
});
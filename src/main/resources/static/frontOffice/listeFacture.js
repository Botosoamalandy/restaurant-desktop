var lien='http://localhost:8087/';
app.controller('listeFactureController', function($scope,$http,$location){
	//initialisation des données
	$scope.tousLesFactures=[];
	$scope.messageModale='';
	
	//fonction
	function getTousLesFacture(){
		$http({
	        method:'GET',
	        url:lien+'tousLesFacture'
	    }).then(function success(response){
	        $scope.tousLesFactures=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getValeurEtat(valeur){
		if(valeur==11){
			return 'Payement';
		}
		return 'commande';
	}
	
	//initialisation
	getTousLesFacture();
	
	//ng-click
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.supprimerFacture=function(idFacture){
		$http({
	        method:'GET',
	        url:lien+'caissierSupprimerFacture?idFacture='+idFacture
	    }).then(function success(response){
	    	$scope.messageModale=''+response.data.message;
	    	$scope.openModale();
        	getTousLesFacture();
	    },function erreur(response){
	    	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
	    });
	}
    $scope.detailFacture=function(numeroCommande,etat){
        $http({
            method:'GET',
            url:lien+'idCommandeAPartNumeroCommande?numeroCommande='+numeroCommande
        }).then(function success(response){
            $location.url('facture?idcommande='+response.data.idCommande+'$'+getValeurEtat(etat));
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
    }
});
var lien='http://localhost:8087/';
app.controller('payementController',function ($scope,$http,$location){
  //initialisation des données
  $scope.commande='';
  $scope.etat='';
  $scope.prixTotal=0;
  $scope.montantPayer=0;
  $scope.argentRestant=0;
  $scope.argentResteClient=0;
  $scope.idCommandeTemporaire='';
  $scope.messageModale='';
  
  //fonction
  function listeCommande(){
	$http({
        method:'GET',
        url:lien+'commandeValider'
    }).then(function success(response){
        $scope.commandeValider=response.data;
    },function erreur(response){
      console.log(response);
      $scope.messageModale='Une erreur c\'est produit';
	  $scope.openModale();
    });
  }
  function detailCommande(idCommande){
    $http({
      method:'GET',
      url:lien+'informationPayementByIdCommande?idCommande='+idCommande
    }).then(function success(response){
      $scope.commande=''+response.data.commande;
      $scope.etat=''+response.data.etat;
      $scope.prixTotal=response.data.prixTotal;
      $scope.montantPayer=response.data.montantPayer;
      $scope.argentRestant=response.data.argentRestant;
      $scope.argentResteClient=response.data.argentResteClient;
      if(response.data.etatCommandes==11){
        document.getElementById('facture').disabled=false;
      }else{
        document.getElementById('facture').disabled=true;
      }
    },function erreur(response){
    	$scope.messageModale='Une erreur c\'est produit';
  	  	$scope.openModale();
  	  	console.log(response);
    });
  }
  //initialisation fonction
  listeCommande();
  document.getElementById('facture').disabled=false;
  //click select commande
  $scope.changementdeCommande=function(idCommande){
    detailCommande(idCommande);
  }

  //click button
  var modal=document.getElementById("myModal");
  $scope.openModale=function(){
      modal.style.display="block";
  }
  $scope.closeModal=function(){
      modal.style.display="none";
  }
  function insertionPayement(data){
    $http({
      method: 'POST',
      data:JSON.stringify(data),
      url: lien+'insertionPayement',
      headers: {'Content-Type': 'application/json','Accept': 'application/json'}
    }).then(function success(response){
        detailCommande($scope.commandeSelectionner);
        listeCommande();
        $scope.messageModale=''+response.data.message;
    	$scope.openModale();
    },function erreur(response){
    	$scope.messageModale='Une erreur c\'est produit';
  	  	$scope.openModale();
  	  	console.log(response);
    });
  }
  $scope.payement=function(){
    if($scope.commandeSelectionner!=null && $scope.montant!=null && $scope.montant!=''){
      $scope.idCommandeTemporaire=$scope.commandeSelectionner;
      var data={ idCommande : $scope.commandeSelectionner, montant : $scope.montant };	
      $http({
        method:'GET',
        url:lien+'verificationDedoublementPayement?idCommande='+$scope.commandeSelectionner+'&montant='+$scope.montant
      }).then(function success(response){
        var verification=''+response.data.message;
        if(verification=='true' && confirm('Validé le payement')){
          insertionPayement(data);
        }else{
          insertionPayement(data);
        }
      },function erreur(response){
    	  $scope.messageModale='Une erreur c\'est produit';
    	  $scope.openModale();
    	  console.log(response);
      });
    }else{
      	$scope.messageModale='Verifiez votre champs';
  		$scope.openModale();
    }
  }

  $scope.facture=function(){
    $location.url('facture?idcommande='+$scope.idCommandeTemporaire+'$Payement');
  }

});
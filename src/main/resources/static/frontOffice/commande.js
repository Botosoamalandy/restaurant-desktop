var lien='http://localhost:8087/';
app.controller("commandeController", function($scope,$http,$location,$interval) {
	//intialisation des variables
	$scope.choixSelecte='tous';
	$scope.messageModale='';
	$scope.commande=[];
	$scope.choix=[{ text : 'Tous',valeur : 'tous'},{ text : 'En cours', valeur : 'cours' },{ text : 'Validé',valeur : 'valider'},{ text : 'Non validé',valeur : 'nonvalider'},{ text : 'Mes commandes',valeur : 'mescommandes' }];
    //function
    function getUrlCommande(valeur){
    	if(valeur=='tous'){
        	url='commandesComplet';
        }else if(valeur=='cours'){
        	url='commandesCompletEnCours';
        }else if(valeur=='valider'){
        	url='commandesCompletValider';
        }else if(valeur=='nonvalider'){
        	url='commandesCompletNonValider';
        }else if(valeur=='mescommandes'){
        	var numeroToken=localStorage.getItem('token');
        	url='commandesCompletMesCommandes?numeroToken='+numeroToken;
        }
    	return url;
    } 
	function getCommande(valeur){
        $http({
            method:'GET',
            url:lien+getUrlCommande(valeur)
        }).then(function success(response){
            $scope.commande=response.data;
            console.log('data :',response.data);
        },function erreur(response){
        console.log(response);
        });
    }
     $interval(function(){
    	 getCommande($scope.choixSelecte);
     },2000);
	
	//initialisation des fonctions
    getCommande($scope.choixSelecte);
    
    //ng-click
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.selectionChoix=function(valeurs){
    	$scope.choixSelecte=valeurs;
    	getCommande($scope.choixSelecte);
    }
	$scope.sort=function(keyname){
		$scope.sortKey=keyname;
		$scope.reverse=$scope.reserve;
	}
    $scope.redirectionDetailCommande=function(idCommande){
        $location.url('detailCommande?idCommande='+idCommande);
    }
    $scope.deleteCommande=function(idCommande){
        var data={ idCommande : idCommande };
        $http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'insertionAnnulationCommande',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
            if(response.data.message!=null && response.data.message!=''){
            	$scope.messageModale=''+response.data.message;
            	$scope.openModale();
            }
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
            console.log(response);
        });
    }
    $scope.redirectionFacture=function(idCommande){
        $http({
            method:'GET',
            url:lien+'updateEtatcommandes?idCommande='+idCommande
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
        	console.log(response);
        });

        $location.url('facture?idcommande='+idCommande+'$commande');
    }
    $scope.modifierCommande=function(idCommande){
        $location.url('modificationCommande?idcommande='+idCommande);
    }
});

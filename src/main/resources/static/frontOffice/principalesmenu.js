var lien='http://localhost:8087/';
app.controller('principalesmenu', function($scope,$http,$location,$interval){
    //initialisation variable
    $scope.commandeParTable=[];
    $scope.notificationInsuffisant='';
    //initialisation des données
    localStorage.setItem('commandeChanged','false');
    localStorage.setItem('commandeTemporaire','false');
    localStorage.setItem('commandeValiderTemporaire','false');
    localStorage.setItem('notificationInsuffisant','0');
    //fonction
    function getNotif(){
    	try {
	      return parseInt(localStorage.getItem('notificationInsuffisant'));
	    } catch (error) {
	      return 0;
	    }
    }
    function getCommandeActuelWithStorage(){
        var valeur=localStorage.getItem('commandeActuel');
        if(valeur!=null && valeur!=''){
            return valeur;
        }else{
            return 'Veuilliez selectionnée une commande';
        }
    }
    function getCommandeActuel(){
        var valeur=localStorage.getItem('commandeActuel');
        if(valeur!=null && valeur!=''){
            return valeur;
        }else{
            return '';
        }
    }
    function getNotificationInsuffisant(){
    	$http({
            method:'GET',
            url:lien+'compteInsuffisantByNumeroToken?numeroToken='+localStorage.getItem('token')
        }).then(function success(response){
        	var count=getNotif();var countInBase=response.data.count;
        	if(count==countInBase){
        		$scope.notificationInsuffisant='';
        	}else if(countInBase==0){
        		$scope.notificationInsuffisant='Aucun';
        		localStorage.setItem('notificationInsuffisant','0');
        	}else{
        		localStorage.setItem('notificationInsuffisant',''+countInBase);
        		$scope.notificationInsuffisant='Nouveau';
        	}        	
        },function erreur(response){
      	  	console.log(response);
        });
    }
    $scope.commandeActuele=getCommandeActuelWithStorage();
    $scope.ajouterCommande=function(){
    	$scope.messageModale='Ajouter une nouvel commande';
  	  	$scope.openModale();
    }
    function getCommandeParTable() { 
        $http({
            method:'GET',
            url:lien+'commandeParTable?numeroCommande='+getCommandeActuel()
        }).then(function success(response){
            $scope.commandeParTable=response.data;
            var size=response.data.length;
            $scope.valeurTotal=0;
            for (let index = 0; index < size; index++) {
                $scope.valeurTotal=$scope.valeurTotal+(response.data[index].nombreDeProduit*response.data[index].prixUnitaire);
            }
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
      	  	$scope.openModale();
      	  	console.log(response);
        }); 
    }
    //initialisation function 
    getCommandeParTable();
    getNotificationInsuffisant();
    //ng-click
    $scope.validerCommande=function(){
        $http({
            method:'GET',
            url:lien+'updateFermetureCommandes?numeroCommande='+getCommandeActuel()
        }).then(function success(response){
        	alert(''+response.data.message);
        },function erreur(response){
        	alert('Une erreur c\'est produit');
      	  	console.log(response);
        }); 
        localStorage.setItem('commandeValiderTemporaire','true');
    }
    function verificationDonnee(valeur){
        if(valeur=='' || valeur==null){
            localStorage.setItem(''+valeur,'false');
        }
    }
    $scope.shopping=function(){
    	$scope.commandeActuele=getCommandeActuelWithStorage();
        getCommandeParTable();
    }
    $scope.deleteAllPlatsCommandes=function(){
    	var valeur=localStorage.getItem('commandeActuel');
    	if(valeur!=null && valeur!=''){
    		$http({
                method:'GET',
                url:lien+'deleteCommandeTableByNumeroCommande?numeroCommande='+valeur
            }).then(function success(response){
            	alert(response.data.message);
            	getCommandeParTable();
            },function erreur(response){
            	alert('Une erreur c\'est produit');
          	  	console.log(response);
            });
    	}else{
    		alert('Veuillez selectionné une commande');
    	}
    }
    
    //actualisation tous les 3 secondes
    $interval(function(){
    	getNotificationInsuffisant();
    },3000);
    /*$interval(function(){
        var commandeChanged=localStorage.getItem('commandeChanged');
        var ajoutCommandeTemporaire=localStorage.getItem('commandeTemporaire');
        var commandeValiderTemporaire=localStorage.getItem('commandeValiderTemporaire');
        verificationDonnee(commandeChanged);verificationDonnee(ajoutCommandeTemporaire);verificationDonnee(commandeValiderTemporaire);
        if(commandeChanged=='true' || ajoutCommandeTemporaire=='true' || commandeValiderTemporaire=='true'){
            $scope.commandeActuele=getCommandeActuelWithStorage();
            verificationSiCommandeFermerOuPas();
            getCommandeParTable();
            if(commandeChanged=='true'){
            	alert('commande :'+$scope.commandeActuele);
                //localStorage.setItem('commandeChanged','false');
            }
            if(ajoutCommandeTemporaire=='true'){
                localStorage.setItem('commandeTemporaire','false');
            }
            if(commandeValiderTemporaire=='true'){
                localStorage.setItem('commandeValiderTemporaire','false');
            }
        }
    },3000);*/
    
});
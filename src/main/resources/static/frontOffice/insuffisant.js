var lien='http://localhost:8087/';
app.controller('insuffisantController',function ($scope,$http,$location){
	//initialisation des données
	$scope.insuffisants=[];
	$scope.messageModale='';
	
	//fonction 
	function getInsuffisant(){
    	var numeroToken=localStorage.getItem('token');
		$http({
			method:'GET',
			url:lien+'listeInsuffisantByNumeroToken?numeroToken='+numeroToken
		}).then(function success(response){
			$scope.insuffisants=response.data;
		},function erreur(response){
		  console.log(response);
		});
	}
	
	//initialisation des fonctions
	getInsuffisant();
	
	// ng-click
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.supprimerCommandeTable=function(idCommande,idInsuffisant){
		$http({
			method:'GET',
			url:lien+'supprimerInsuffisant?idCommande='+idCommande+'&idInsuffisant='+idInsuffisant
		}).then(function success(response){
			$scope.messageModale=''+response.data.message;
        	$scope.openModale();
			getInsuffisant();
		},function erreur(response){
			$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
		});
	}
	$scope.modification=function(insuffisant){
		$http({
			method:'GET',
			url:lien+'supprimerInsuffisantEtModifierCommande?idInsuffisant='+insuffisant.idInsuffisant+'&idCommande='+insuffisant.idCommande
		}).then(function success(response){
			$scope.messageModale=''+response.data.message;
        	$scope.openModale();
			$location.url('modificationCommande?idcommande='+insuffisant.idCommande);
		},function erreur(response){
			$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
		});
	}
	
	
});
var lien='http://localhost:8087/';
app.controller('accueilController',function ($scope,$http){
	//initialisation des données
	var token=localStorage.getItem('token');
	$scope.limite=false;
	$scope.limiteProduit=8;
	$scope.commandeActuel=[];
	$scope.categorie=[];
	$scope.produit=[];
	$scope.messageModale='';
	//function 
	function getCommandeActuel(){
		$http({
	        method:'GET',
	        url:lien+'mesCommandeActuel?numeroToken='+token
	    }).then(function success(response){
	        $scope.commandeActuel=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getCategorie(){
		$http({
	        method:'GET',
	        url:lien+'categorie'
	    }).then(function success(response){
	        $scope.categorie=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getProduit(){
		$http({
	        method:'GET',
	        url:lien+'produit'
	    }).then(function success(response){
	        $scope.produit=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function nombreQuantite(){
        var nombreReel=[20];
        for (let index = 0; index < 20; index++) {
            nombreReel[index]=(index+1);
        }
        return nombreReel;
    }
    $scope.getTotal=function(idProduits,nombres,prixUnitaire){
        document.getElementById('total'+idProduits).innerHTML='Total : '+(nombres*prixUnitaire)+' Ariary';
    }
    
    //initialisation des fonctions
    
    $scope.nombre=nombreQuantite();
    getCommandeActuel();//commande
    getCategorie();//categorie
    getProduit();//produit
    
    //ng-click
    //modal
    var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.definirCommeCommandeActuel=function(){
    	if($scope.commandeSelect!=null && $scope.commandeSelect!=''){
    		localStorage.setItem('commandeActuel',$scope.commandeSelect);
            localStorage.setItem('commandeChanged','true');
            $scope.messageModale='Ajouté comme commande actuel : '+$scope.commandeSelect;
    	}else{
    		$scope.messageModale='Veuillez selectionné quelque chose';
    	}
        $scope.openModale();
    }
    $scope.ajouterDansLeCommande=function(idProduit,prixUnitaires){
        var commande=localStorage.getItem('commandeActuel');
        var nbr=document.getElementById('nombre'+idProduit).value;
        var data={ idProduit : idProduit, nombres : nbr, prixUnitaires : prixUnitaires, commande : commande};
        $http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'insertionCommandePlats',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.messageModale=response.data.message;
        	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale=response.data.message;
        	$scope.openModale();
            console.log(response);
        });
        localStorage.setItem('commandeTemporaire','true');
    }
    $scope.superieur=function(){
    	var sizeProduit=$scope.produit.length;
    	if(sizeProduit>=$scope.limiteProduit){
    		$scope.limiteProduit=$scope.limiteProduit+4;
    	}
    }
    $scope.inferieur=function(){
    	if($scope.limiteProduit>8){
    		$scope.limiteProduit=$scope.limiteProduit-4;
    	}
    }
    
    
});

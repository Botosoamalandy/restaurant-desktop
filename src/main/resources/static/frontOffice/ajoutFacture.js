var lien='http://localhost:8087/';
app.controller('ajoutFactureController', function($scope,$http,$location){
	//initialisation des données
	$scope.tousLesCommandes=[];
	$scope.tousLesFactures=[];
	$scope.numeroCommande='';
	$scope.nomUtilisateur='';
	$scope.etatPayer='';
	$scope.montant=0;
	$scope.dates='';
	$scope.messageModale='';
	//fonction
	function getTousLesCommande(){
		$http({
	        method:'GET',
	        url:lien+'tousLesCommande'
	    }).then(function success(response){
	        $scope.tousLesCommandes=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getTousLesFacture(){
		$http({
	        method:'GET',
	        url:lien+'tousLesFacture'
	    }).then(function success(response){
	        $scope.tousLesFactures=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getTextEtatCommande(valeur){
		if(valeur>=11){
			return "Payé";
		}
		return "Non payé";
	}
	function getValeurEtat(valeur){
		if(valeur==11){
			return 'Payement';
		}
		return 'commande';
	}
	
	//initialisation
	getTousLesCommande();
	getTousLesFacture();
	
	//ng-click
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.getInformationComande=function(idCommande){
		$http({
	        method:'GET',
	        url:lien+'informationCommandeReturnPayement?idCommande='+idCommande
	    }).then(function success(response){
	    	$scope.numeroCommande=response.data.idCommande;
	    	$scope.nomUtilisateur=response.data.idPayement;
	    	$scope.etatPayer=getTextEtatCommande(response.data.etatPayement);
	    	$scope.montant=response.data.montantTotal;
	    	$scope.dates=response.data.datePayement;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	$scope.ajoutFacture=function(){
		if($scope.idCommande!=null && $scope.idCommande!=''){
			var data={ idCommande : $scope.idCommande };
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'ajoutFacture',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	$scope.messageModale=''+response.data.message;
            	$scope.openModale();
            	getTousLesFacture();
            },function erreur(response){
				$scope.messageModale='Une erreur c\'est produit';
            	$scope.openModale();
				console.log(response);
            });
		}
	}
	$scope.supprimerFacture=function(idFacture){
		$http({
	        method:'GET',
	        url:lien+'caissierSupprimerFacture?idFacture='+idFacture
	    }).then(function success(response){
			$scope.messageModale=''+response.data.message;
        	$scope.openModale();
        	getTousLesFacture();
	    },function erreur(response){
	      console.log(response);
	    });
	}
	$scope.detailFacture=function(numeroCommande,etat){
        $http({
            method:'GET',
            url:lien+'idCommandeAPartNumeroCommande?numeroCommande='+numeroCommande
        }).then(function success(response){
            $location.url('facture?idcommande='+response.data.idCommande+'$'+getValeurEtat(etat));
        },function erreur(response){
          console.log(response);
        });
    }
});
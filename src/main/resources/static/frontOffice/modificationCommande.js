var lien='http://localhost:8087/';
app.controller('modificationCommandeController',function ($scope,$http,$location){
	//initialisation des données
	$scope.total=0;
	$scope.valeur='';
	$scope.commande='';
	$scope.numeroTable='';
	$scope.utilisateur='';
	$scope.dateCommande='';
	$scope.commandeParTable=[];
	$scope.messageModale='';
	
	//function
	function calculPrixTotal(data) {
        var total=0;var teste=false;
        for (let index = 0; index < data.length; index++) {
            total=total+(data[index].prixUnitaire*data[index].nombreDeProduit);
            if(teste==false){
                $scope.utilisateur=data[index].nomUtilisateur;
                $scope.numeroTable=data[index].numeroTables;
                $scope.dateCommande=data[index].dateCommande;
                $scope.commande=data[index].numeroCommande;teste=true;
            }
        }
        return total;
    }
	function getCommandeTable(valeur){
		$http({
            method:'GET',
            url:lien+'commandeParTableByIdCommande?idCommande='+valeur
        }).then(function success(response){
            $scope.commandeParTable=response.data;
            $scope.total=calculPrixTotal(response.data);
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
	    	$scope.openModale();
          console.log(response);
        });
	}
    var paramametres=window.location.search.slice(1,window.location.search.length);
    var first=paramametres.split("&");
    if(first.length==1){
        var valuesParametre=paramametres.split('=');
        $scope.valeur=valuesParametre[1];
        getCommandeTable($scope.valeur);
    }else{
    	$scope.messageModale='Il y a une erreur';
    	$scope.openModale();
    }
    function nombreQuantite(){
        var nombreReel=[20];
        for (let index = 0; index < 20; index++) {
            nombreReel[index]=(index+1);
        }
        return nombreReel;
    }
    $scope.nombre=nombreQuantite();
    
    
    //ng-click
    var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.rafraichier=function(commandeParTables,idProduit){	
		var idMontant='montant'+idProduit;
		document.getElementById(idMontant).innerHTML=''+(commandeParTables.prixUnitaire * commandeParTables.nombreDeProduit)+' Ariary';
    }
    $scope.changeTotalMontant=function(commandeParTables){
    	var idQuantite='quantite'+commandeParTables.idProduit;
    	var idMontant='montant'+commandeParTables.idProduit;
    	var quantite=document.getElementById(idQuantite).value;
    	var montant=(commandeParTables.prixUnitaire * parseInt(quantite));
    	document.getElementById(idMontant).innerHTML=''+montant+' Ariary';
    }
    $scope.modifier=function(commandeParTables){
    	var idQuantite='quantite'+commandeParTables.idProduit;
    	var quantiteTemporaire=document.getElementById(idQuantite).value;
    	var quantite=parseInt(quantiteTemporaire);
    	if(quantite>0 && quantite!=commandeParTables.nombreDeProduit && quantite<=commandeParTables.quantite){
    		$http({
                method:'GET',
                url:lien+'updateCommandeTable?idCommandeTable='+commandeParTables.idCommandeTable+'&quantite='+quantite
            }).then(function success(response){
            	$scope.messageModale=''+response.data.message;
    	    	$scope.openModale();
                getCommandeTable($scope.valeur);
            },function erreur(response){
            	$scope.messageModale='Il y a une erreur';
    	    	$scope.openModale();
              console.log(response);
            });
    	}else{
    		$scope.messageModale='Verifiez votre champs car la quantite est négative ou depasse la valeur dans le stocke';
	    	$scope.openModale();
    	}
    }
    $scope.supprimer=function(commandeParTables){
    	$http({
            method:'GET',
            url:lien+'supprimerCommandeTable?idCommandeTable='+commandeParTables.idCommandeTable
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
	    	$scope.openModale();
            getCommandeTable($scope.valeur);
        },function erreur(response){
          console.log(response);
        });
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
});
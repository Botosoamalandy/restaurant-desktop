var app = angular.module('principales', ['ngRoute','angularUtils.directives.dirPagination','datatables']);

app.config(function($routeProvider, $locationProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'accueil',
            controller : 'accueilController'
        }).when('/commande', {
            templateUrl: 'commande',
			controller : 'commandeController'
        }).when('/ajoutPlats', {
            templateUrl: 'ajoutPlats',
			controller : 'ajoutPlatsController'
        }).when('/ajoutCategorie', {
            templateUrl: 'ajoutCategorie',
			controller : 'ajoutCategorieController'
        }).when('/ajoutCommande', {
            templateUrl: 'ajoutCommande',
			controller : 'ajoutCommandeController'
        }).when('/detailCommande', {
            templateUrl: 'detailCommande',
			controller : 'detailCommandeController'
        }).when('/facture', {
            templateUrl: 'facture',
			controller : 'factureController'
        }).when('/modificationCommande', {
            templateUrl: 'modificationCommande',
			controller : 'modificationCommandeController'
        }).when('/payement', {
            templateUrl: 'payement',
			controller : 'payementController'
        }).when('/listePayement', {
            templateUrl: 'listePayement',
			controller : 'listePayementController'
        }).when('/ajoutPlatsDuJour', {
            templateUrl: 'ajoutPlatsDuJour',
			controller : 'ajoutPlatsDuJourController'
        }).when('/platsDuJours', {
            templateUrl: 'platsDuJours',
			controller : 'platsDuJoursController'
        }).when('/ajoutFacture', {
            templateUrl: 'ajoutFacture',
			controller : 'ajoutFactureController'
        }).when('/listeFacture', {
            templateUrl: 'listeFacture',
			controller : 'listeFactureController'
        }).when('/insuffisant', {
            templateUrl: 'insuffisant',
			controller : 'insuffisantController'
        }).when('/preparation', {
            templateUrl: 'preparation',
			controller : 'preparationController'
        }).when('/moncompte', {
            templateUrl: 'moncompte',
			controller : 'moncompteController'
        }).otherwise({
            redirectTo : '/'
        });

        $locationProvider.html5Mode({
            enabled: true,
            requireBase: false
        });
});

app.directive('modalDialog', function() {
  return {
    restrict: 'E',
    scope: {
      show: '='
    },
    replace: true, // Replace with the template below
    transclude: true, // we want to insert custom content inside the directive
    link: function(scope, element, attrs) {
      scope.dialogStyle = {};
      if (attrs.width)
        scope.dialogStyle.width = attrs.width;
      if (attrs.height)
        scope.dialogStyle.height = attrs.height;
      scope.hideModal = function() {
        scope.show = false;
      };
    },
    template: '...' // See below
  };
});


app.controller('principalesController', function($scope, $route, $routeParams, $location){
    $scope.variable = "VARIABLE DANS LE CONTROLLEUR PRINCIPAL";
});
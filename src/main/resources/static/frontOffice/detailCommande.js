var lien='http://localhost:8087/';
app.controller('detailCommandeController',function ($scope,$http,$location){
	//initialisation des données
	$scope.etat=0;
	$scope.prixTotal=0;
	$scope.commande='';
	$scope.messageModale='';
	//fonction
	$scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	var paramametres=window.location.search.slice(1,window.location.search.length);
    var first=paramametres.split("&");
	$scope.message='page detail commande';
	if(first.length==1){
		var valuesParametre=paramametres.split('=');
		var valeur=valuesParametre[1];
		$scope.message=valeur;
		if(valeur!=null && valeur!=''){
			$http({
				method:'GET',
				url:lien+'commandeParTableByIdCommande?idCommande='+valeur
			}).then(function success(response){
				$scope.commandeParTable=response.data;
				var size=response.data.length;
				for (let index = 0; index < size; index++) {
					$scope.etat=response.data[index].etatCommande;
					$scope.commande=response.data[index].numeroCommande;
					$scope.prixTotal=$scope.prixTotal+(response.data[index].nombreDeProduit*response.data[index].prixUnitaire);
				}
			},function erreur(response){
				$scope.messageModale='Une erreur c\'est produit';
	        	$scope.openModale();
	        	console.log(response);
			});
		}else{
			$scope.messageModale='cette commande n\'existe pas';
        	$scope.openModale();
		}
	}else{
		$scope.messageModale='cette commande n\'existe pas ';
    	$scope.openModale();
	}
});
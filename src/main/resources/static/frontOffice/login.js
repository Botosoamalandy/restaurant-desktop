var lien='http://localhost:8087/';
var login = angular.module('login', []);
login.controller("loginController",function ($scope,$http){
	//initialisation des données
	$scope.messageModale='';
	$scope.affichageErreur=false;	
	var testLogin=1;
	var token=localStorage.getItem('token');
	
	//condition
	if(testLogin==1 || token!=null || token!=''){
        $http({
              method:'GET',
              url:lien+'loginToken?token='+localStorage.getItem('token')
          }).then(function success(response){
              var resultat=''+response.data.validation;
              if(resultat=='true'){
                  document.location.href="page";
              }else{
                  teste=0;
              }
          },function erreur(response){
            console.log(response);
          });
    }
	$scope.login=function(){
		if($scope.nom!='' && $scope.nom!=null && $scope.motDePasse!='' && $scope.motDePasse!=null){
			var data={ nom : $scope.nom, motDePasse : $scope.motDePasse };		 
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'login',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	var token=response.data;
            	if(token.numeroToken!=''){
            		localStorage.setItem('token',token.numeroToken);
            		document.location.href="page";
            	}else{
            		$scope.messageModale='Verifiez votre champs ou vous n\'avez pas l\' autorization requise';
            		$scope.affichageErreur=true;
            	}
            },function erreur(response){
            	$scope.messageModale='Une erreur c\'est produit';
            	$scope.affichageErreur=true;
				console.log(response);
            });
		}else{
			$scope.messageModale='Il y a un champs vide';
			$scope.affichageErreur=true;
		}
	}
});

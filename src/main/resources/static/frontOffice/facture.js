var lien='http://localhost:8087/';
app.controller('factureController',function ($scope,$http,$location){
	//initialiqation des données
    $scope.commande='';
    $scope.datesCommande='';
    $scope.commande='';
    $scope.etat='Commande non payé';
    $scope.prixTotal=0;
    $scope.argentResteClient=0;
    $scope.date='';
    $scope.messageModale='';
    
    //fonction
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    function calculPrixTotal(data) {
        var total=0;var teste=false;
        for (let index = 0; index < data.length; index++) {
            total=total+(data[index].prixUnitaire*data[index].nombreDeProduit);
            if(teste==false){
                $scope.datesCommande=data[index].dateCommande;
                $scope.commande=data[index].numeroCommande;
                teste=true;
            }
        }
        return total;
    }
    function detailCommande(idCommande){
        $http({
          method:'GET',
          url:lien+'informationPayementByIdCommande?idCommande='+idCommande
        }).then(function success(response){
          $scope.commande=''+response.data.commande;
          $scope.etat=''+response.data.etat;
          $scope.prixTotal=response.data.prixTotal;
          $scope.argentResteClient=response.data.argentResteClient;
          $scope.date=response.data.date;
        },function erreur(response){
          console.log(response);
        });
      }
    var paramametres=window.location.search.slice(1,window.location.search.length);
    var first=paramametres.split("&");
    if(first.length==1){
        var valuesParametre=paramametres.split('=');
        var secondeValuesParametre=valuesParametre[1].split('$');
        var valeur1=secondeValuesParametre[0];
        $scope.valeur2=secondeValuesParametre[1];
        $http({
            method:'GET',
            url:lien+'commandeParTableByIdCommande?idCommande='+valeur1
        }).then(function success(response){
            $scope.prixTotal=calculPrixTotal(response.data);
            $scope.platsCommande=response.data;
            detailCommande(valeur1);
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
        	console.log(response);
        });
        
    }else{
    	$scope.messageModale='Il y a une erreur';
    	$scope.openModale();
        //redirection page erreur
        alert('Erreur de page');
    }
    
});
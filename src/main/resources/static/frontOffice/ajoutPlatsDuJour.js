var lien='http://localhost:8087/';
app.controller('ajoutPlatsDuJourController',function ($scope,$http,$location){
	//initialisation des données
	$scope.produits=[];
	$scope.platsDuJours=[];
	$scope.nomProduit='';
	$scope.categrorie='';
	$scope.prixProduit=0;
	$scope.nomImage='';
	$scope.messageModale='';
	function getProduit(){
	    $http({
	        method:'GET',
	        url:lien+'produit'
	    }).then(function success(response){
	        $scope.produits=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	function getPlatsDuJours(){
		$http({
	        method:'GET',
	        url:lien+'tousLesPlatsDuJour'
	    }).then(function success(response){
	    	$scope.platsDuJours=response.data;
	    },function erreur(response){
	      console.log(response);
	    });
	}
	//initialisation fonction
	getProduit();
	getPlatsDuJours();
	
	//ng-click
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
	$scope.selectionProduit=function(produit){
		$scope.nomProduit=produit.nomProduit;
		$scope.categrorie=produit.categorie;
		$scope.prixProduit=produit.prixUnitaire;
		$scope.nomImage=produit.nomImage;
	}
	$scope.insertionPlatsDuJours=function(){
		var objectProduit=JSON.parse($scope.produitObject);
		var token=localStorage.getItem('token');
		var data={ 
			idProduit : objectProduit.idProduit,
			categorie : objectProduit.categorie,
			prixUnitaire : objectProduit.prixUnitaire,
			descriptions : objectProduit.nomImage,
			tokenUtilisateur : token 
		};		 
        $http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'insertionPlatsDuJour',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
            getPlatsDuJours();
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
        	$scope.openModale();
			console.log(response);
        });
	}
	$scope.suppressionPlatsDuJour=function(idPlatsDuJour){
		$http({
	        method:'GET',
	        url:lien+'supprimerPlatsDuJour?idPlatsDuJour='+idPlatsDuJour
	    }).then(function success(response){
	    	$scope.messageModale=''+response.data.message;
        	$scope.openModale();
            getPlatsDuJours();
	    },function erreur(response){
	    	$scope.messageModale='Une erreur c\'est produit';
	    	$scope.openModale();
	    	console.log(response);
	    });
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
});
var lien='http://localhost:8087/';
app.controller('ajoutCommandeController', function($scope,$http,$location){
	$scope.messageModale='';
	//fonction
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    function defaultFonction(){
    	$http({
            method:'GET',
            url:lien+'commandeActuel'
        }).then(function success(response){
        	$scope.commande=response.data;
        },function erreur(response){
          console.log(response);
        });
    }
    function getTableRestaurent(){
    	$http({
            method:'GET',
            url:lien+'tablesRestaurant'
        }).then(function success(response){
            $scope.tablesRestaurant=response.data;
        },function erreur(response){
        	$scope.messageModale='Il y a une erreur';
        	$scope.openModale();
          console.log(response);
        });
    }
    //initialisation des fonctions
	defaultFonction();
	getTableRestaurent();
	
	//ng-click
    $scope.ajouterCommande=function(){
        var tokenUtilisateur=''+localStorage.getItem('token');
        if($scope.numeroTable!=null && $scope.numeroTable!='' && tokenUtilisateur!=null && tokenUtilisateur!=''){
            var data={ numeroTable : $scope.numeroTable , tokenUtilisateurs : tokenUtilisateur};	
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'insertionCommande',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	$scope.messageModale=response.data.message;
            	$scope.openModale();
            	defaultFonction();
            },function erreur(response){
				$scope.messageModale='Une erreur c\'est produit';
	        	$scope.openModale();
				console.log(response);
            });
        }else{
            $scope.messageModale='Il y a une champs vide';
        	$scope.openModale();
        }
    }
    $scope.facturerEtFemer=function(numeroCommande){
        $scope.messageModale='facturation du commande :'+numeroCommande;
    	$scope.openModale();
        localStorage.removeItem('commandeActuel');
    }
    $scope.detail=function(idCommande){
    	$location.url('detailCommande?idCommande='+idCommande);
    }
});

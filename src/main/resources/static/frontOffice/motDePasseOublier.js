var valeur=false;
function getVerificationLenghtWords(password){
    if(password.length<8){
        return false;
    }
    return true;
}
function getIfNumberExist(password){
    if(password.search(/[0123456789]/)=== -1){
        return false;
    }
    return true;
}
function getIfAlphabSmallWordExist(password){
    if(password.search(/[a-z]/)==-1){
        return false;
    }
    return true;
}
function getIfAlphabExist(password){
    if(password.search(/[A-Z]/)==-1){
        return false;
    }
    return true;
}
function getIfWordsSpecialExist(password){
    if(password.search(/[\[~!@#|\"$%;^'\\[=&(:?`/<>.,)£ù*§\]]/)==-1){
        return false;
    }
    return true;
}
function assembleLesLettre(valeur,mot){
    if(valeur==false){
        return mot;
    }
    return '';
}
function disabledBouton(){
	if(valeur==true){
		document.getElementById('bouton').disabled=false;
	}else{
		document.getElementById('bouton').disabled=true;
	}
}
function verificationPassword(){
    var passwords=document.getElementById('password').value;
    let texts1='Aux moins 8 caractère';let texts2=',un majuscule';let texts3=',une minuscule';let texts4=',un chiffre';let texts5=',un caractère speciaux';
    if(getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
        document.getElementById('textValidation').innerHTML='Mot de passe correct';
        document.getElementById('textValidation').style.color="green";confirmationPassword();disabledBouton();
    }else{
        document.getElementById('textValidation').style.color="gray";
        document.getElementById('textValidation').innerHTML=''+assembleLesLettre(getVerificationLenghtWords(passwords),texts1)+assembleLesLettre(getIfAlphabExist(passwords),texts2)+
        assembleLesLettre(getIfAlphabSmallWordExist(passwords),texts3)+assembleLesLettre(getIfNumberExist(passwords),texts4)+
        assembleLesLettre(getIfWordsSpecialExist(passwords),texts5);valeur=false;disabledBouton();
    }
}
function confirmationPassword(){
    var passwords=document.getElementById('password').value;var confirmer=document.getElementById('confirmation').value;
    if(passwords==confirmer && getVerificationLenghtWords(passwords) && getIfAlphabExist(passwords) && getIfAlphabSmallWordExist(passwords) && getIfNumberExist(passwords) && getIfWordsSpecialExist(passwords)){
        document.getElementById('textConfirmation').innerHTML='Confirmé';
        document.getElementById('textConfirmation').style.color="green";
        valeur=true;document.getElementById('bouton').disabled=false;
    }else{
        document.getElementById('textConfirmation').innerHTML='Non confirmer';
        document.getElementById('textConfirmation').style.color="gray";
        valeur=false;disabledBouton();
    }
}

var lien='http://localhost:8087/';
var motDePasseOublier = angular.module('motDePasseOublier', []);
motDePasseOublier.controller("motDePasseOublierController",function ($scope,$http){
	$scope.ModifierMotDePasse=function(){
		var dates=document.getElementById('dateAjout').value;
		if($scope.nom!=null && $scope.nom!='' && dates!=null && dates!='' && $scope.motDePasse!=null && $scope.motDePasse!='' && valeur==true){
			var data={
				nom : $scope.nom,
		        dateAjout : dates,
		        nouveauMotDePasse : $scope.motDePasse
			}
			console.log(data);
			$http({
				method: 'POST',
				data:JSON.stringify(data),
				url: lien+'motDePasseOublier',
				headers: {'Content-Type': 'application/json','Accept': 'application/json'}
			}).then(function success(response){
				alert(response.data.message);
			},function erreur(response){
				alert('Une erreur c\'est produit');
				console.log(response);
			});
		}else{
			alert('Verifiez votre champs');
		}
	}
});




















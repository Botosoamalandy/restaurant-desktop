var lien='http://localhost:8087/';
app.controller('ajoutCategorieController',function ($scope,$http){
	$scope.messageModale='';
	var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.ajouterCategorie=function(){
        if($scope.categorie!=null && $scope.categorie!=''){
            var data={ categorie : $scope.categorie };		 
            $http({
                method: 'POST',
                data:JSON.stringify(data),
                url: lien+'insertionCategorie',
                headers: {'Content-Type': 'application/json','Accept': 'application/json'}
            }).then(function success(response){
            	$scope.messageModale=''+response.data.message;
            	$scope.openModale();
            },function erreur(response){
            	$scope.messageModale='Une erreur c\'est produit';
            	$scope.openModale();
				console.log(response);
            });
        }else{
        	$scope.messageModale='Il y a une champs vide';
        	$scope.openModale();
        }
    }
});

var lien='http://localhost:8087/';
app.controller('platsDuJoursController',function ($scope,$http,$window){
	var token=localStorage.getItem('token');
	//initialisation des données
	$scope.limite=false;
	$scope.limiteProduit=8;
	$scope.produits=[];
	$scope.commandeActuel=[];
	$scope.messageModale='';
	//fonction
    function nombreQuantite(){
        var nombreReel=[20];
        for (let index = 0; index < 20; index++) {
            nombreReel[index]=(index+1);
        }
        return nombreReel;
    }
	function getProduit(){
	    $http({
	        method:'GET',
	        url:lien+'produitPlatsDuJour'
	    }).then(function success(response){
	        $scope.produits=response.data;
	    },function erreur(response){
	    	$scope.messageModale='Une erreur c\'est produit';
	  	  	$scope.openModale();
	      console.log(response);
	    });
	}
	function getCommande(){
	    $http({
	        method:'GET',
	        url:lien+'mesCommandeActuel?numeroToken='+token
	    }).then(function success(response){
	        $scope.commandeActuel=response.data;
	    },function erreur(response){
	    	$scope.messageModale='Une erreur c\'est produit';
	  	  	$scope.openModale();
	      console.log(response);
	    });
	}
	$scope.getTotal=function(idProduits,nombres,prixUnitaire){
        document.getElementById('total'+idProduits).innerHTML='Total : '+(nombres*prixUnitaire)+' }} Ariary';
    }
	
	//initialisation fonction
	getProduit();
	getCommande();
    $scope.nombre=nombreQuantite();
    
    
    //ng-click
    var modal=document.getElementById("myModal");
    $scope.openModale=function(){
        modal.style.display="block";
    }
    $scope.closeModal=function(){
        modal.style.display="none";
    }
    $scope.ajouterDansLeCommande=function(idProduit,prixUnitaires){
        var commande=localStorage.getItem('commandeActuel');
        var nbr=document.getElementById('nombre'+idProduit).value;
        var data={ idProduit : idProduit, nombres : nbr, prixUnitaires : prixUnitaires, commande : commande};
        $http({
            method: 'POST',
            data:JSON.stringify(data),
            url: lien+'insertionCommandePlats',
            headers: {'Content-Type': 'application/json','Accept': 'application/json'}
        }).then(function success(response){
        	$scope.messageModale=''+response.data.message;
      	  	$scope.openModale();
        },function erreur(response){
        	$scope.messageModale='Une erreur c\'est produit';
      	  	$scope.openModale();
            console.log(response);
        });
        localStorage.setItem('commandeTemporaire','true');
    }
    $scope.definirCommeCommandeActuel=function(){
        localStorage.setItem('commandeActuel',$scope.commandeSelect);
        localStorage.setItem('commandeChanged','true');
    	$scope.messageModale='Ajouté comme commande actuel : '+$scope.commandeSelect;
  	  	$scope.openModale();
    }
    $scope.superieur=function(){
    	var sizeProduit=$scope.produits.length;
    	if(sizeProduit>=$scope.limiteProduit){
    		$scope.limiteProduit=$scope.limiteProduit+4;
    	}
    }
    $scope.inferieur=function(){
    	if($scope.limiteProduit>8){
    		$scope.limiteProduit=$scope.limiteProduit-4;
    	}
    }
});
package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.FactureService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class FactureController {
	FactureService factureService=new FactureService();
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	@PostMapping("/ajoutFacture")
	public String ajoutFacture(@RequestBody HashMap<String,String> data) throws Exception {
		String idCommande=(String)data.get("idCommande");
		return factureService.insertionFactures(idCommande);
	}
	
	@GetMapping("/caissierSupprimerFacture")
	public String caissierSupprimerFacture(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return factureService.supprimerCaissierFacture((String)request.getParameter("idFacture"));
	}
	
	@GetMapping("/tousLesFacture")
	public String tousLesFacture(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return factureService.tousLesFactureNonSupprimer();
	}
	
	@GetMapping("/tousLesFactureAdministrateur")
	public String tousLesFactureAdministrateur(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return factureService.getFactureAdministrateurs();
	}
	
	@GetMapping("/supprimerFacture")
	public String supprimerFacture(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return factureService.supprimerFactureAdministrateur((String)request.getParameter("idFacture"));
	}
	
}

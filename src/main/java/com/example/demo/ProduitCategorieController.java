package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.ProduitCategorieService;
import com.restaurent.service.ProduitService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class ProduitCategorieController {
	ProduitCategorieService produitCategorieService=new ProduitCategorieService();
	ProduitService produitService=new ProduitService();
	
	@GetMapping("/produit")
	public String produit(HttpServletResponse response) throws Exception {
		(new FonctionGeneraliser()).settingCorse(response, "json");
		return produitCategorieService.getProduitCategorieToFormatJson();
	}
	
	@GetMapping("/produitPlatsDuJour")
	public String produitPlatsDuJour(HttpServletResponse response) throws Exception {
		(new FonctionGeneraliser()).settingCorse(response, "json");
		return produitCategorieService.getProduitCategoriePlatsDuJourToFormatJson();
	}
	@PostMapping("/updateQuantiteEtDescriptionProduitService")
	public String updateQuantiteEtDescriptionProduitService(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String idProduit=(String)data.get("idProduit"),quantite=data.get("quantite"),description=data.get("description");
		return produitService.updateQuantiteEtDescriptionProduitService(idProduit, quantite, description);
	}
}

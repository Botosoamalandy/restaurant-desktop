package com.example.demo;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.TablesRestaurantService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class TablesRestaurantControlleur {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	@GetMapping("tablesRestaurant")
	public String tablesRestaurant(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		TablesRestaurantService restaurantService=new TablesRestaurantService();
		return restaurantService.getTablesRestaurant();
	}
}

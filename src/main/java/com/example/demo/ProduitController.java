package com.example.demo;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import com.restaurent.service.ProduitService;

@Controller
public class ProduitController {
	String uploadDirectory=System.getProperty("user.dir")+"/src/main/resources/static/assets/images/img";
	@RequestMapping("/insertionProduits")
	 public String insertionProduits(Model model,@RequestParam("files") MultipartFile[] files,HttpServletRequest request) {
		 StringBuilder fileNames=new StringBuilder();
		 for (MultipartFile file : files) {
			try {
				if(file!=null) {
					ProduitService produitService=new ProduitService();
					String nomPhoto=produitService.creationNomPhoto(file.getOriginalFilename());
					System.out.println("1---------------------------------");
					System.out.println("uploadDirectory :"+uploadDirectory);
					System.out.println("nomPhoto :"+nomPhoto);
					System.out.println("1---------------------------------");
					Path fileNameAndPath=Paths.get(uploadDirectory,nomPhoto);
					fileNames.append(file.getOriginalFilename());
					Files.write(fileNameAndPath, file.getBytes());		
					String categorie=(String)request.getParameter("categorie"),nom=(String)request.getParameter("nom"),
						   prixUnitaire=(String)request.getParameter("prixUnitaire"),tva=(String)request.getParameter("tva"),
						   remise=(String)request.getParameter("remise"),description=(String)request.getParameter("description");
					String msg= produitService.insertionProduits(categorie, nom, prixUnitaire, description, nomPhoto, tva, remise);
					System.out.println("msg"+msg);
				}
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.getMessage());
			}
		 }
		 return "frontOffice/ajoutPlats";
	 }
}

package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.PreparationService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class PreparationController {
	PreparationService preparationService=new PreparationService();
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	@PostMapping("/insertionPreparation")
	public String insertionPreparation(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String idCommande=(String)data.get("idCommande"),numeroCommande=data.get("numeroCommande"),nombreDePlats=data.get("nombreDePlats"),
			   numeroTable=(String)data.get("numeroTables");
		return preparationService.insertePreparation(idCommande, numeroCommande, nombreDePlats,numeroTable);
	}
	@GetMapping("/etatPreparation")
	public String etatPreparation(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.getAutorisationAnnulation((String)request.getParameter("idCommande"));
	}
	@GetMapping("/listePreparation")
	public String listePreparation(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.getListePreparation();
	}
	@GetMapping("/listePreparationPret")
	public String listePreparationPret(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.getListePreparationPret();
	}
	@GetMapping("/listePreparationPretByIdUtilisateur")
	public String listePreparationPretByIdUtilisateur(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.getListePreparationByIdUtilisateur((String)request.getParameter("numeroToken"));
	}
	@GetMapping("/commandePret")
	public String commandePret(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.commandePret((String)request.getParameter("idPreparation"));
	}
	@GetMapping("/supprimerPreparationCuisine")
	public String supprimerPreparationCuisine(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.supprimerPreparation((String)request.getParameter("idPreparation"));
	}
	@GetMapping("/preparationNonSupprimer")
	public String preparationNonSupprimer(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return preparationService.getListePreparationNonSupprimer();
	}
}

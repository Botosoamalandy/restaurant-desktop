package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageCuisineControlleur {
	@RequestMapping("/indexCuisine")
	public String indexCuisine() {
		return "cuisine/indexCuisine";
	}
	@RequestMapping("/pageCuisine")
	public String pageCuisine() {
		return "cuisine/pageCuisine";
	}
	@RequestMapping("/listeCommandeCuisine")
	public String listeCommandeCuisine() {
		return "cuisine/listeCommandeCuisine";
	}
	@RequestMapping("/menuCuisine")
	public String menuCuisine() {
		return "cuisine/menuCuisine";
	}
	@RequestMapping("/detailCommandeCuisine")
	public String detailCommandeCuisine() {
		return "cuisine/detailCommandeCuisine";
	}
	@RequestMapping("/listePreparationCuisine")
	public String listePreparationCuisine() {
		return "cuisine/listePreparationCuisine";
	}
	@RequestMapping("/listeCommandePretCuisine")
	public String listeCommandePretCuisine() {
		return "cuisine/listeCommandePretCuisine";
	}
	@RequestMapping("/platsCuisine")
	public String platsCuisine() {
		return "cuisine/platsCuisine";
	}
	
}

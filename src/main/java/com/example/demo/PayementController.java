package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.PayementService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class PayementController {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	PayementService payementService=new PayementService();
	
	@GetMapping("/informationPayementByIdCommande")
	public String informationPayementByIdCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.getInformationPayementByIdCommande((String)request.getParameter("idCommande"));
	}
	@PostMapping("/insertionPayement")
	public String insertionPayement(@RequestBody HashMap<String,String> data) {
		String idCommande=(String)data.get("idCommande"),montant=(String)data.get("montant");
		return payementService.insertionPayements(idCommande, montant);
	}
	
	@GetMapping("/verificationDedoublementPayement")
	public String verificationDedoublementPayement(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.dedoublementPayement((String)request.getParameter("idCommande"),(String)request.getParameter("montant"));
	}
	
	@GetMapping("/tousLesPayement")
	public String tousLesPayement(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.getPayementsByidCommande();
	}
	
	@GetMapping("/caissierSupprimerPayement")
	public String caissierSupprimerPayement(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.caissierSupprimerPayement((String)request.getParameter("idPayement"));
	}
	@GetMapping("/annulationPayementParCommandeOuParPayement")
	public String annulationPayementParCommandeOuParPayement(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String typeDAnnulation=(String)request.getParameter("typeDAnnulation");boolean valeur=false;
		if(typeDAnnulation.equals("true")) {
			valeur=true;
		}
		return payementService.annulationPayementParCommande((String)request.getParameter("idPayement"),valeur);
	}
	@GetMapping("/informationDUnPayement")
	public String informationDUnPayement(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.getInformationPayementByIdPayement((String)request.getParameter("idPayement"));
	}
	@GetMapping("/informationCommandeReturnPayement")
	public String informationCommandeReturnPayement(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.getInformationPayementCommandeByIdCommande((String)request.getParameter("idCommande"));
	}
	
	
	@GetMapping("/listePayementAdministrateur")
	public String listePayementAdministrateur(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.getPayementsByidCommandeAdministrateur();
	}
	@GetMapping("/supprimerPayementAdministrateur")
	public String supprimerPayementAdministrateur(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return payementService.supprimerPayementAdministrateur((String)request.getParameter("idPayement"));
	}
}

package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.InsuffisantService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class InsuffisantController {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	InsuffisantService insuffisantService=new InsuffisantService();
	
	@PostMapping("/insertionInsuffisantCuisine")
	public String insertionInsuffisantCuisine(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String idProduit=(String)data.get("idProduit"),idCommande=data.get("idCommande"),idUtilisateur=data.get("idUtilisateur"),numeroCommande=data.get("numeroCommande");
		return insuffisantService.inserteInsuffisant(idProduit, idCommande, idUtilisateur, numeroCommande);
	}
	@GetMapping("/listeInsuffisantByNumeroToken")
	public String listeInsuffisantByNumeroToken(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return insuffisantService.getListeInsuffisantByIdUtilisateur((String)request.getParameter("numeroToken"));
	}
	@GetMapping("/compteInsuffisantByNumeroToken")
	public String compteInsuffisantByNumeroToken(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return insuffisantService.getCompterInsuffisantByIdUtilisateur((String)request.getParameter("numeroToken"));
	}
	@GetMapping("/supprimerInsuffisant")
	public String supprimerInsuffisant(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return insuffisantService.supprimerInsuffisant((String)request.getParameter("idInsuffisant"),(String)request.getParameter("idCommande"));
	}
	@GetMapping("/supprimerInsuffisantEtModifierCommande")
	public String supprimerInsuffisantEtModifierCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return insuffisantService.supprimerInsuffisantEtModifierCommande((String)request.getParameter("idInsuffisant"),(String)request.getParameter("idCommande"));
	}
}

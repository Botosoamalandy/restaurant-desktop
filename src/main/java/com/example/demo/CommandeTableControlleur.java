package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.CommandeTableService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class CommandeTableControlleur {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	CommandeTableService commandeTableService=new CommandeTableService();
	
	@PostMapping("insertionCommandePlats")
	public String insertionCommandePlats(@RequestBody HashMap<String,String> data) {
		String idProduit=(String)data.get("idProduit"),nombres=(String)data.get("nombres"),prixUnitaires=(String)data.get("prixUnitaires"),commande=(String)data.get("commande");
		return commandeTableService.insertionCommandeTables(idProduit, nombres, prixUnitaires, commande);
	}
	
	@GetMapping("/supprimerCommandeTable")
	public String supprimerCommandeTable(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeTableService.suppressionEtUpdateCommandeTable((String)request.getParameter("idCommandeTable"), "4",false);
	}
	@GetMapping("/updateCommandeTable")
	public String updateCommandeTable(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeTableService.suppressionEtUpdateCommandeTable((String)request.getParameter("idCommandeTable"),(String)request.getParameter("quantite"),true);
	}
	@GetMapping("/deleteCommandeTableByNumeroCommande")
	public String deleteCommandeTableByNumeroCommande(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeTableService.supprimerCommandeTableByNumeroCommande((String)request.getParameter("numeroCommande"));
	}
}

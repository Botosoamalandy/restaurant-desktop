package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.service.TableauStatistiqueService;

@RestController
public class TableauStatistiqueController {
	TableauStatistiqueService tableauStatistiqueService=new TableauStatistiqueService();
		
	@PostMapping("/tableauStatistique")
	public String tableauStatistique(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String typeDeClassement=(String)data.get("typeDeClassement"),classement=data.get("classement"),annee=data.get("annee"),mois=data.get("mois");
		return tableauStatistiqueService.getTableauStatistique(typeDeClassement, classement, annee, mois);
	}
}

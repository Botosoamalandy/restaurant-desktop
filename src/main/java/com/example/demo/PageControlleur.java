package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageControlleur {
	@RequestMapping("/")
	public String index() {
		return "frontOffice/index";
	}
	@RequestMapping("/inscription")
	public String inscription() {
		return "frontOffice/inscription";
	}
	@RequestMapping("/page")
	public String page() {
		return "frontOffice/page";
	}
	@RequestMapping("/menu")
	public String menu() {
		return "frontOffice/menu";
	}
	@RequestMapping("/accueil")
	public String accueil() {
		return "frontOffice/accueil";
	}
	@RequestMapping("/commande")
	public String commande() {
		return "frontOffice/commande";
	}
	@RequestMapping("/ajoutPlats")
	public String ajoutPlats() {
		return "frontOffice/ajoutPlats";
	}
	@RequestMapping("/ajoutCategorie")
	public String ajoutCategorie() {
		return "frontOffice/ajoutCategorie";
	}
	@RequestMapping("/ajoutCommande")
	public String ajoutCommande() {
		return "frontOffice/ajoutCommande";
	}
	@RequestMapping("/detailCommande")
	public String detailCommande() {
		return "frontOffice/detailCommande";
	}
	@RequestMapping("/facture")
	public String facture() {
		return "frontOffice/facture";
	}
	@RequestMapping("/modificationCommande")
	public String modificationCommande() {
		return "frontOffice/modificationCommande";
	}
	@RequestMapping("/payement")
	public String payement() {
		return "frontOffice/payement";
	}
	@RequestMapping("/listePayement")
	public String listePayement() {
		return "frontOffice/listePayement";
	}
	@RequestMapping("/ajoutPlatsDuJour")
	public String ajoutPlatsDuJour() {
		return "frontOffice/ajoutPlatsDuJour";
	}
	@RequestMapping("/platsDuJours")
	public String platsDuJours() {
		return "frontOffice/platsDuJours";
	}
	@RequestMapping("/ajoutFacture")
	public String ajoutFacture() {
		return "frontOffice/ajoutFacture";
	}
	@RequestMapping("/listeFacture")
	public String listeFacture() {
		return "frontOffice/listeFacture";
	}
	@RequestMapping("/insuffisant")
	public String insuffisant() {
		return "frontOffice/insuffisant";
	}
	@RequestMapping("/preparation")
	public String preparation() {
		return "frontOffice/preparation";
	}
	@RequestMapping("/moncompte")
	public String moncompte() {
		return "frontOffice/moncompte";
	}
	@RequestMapping("/motDePasseOublier")
	public String motDePasseOublier() {
		return "frontOffice/motDePasseOublier";
	}
}

package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.CommandeService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class CommandeControlleur {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	CommandeService commandeService=new CommandeService();
	
	@GetMapping("/commandeActuel")
	public String commandeActuel(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeService.getCommandeActuel();
	}
	@GetMapping("/idCommandeByNumeroCommande")
	public String idCommandeByNumeroCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroCommande=(String)request.getParameter("numeroCommande");
		return commandeService.getidCommandeByNumeroCommande(numeroCommande);
	}
	@GetMapping("/mesCommandeActuel")
	public String mesCommandeActuel(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroToken=(String)request.getParameter("numeroToken");
		return commandeService.getMesCommandesActuel(numeroToken);
	}
	@GetMapping("/mesCommandeByIdUtilisateur")
	public String mesCommandeByIdUtilisateur(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroToken=(String)request.getParameter("numeroToken");
		return commandeService.getMesCommandesByIdUtilisateur(numeroToken);
	}
	@PostMapping("/insertionCommande")
	public String insertionCommande(@RequestBody HashMap<String,String> data) throws Exception {
		String numeroTable=(String)data.get("numeroTable"),tokenUtilisateurs=(String)data.get("tokenUtilisateurs");
		return commandeService.insertionCommandes(numeroTable, tokenUtilisateurs);
	}
	
	@GetMapping("/verificationSiCommandeFermerOuPas")
	public String getVerificationSiCommandeFermerOuPas(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroCommande=(String)request.getParameter("numeroCommande");
		return commandeService.getVerificationSiCommandeFermerOuPas(numeroCommande);
	}
	
	@GetMapping("/updateFermetureCommandes")
	public String updateFermetureCommandes(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroCommande=(String)request.getParameter("numeroCommande");
		return commandeService.updateFermetureCommandes(numeroCommande);
	}
	
	@GetMapping("/updateEtatcommandes")
	public String updateEtatcommandes(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeService.updateEtatcommandes((String)request.getParameter("idCommande"));
	}
	
	@GetMapping("/commandeValider")
	public String commandeValider(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeService.getCommandeValides();
	}
	
	@GetMapping("/tousLesCommande")
	public String tousLesCommande(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeService.getTousLesCommande();
	}
	@GetMapping("/idCommandeAPartNumeroCommande")
	public String getidCommandeAPartNumeroCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroCommande=(String)request.getParameter("numeroCommande");
		return commandeService.getidCommandeAPartNumeroCommande(numeroCommande);
	}
}

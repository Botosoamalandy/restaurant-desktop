package com.example.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageBackOfficeController {
	@RequestMapping("/loginBackOffice")
	public String loginBackOffice() {
		return "backOffice/loginBackOffice";
	}
	@RequestMapping("/pageBackOffice")
	public String pageBackOffice() {
		return "backOffice/pageBackOffice";
	}
	@RequestMapping("/listeUtilisateurBack")
	public String listeUtilisateurBack() {
		return "backOffice/listeUtilisateurBack";
	}
	@RequestMapping("/ajoutUtilisateurBack")
	public String ajoutUtilisateurBack() {
		return "backOffice/ajoutUtilisateurBack";
	}
	@RequestMapping("/listeCommandeBack")
	public String listeCommandeBack() {
		return "backOffice/listeCommandeBack";
	}
	@RequestMapping("/detailCommandeBack")
	public String detailCommandeBack() {
		return "backOffice/detailCommandeBack";
	}
	@RequestMapping("/listePayementBack")
	public String listePayementBack() {
		return "backOffice/listePayementBack";
	}
	@RequestMapping("/listeFactureBack")
	public String listeFactureBack() {
		return "backOffice/listeFactureBack";
	}
	@RequestMapping("/factureBack")
	public String factureBack() {
		return "backOffice/factureBack";
	}
	@RequestMapping("/statistiqueBack")
	public String statistiqueBack() {
		return "backOffice/statistiqueBack";
	}
	@RequestMapping("/menuback")
	public String menuback() {
		return "backOffice/menuback";
	}
}

package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.CommandesCompletService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class CommandesCompletControlleur {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	CommandesCompletService commandesCompletService=new CommandesCompletService();
	
	@GetMapping("/commandesComplet")
	public String commandesCompletTous(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletToFormatJson();
	}
	@GetMapping("/commandesCompletValider")
	public String commandesCompletValider(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletValiderToFormatJson();
	}
	@GetMapping("/commandesCompletNonValider")
	public String commandesCompletNonValider(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletNonValiderToFormatJson();
	}
	@GetMapping("/commandesCompletNonValiderTemporaire")
	public String commandesCompletNonValiderTemporaire(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletNonValide();
	}
	@GetMapping("/commandesCompletEnCours")
	public String commandesCompletEnCours(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletEnCoursToFormatJson();
	}
	@GetMapping("/commandesCompletMesCommandes")
	public String commandesCompletMesCommandes(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroToken=(String)request.getParameter("numeroToken");
		return commandesCompletService.getCommandesCompletMesCommandesToFormatJson(numeroToken);
	}
	@GetMapping("/commandesCompletByIdCommandeMobile")
	public String getCommandesCompletByIdCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletByIdCommande((String)request.getParameter("idCommande"));
	}
	@GetMapping("/commandesCompletAdministrateur")
	public String commandesCompletAdministrateur(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandesCompletService.getCommandesCompletAdminstrateurToFormatJson();
	}
}

package com.example.demo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.CommandeParTableService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class CommandeParTableController {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	CommandeParTableService commandeParTableService=new CommandeParTableService();
	@GetMapping("commandeParTable")
	public String CommandeParTable(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numero=(String)request.getParameter("numeroCommande");
		if(!numero.equals("")) {
			return commandeParTableService.getCommandeParTableByNumeroCommande(numero);
		}
		return "";
	}
	@GetMapping("commandeParTableByIdCommande")
	public String commandeParTableByIdCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeParTableService.getCommandeParTableByIdCommande((String)request.getParameter("idCommande"));
	}
	@GetMapping("supprimerCommandeParTableByIdCommande")
	public String supprimerCommandeParTableByIdCommande(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return commandeParTableService.supprimerCommandeParTable((String)request.getParameter("idCommande"),true);
	}
}

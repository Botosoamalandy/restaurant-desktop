package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.entite.Json;
import com.restaurent.service.TokenService;
import com.restaurent.service.UtilisateurService;
import com.restaurent.service.UtilisateurSupprimerService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class UtilisateurController {
	UtilisateurService utilisateurService=new UtilisateurService();
	UtilisateurSupprimerService utilisateurSupprimerService=new UtilisateurSupprimerService();
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();

	@PostMapping("/login")
	public String login(@RequestBody HashMap<String,String> data) throws Exception {
		String nom=(String)data.get("nom"),motDePasse=data.get("motDePasse");
		return utilisateurService.login(nom, motDePasse,true);
	}
	
	@PostMapping("/loginAmdinstrateur")
	public String loginAmdinstrateur(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String nom=(String)data.get("nom"),motDePasse=data.get("motDePasse");
		return utilisateurService.loginAdministrateur(nom, motDePasse);
	}
	
	@PostMapping("/loginMobile")
	public String loginMobile(@RequestBody HashMap<String,String> data,HttpServletResponse response) throws Exception {
		String nom=(String)data.get("nom"),motDePasse=data.get("motDePasse");
		return utilisateurService.login(nom, motDePasse,false);
	}
	
	@GetMapping("/loginToken")
	public String loginToken(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String numeroToken=(String)request.getParameter("token");
		Json json=new Json();boolean validationToken=false;
		if(!numeroToken.equals("null")) {
			TokenService tokenService=new TokenService();
			validationToken=tokenService.verificationTokenSiValable(numeroToken);
		}
		json.put("validation",validationToken);
		return json.toString();
	}
	@PostMapping("/inscripition")
	public String inscription(@RequestBody HashMap<String,String> data) {
		String nom=(String)data.get("nom"),etatUtilisateur=(String)data.get("etatUtilisateur"),motDePasse=(String)data.get("motDePasse");
		return utilisateurService.insertionUtilisateurService(nom, etatUtilisateur, motDePasse);
	}
	@GetMapping("/listeUtilisateur")
	public String listeUtilisateur(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return utilisateurService.getListeUtilisateur();
	}
	@GetMapping("/supprimerUtilisateur")
	public String supprimerUtilisateur(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return utilisateurSupprimerService.supprimerUtilisateur((String)request.getParameter("idUtilisateur"),(String)request.getParameter("numeroToken"));
	}
	@GetMapping("/informationUtilisateur")
	public String informationUtilisateur(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		System.out.println("token :"+utilisateurService.getInformationUtilisateur((String)request.getParameter("numeroToken")));
		return utilisateurService.getInformationUtilisateur((String)request.getParameter("numeroToken"));
	}
	@PostMapping("/updateUtilisateur")
	public String updateUtilisateur(@RequestBody HashMap<String,String> data) throws Exception {
		String nom=(String)data.get("nom"),motDePasse=data.get("motDePasse"),numeroToken=data.get("numeroToken"),motDePasseActuel=data.get("motDePasseActuel");
		return utilisateurService.updateUtilisateur(nom, motDePasse, numeroToken,motDePasseActuel);
	}
	@PostMapping("/motDePasseOublier")
	public String motDePasseOublier(@RequestBody HashMap<String,String> data) throws Exception {
		String nom=(String)data.get("nom"),dateAjout=data.get("dateAjout"),nouveauMotDePasse=data.get("nouveauMotDePasse");
		return utilisateurService.motDePasseOublier(nom, dateAjout, nouveauMotDePasse);
	}
}

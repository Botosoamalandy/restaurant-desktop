package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.CategorieService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class CategorieController {
	
	@GetMapping("/categorie")
	public String Categorie(HttpServletResponse response) throws Exception {
		(new FonctionGeneraliser()).settingCorse(response, "json");
		CategorieService categorieService=new CategorieService();
		return categorieService.getCategorieToFormatJson();
	}
	
	@PostMapping("/insertionCategorie")
	public String insertionCategorie(@RequestBody HashMap<String,String> data) throws Exception {
		String categorie=(String)data.get("categorie");
		CategorieService categorieService=new CategorieService();
		return categorieService.insertionCategories(categorie);
	}
}

package com.example.demo;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.ProduitMobileServie;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class ProduitMobileController {
	ProduitMobileServie produitMobileServie=new ProduitMobileServie();
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	@GetMapping("/produitMobile")
	public String produitMobile(HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return produitMobileServie.getProduitMobile();
	}
}

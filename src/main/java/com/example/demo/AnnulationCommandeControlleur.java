package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.AnnulationCommandeService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class AnnulationCommandeControlleur {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	AnnulationCommandeService annulationCommandeService=new AnnulationCommandeService();
	
	@PostMapping("insertionAnnulationCommande")
	public String insertionAnnulationCommande(@RequestBody HashMap<String,String> data) {
		String idCommande=(String)data.get("idCommande");
		return annulationCommandeService.insertionAnnulationCommandes(idCommande,1);
	}
	
	@GetMapping("SupprimerCommandeAdministrateur")
	public String SupprimerCommandeAdministrateur(HttpServletRequest request,HttpServletResponse response) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		return annulationCommandeService.insertionAnnulationCommandesAdministrateur((String)request.getParameter("idCommande"));
	}
}

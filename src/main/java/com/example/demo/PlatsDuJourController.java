package com.example.demo;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.service.PlatsDuJourService;

@RestController
@CrossOrigin(origins = "http://localhost:8100")
public class PlatsDuJourController {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	PlatsDuJourService platsDuJourService=new PlatsDuJourService();
	
	@PostMapping("/insertionPlatsDuJour")
	public String insertionPlatsDuJour(@RequestBody HashMap<String,String> data) throws Exception {
		String idProduit=(String)data.get("idProduit"),tokenUtilisateur=data.get("tokenUtilisateur"),
				categorie=(String)data.get("categorie"),prixUnitaire=(String)data.get("prixUnitaire"),descriptions=(String)data.get("descriptions");
		return platsDuJourService.insertionPlatsDuJours(idProduit, tokenUtilisateur, categorie, prixUnitaire, descriptions);
	}
	
	@GetMapping("/supprimerPlatsDuJour")
	public String supprimerPlatsDuJour(HttpServletResponse response,HttpServletRequest request) throws Exception {
		fonctionGeneraliser.settingCorse(response, "json");
		String idPlatsDuJour=(String)request.getParameter("idPlatsDuJour");
		return platsDuJourService.supprimerPlatsDuJour(idPlatsDuJour);
	}
	
	@GetMapping("/tousLesPlatsDuJour")
	public String tousLesPlatsDuJour(HttpServletResponse response) throws Exception {
		return platsDuJourService.getPlatsDuJours();
	}
	
}

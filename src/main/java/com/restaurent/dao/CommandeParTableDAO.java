package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.CommandeParTable;

public class CommandeParTableDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	public CommandeParTable setTableauObjectToObject(CommandeParTable []elements) {
		CommandeParTable element=null;
		for(CommandeParTable elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public CommandeParTable[] getCommandeParTable(Connexion connexion,String requette) throws Exception {
		CommandeParTable objectTmp=new CommandeParTable();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		CommandeParTable [] objectFinale=vectorObject.toArray(new CommandeParTable[vectorObject.size()]);
		return objectFinale;
	}
	public CommandeParTable[] getCommandeParTableWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getCommandeParTable(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public CommandeParTable[] getCommandeParTableAPartNumroCommande(String numeroCommande) throws Exception {
		return getCommandeParTableWithConnexion("select * from commandeParTable where numeroCommande='"+numeroCommande+"' order by idcommandetable DESC");
	}
	public CommandeParTable[] getCommandeParTableAPartIdCommande(String idCommande) throws Exception {
		return getCommandeParTableWithConnexion("select * from commandeParTable where idCommande='"+idCommande+"' order by idcommandetable DESC");
	}
	public String deleteCommandeParTableByIdCommandeOrByIdInsuffisant(String idCommande,String idCommandeTable,boolean commandeTableOrCommande) throws Exception {
		if(!idCommande.equals("") && !idCommandeTable.equals("")) {
			if(commandeTableOrCommande) {
				return fonctionGeneraliser.updateOrDeleteWithConnexion("delete from commandetable where idcommande='"+idCommande+"'","Suppression avec succé");
			}else{
				return fonctionGeneraliser.updateOrDeleteWithConnexion("delete from commandetable where idcommandetable='"+idCommandeTable+"'","Suppression avec succé");
			}
		}
		return "Il y a une erreur d'information";
	}
}

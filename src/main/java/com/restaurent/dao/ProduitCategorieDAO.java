package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.ProduitCategorie;

public class ProduitCategorieDAO {
	public ProduitCategorie setTableauObjectToObject(ProduitCategorie []elements) {
		ProduitCategorie element=null;
		for(ProduitCategorie elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public ProduitCategorie[] getProduitCategorie(Connexion connexion,String requette) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		ProduitCategorie objectTmp=new ProduitCategorie();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		ProduitCategorie [] objectFinale=vectorObject.toArray(new ProduitCategorie[vectorObject.size()]);
		return objectFinale;
	}
	public ProduitCategorie[] getProduitCategorieWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getProduitCategorie(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public ProduitCategorie[] getAllProduitCategorie() throws Exception {
		return getProduitCategorieWithConnexion("select * from produitCategorie");
	}
	public ProduitCategorie[] getProduitCategoriePlatsDuJour() throws Exception {
		return getProduitCategorieWithConnexion("select * from produitCategorie where idProduit in (select idProduit from PlatsDuJour)");
	}
}

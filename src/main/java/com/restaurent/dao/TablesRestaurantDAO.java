package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.TablesRestaurant;

public class TablesRestaurantDAO {
	public TablesRestaurant setTableauObjectToObject(TablesRestaurant []elements) {
		TablesRestaurant element=null;
		for(TablesRestaurant elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public TablesRestaurant[] getTablesRestaurant(Connexion connexion,String requette) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		TablesRestaurant objectTmp=new TablesRestaurant();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		TablesRestaurant [] objectFinale=vectorObject.toArray(new TablesRestaurant[vectorObject.size()]);
		return objectFinale;
	}
	public TablesRestaurant[] getTablesRestaurantWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getTablesRestaurant(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public TablesRestaurant[] getAllTablesRestaurant() throws Exception {
		return getTablesRestaurantWithConnexion("select * from TablesRestaurant");
	}
}

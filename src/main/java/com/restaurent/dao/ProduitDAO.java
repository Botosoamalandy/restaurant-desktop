package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.Produit;

public class ProduitDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Produit setTableauObjectToObject(Produit []elements) {
		Produit element=null;
		for(Produit elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Produit[] getProduit(Connexion connexion,String requette) throws Exception {
		Produit objectTmp=new Produit();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		Produit [] objectFinale=vectorObject.toArray(new Produit[vectorObject.size()]);
		return objectFinale;
	}
	public Produit[] getProduitWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getProduit(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Produit[] getAllProduit() throws Exception {
		return getProduitWithConnexion("select * from produit");
	}
	public Produit[] getAllProduitSepiciale() throws Exception {
		return getProduitWithConnexion("SELECT idproduit,categorie.categorie as idcategorie, nomproduit, prixunitaire, descriptions, nomimage, tva, remise,quantite FROM produit join categorie  on produit.idcategorie=categorie.idcategorie");
	}
	public boolean verificationDeDoublement(Produit produit) throws Exception {
		Produit []produits=getAllProduit();int size=produits.length;
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		String []fieldNoninclus= {"idProduit","nomImage"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(produit, produits[i], fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public String insertionProduit(Produit produit) throws Exception {
		if(produit!=null && !verificationDeDoublement(produit)) {
			Connexion connexion=new Connexion();
			try {
				if(!verificationDeDoublement(produit)) {
					fonctionGeneraliser.insertAll(connexion, produit,"Produit","nextval('produitSequence')");
					return "insertion reussi";
				}
				return "Il y a une dedoublement";
			} catch (Exception e) {
				System.out.println(e.getMessage());
				e.printStackTrace();
				connexion.rollback();
				return e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Verifiez votre champs";
	}
	public String updateQuantiteProduit(Connexion connexion,String idProduit,String quantite) {
		if(!idProduit.equals("") && !quantite.equals("")) {
			try {
				fonctionGeneraliser.uptadeAndDelete(connexion,"UPDATE produit SET quantite="+quantite+"  WHERE idproduit='"+idProduit+"'");
				return "Quantie modifié";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une information ";
	}
	public String updateQuantiteEtDescriptionProduit(String idProduit,String quantite,String description) {
		if(!idProduit.equals("") && !quantite.equals("") && !description.equals("")) {
			try {
				fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE produit SET quantite="+quantite+",descriptions='"+description+"'  WHERE idproduit='"+idProduit+"'","");
				return "Modification avec succé";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une information ";
	}
}

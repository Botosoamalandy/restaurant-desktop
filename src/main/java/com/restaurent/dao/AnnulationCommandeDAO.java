package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.AnnulationCommande;

public class AnnulationCommandeDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public AnnulationCommande setTableauObjectToObject(AnnulationCommande []elements) {
		AnnulationCommande element=null;
		for(AnnulationCommande elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public AnnulationCommande[] getAnnulationCommandeWithConnexion(String requette) throws Exception {
		AnnulationCommande objectTmp=new AnnulationCommande();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new AnnulationCommande[vectorObject.size()]);
	}
	public AnnulationCommande[] getAllAnnulationCommande() throws Exception {
		return getAnnulationCommandeWithConnexion("select * from AnnulationCommande");
	}
	public String insertionAnnulationCommande(AnnulationCommande annulationCommande) {
		if(annulationCommande!=null) {
			try {
				fonctionGeneraliser.insertionWithConnexion(annulationCommande,"AnnulationCommande","nextval('annulationCommandeSequence')");
				return "Suppression avec succé";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une erreur";
	}
	public String updateAnnulationCommande(String idCommande) {
		if(!idCommande.equals("")) {
			try {
				fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE annulationcommande SET etatannulation=11 WHERE idcommande='"+idCommande+"'", "");
				return "Suppression avec succé";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une erreur";
	}
}

package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.Facture;

public class FactureDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Facture setTableauObjectToObject(Facture []elements) {
		Facture element=null;
		for(Facture elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Facture[] getFactureWithConnexion(String requette) throws Exception {
		Facture objectTmp=new Facture();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new Facture[vectorObject.size()]);
	}
	public Facture[] getAllFacture() throws Exception {
		return getFactureWithConnexion("select * from facture");
	}
	public Facture[] getFactureNonSupprimer() throws Exception {
		return getFactureWithConnexion("SELECT idfacture,commande.numeroCommande as idcommande, montantpayer, montanttotal, montantresteclients, etatpayement, datepayement, etatfacturepayement, datefacturepayement FROM facture join commande on facture.idCommande=commande.idCommande WHERE facture.etatfacturepayement=1");
	}
	public Facture[] getFactureAdministrateur() throws Exception {
		return getFactureWithConnexion("SELECT idfacture,commande.numeroCommande as idcommande, montantpayer, montanttotal, montantresteclients, etatpayement, datepayement, etatfacturepayement, datefacturepayement FROM facture join commande on facture.idCommande=commande.idCommande");
	}
	public String insertionFacture(Facture facture) {
		try {
			if(facture!=null) {
				fonctionGeneraliser.insertionWithConnexion(facture,"facture","nextval('factureSequence')");
				System.out.println("=>Facturation avec succé");
				return "Facturation avec succé";
			}
			return "Il y a une erreur d'information ";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String suppressionFacture(String idFacture,boolean caissier) {
		try {
			System.out.println("caissier :"+caissier);
			if(!caissier) {
				fonctionGeneraliser.updateOrDeleteWithConnexion("DELETE FROM facture WHERE idFacture='"+idFacture+"'","");
			}else {
				fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE facture SET etatfacturepayement=11 WHERE idFacture='"+idFacture+"'","");
			}
			return "Suppression avec succé";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}

package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.CommandeTable;

public class CommandeTableDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public CommandeTable setTableauObjectToObject(CommandeTable []elements) {
		CommandeTable element=null;
		for(CommandeTable elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public CommandeTable[] getCommandeTable(Connexion connexion,String requette) throws Exception {
		CommandeTable objectTmp=new CommandeTable();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		CommandeTable [] objectFinale=vectorObject.toArray(new CommandeTable[vectorObject.size()]);
		return objectFinale;
	}
	public CommandeTable[] getCommandeTableWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getCommandeTable(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public CommandeTable[] getAllCommandeTable() throws Exception {
		return getCommandeTableWithConnexion("select * from CommandeTable");
	}
	public CommandeTable[] getCommandeTableAPartidCommande(String idCommandes) throws Exception {
		return getCommandeTableWithConnexion("select * from CommandeTable where idCommande='"+idCommandes+"'");
	}
	public String insertionCommandeTable(CommandeTable commandeTable) throws Exception {
		if(commandeTable!=null) {
			Connexion connexion=new Connexion();
			try {
				fonctionGeneraliser.insertAll(connexion, commandeTable,"CommandeTable", "nextval('commandeTableSequence')");
				return "insertion reussi";
			} catch (Exception e) {
				throw new Exception("Erreur :"+e.getMessage());
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Il y a une champs vide ou cet informationexiste déjà";
	}
	public String supprimerEtUpdateCommandeTable(String idCommandeTable,int quanite,boolean update) throws Exception {
		if(!idCommandeTable.equals("") && quanite>0) {
			String requette="",resultat="";
			if(update) {
				requette="UPDATE commandetable SET nombredeproduit="+quanite+" WHERE idcommandetable='"+idCommandeTable+"'";
				resultat="Update reussi";
			}else {
				requette="DELETE FROM commandetable WHERE idcommandetable='"+idCommandeTable+"'";
				resultat="Suppression reussi";
			}
			fonctionGeneraliser.updateOrDeleteWithConnexion(requette,"");
			return resultat;
		}
		return "Information invalide";
	}
	public String supprimerCommandeTableAPartIdCommande(String idCommande) throws Exception {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("delete from commandetable WHERE idcommande='"+idCommande+"'","");
			return "Suppression des plats avec succé";
		} catch (Exception e) {
			return ""+e.getMessage();
		}
	}
}

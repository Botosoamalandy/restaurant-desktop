package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.CommandesComplet;

public class CommandesCompletDao {
	public CommandesComplet setTableauObjectToObject(CommandesComplet []elements) {
		CommandesComplet element=null;
		for(CommandesComplet elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public CommandesComplet[] getCommandesComplet(Connexion connexion,String requette) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		CommandesComplet objectTmp=new CommandesComplet();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		CommandesComplet [] objectFinale=vectorObject.toArray(new CommandesComplet[vectorObject.size()]);
		return objectFinale;
	}
	public CommandesComplet[] getCommandesCompletWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getCommandesComplet(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public CommandesComplet[] getAllCommandesComplet() throws Exception {
		return getCommandesCompletWithConnexion("SELECT * FROM commandescomplet");
	}
	public CommandesComplet getAllCommandesCompletAPArtIdCommande(String idCommande) throws Exception {
		return setTableauObjectToObject(getCommandesCompletWithConnexion("SELECT * FROM commandescomplet where idCommande='"+idCommande+"'"));
	}
	public CommandesComplet[] getCommandesCommpletSpecialeAdministrateur() throws Exception {
		return getCommandesCompletWithConnexion("select commande.idCommande,utilisateur.idUtilisateur,utilisateur.nomUtilisateur,utilisateur.motDePasse,utilisateur.etatUtilisateur,utilisateur.dateAjout,TablesRestaurant.idTablesRestaurant,TablesRestaurant.numeroTables,TablesRestaurant.typeDeTables,commande.numeroCommande,commande.numeroOriginale,commande.fermetureCommande,commande.etatCommande,commande.dateCommande from utilisateur join commande on commande.idUtilisateur=utilisateur.idUtilisateur join TablesRestaurant on TablesRestaurant.idTablesRestaurant=commande.idTablesRestaurant where commande.idCommande not in (select idCommande from AnnulationCommande where etatannulation=11)");
	}
	public CommandesComplet[] getCommandesCompletAPartIdUtilisateur(String idUtilisateur) throws Exception {
		return getCommandesCompletWithConnexion("SELECT * FROM commandescomplet where idUtilisateur='"+idUtilisateur+"' and idCommande not in (select idCommande from AnnulationCommande)");
	}
	public CommandesComplet[] getCommandesCompletAPartFermetureCommandeAndEtatCommande() throws Exception {
		return getCommandesCompletWithConnexion("SELECT * FROM commandescomplet where fermeturecommande=11 and etatcommande=1 and idCommande not in (select idCommande from AnnulationCommande) order by numeroOriginale DESC");
	}
	public CommandesComplet[] getCommandesCompletAPartFermetureCommandeAndEtatCommandeAndValidation() throws Exception {
		return getCommandesCompletWithConnexion("SELECT * FROM commandescomplet where fermeturecommande=11 and etatcommande=11 and idCommande not in (select idCommande from AnnulationCommande)");
	}
	public CommandesComplet[] getCommandesCompletAPartFermetureCommandeAndEtatCommandeAndEnCours() throws Exception {
		return getCommandesCompletWithConnexion("SELECT * FROM commandescomplet where fermeturecommande=1 and etatcommande=1 and idCommande not in (select idCommande from AnnulationCommande)");
	}
}

package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.Insuffisant;
import com.restaurent.entite.InsuffisantProduit;
import com.restaurent.service.CommandeService;
import com.restaurent.service.InsuffisantProduitService;

public class InsuffisantDao {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Insuffisant setTableauObjectToObject(Insuffisant []elements) {
		Insuffisant element=null;
		for(Insuffisant elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Insuffisant[] getInsuffisantWithConnexion(String requette) throws Exception {
		Insuffisant objectTmp=new Insuffisant();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new Insuffisant[vectorObject.size()]);
	}
	public Insuffisant[] getInsuffisant() throws Exception {
		return getInsuffisantWithConnexion("select * from Insuffisant");
	}
	public Insuffisant getInsuffisantAPartIdCommande(String idCommande) throws Exception {
		return setTableauObjectToObject(getInsuffisantWithConnexion("select * from Insuffisant where idcommande='"+idCommande+"'"));
	}
	public Insuffisant[] getInsuffisantAPartIdUtilisateur(String idUtilisateur) throws Exception {
		return getInsuffisantWithConnexion("SELECT * FROM insuffisant where idutilisateur='"+idUtilisateur+"' order by idInsuffisant DESC");
	}
	public boolean insertionInsuffisant(Connexion connexion,Insuffisant insuffisant) throws Exception {
		if(insuffisant!=null) {
			fonctionGeneraliser.insertAll(connexion, insuffisant,"Insuffisant","nextval('insuffisantSequence')");
			return true;
		}
		return false;
	}
	public String deleteInsuffiisant(String idInsuffisant) {
		if(!idInsuffisant.equals("")) {
			try {
				fonctionGeneraliser.updateOrDeleteWithConnexion("delete from insuffisant where idInsuffisant='"+idInsuffisant+"'","");
				return "Suppression avec succé";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une erreur d'information";
	}
	public String insertInsuffisantEtProduit(Insuffisant insuffisant,InsuffisantProduit []insuffisantProduits) throws Exception {
		int size=insuffisantProduits.length;
		if(insuffisant!=null && size>0) {
			InsuffisantProduitService insuffisantProduitService=new InsuffisantProduitService();
			Connexion connexion=new Connexion();
			try {
				insertionInsuffisant(connexion, insuffisant);
				for (int i = 0; i < size; i++) {
					insuffisantProduitService.insertionInsuffisantProduit(connexion, insuffisantProduits[i]);
				}
				connexion.commit();
				return "Commande insuffisant renvoi vers le serveur/serveuse";
			} catch (Exception e) {
				connexion.rollback();
				return e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.rollback();
				}
			}
		}
		return "Il y a une erreur d'information";
	}
	public String deleteInsuffiisantAndUpdateCommande(String idInsuffisant,String idCommande) throws Exception {
		if(!idInsuffisant.equals("") && !idCommande.equals("")) {
			Connexion connexion=new Connexion();
			try {
				CommandeService commandeService=new CommandeService();
				commandeService.updateEtatcommandes(idCommande);
				fonctionGeneraliser.uptadeAndDelete(connexion, "delete from insuffisant where idInsuffisant='"+idInsuffisant+"'");
				return "modification commande";
			} catch (Exception e) {
				connexion.rollback();
				return e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.rollback();
				}
			}
		}
		return "Il y a une erreur d'information";
	}
}

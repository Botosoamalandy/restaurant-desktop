package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.FactureCommande;
import com.restaurent.entite.Preparation;

public class PreparationDao {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Preparation setTableauObjectToObject(Preparation []elements) {
		Preparation element=null;
		for(Preparation elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Preparation[] getPreparationWithConnexion(String requette) throws Exception {
		Preparation objectTmp=new Preparation();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new Preparation[vectorObject.size()]);
	}
	public Preparation[] getAllPreparation() throws Exception {
		return getPreparationWithConnexion("select * from Preparation order by numeroPreparation DESC");
	}
	public Preparation[] getPreparationEnCours() throws Exception {
		return getPreparationWithConnexion("select * from Preparation where etatPreparation=1 order by numeroPreparation DESC");
	}
	public Preparation[] getPreparationPret() throws Exception {
		return getPreparationWithConnexion("select * from Preparation where etatPreparation=11 order by numeroPreparation DESC");
	}
	public Preparation[] getPreparationPretAPartIdUtilisateur(String idUtilisateur) throws Exception {
		return getPreparationWithConnexion("SELECT idpreparation,commande.idcommande,commande.numerocommande, numerotables, numeropreparation, nombredeplats, etatpreparation, datepreparation FROM preparation join commande on preparation.idcommande= commande.idcommande where commande.idUtilisateur='"+idUtilisateur+"' and preparation.etatPreparation=11 order by numeroPreparation DESC");
	}
	public Preparation[] getAllPreparationNonSupprimer() throws Exception {
		return getPreparationWithConnexion("SELECT idpreparation,commande.idcommande,commande.numerocommande, numerotables, numeropreparation, nombredeplats, etatpreparation, datepreparation FROM preparation join commande on preparation.idcommande= commande.idcommande where preparation.etatPreparation<22 order by numeroPreparation DESC");
	}
	public Preparation getPreparationAPartIdCommande(String idCommande) throws Exception {
		return setTableauObjectToObject(getPreparationWithConnexion("select * from Preparation where idCommande='"+idCommande+"'"));
	}
	public String insertionPreparation(Preparation preparation,FactureCommande factureCommande) throws Exception {
		if(preparation!=null) {
			Connexion connexion=new Connexion();
			try {
				FactureCommandeDAO factureCommandeDAO=new FactureCommandeDAO();
				factureCommandeDAO.insertionFactureCommande(connexion, factureCommande);
				fonctionGeneraliser.uptadeAndDelete(connexion,"UPDATE commande SET etatcommande=11 WHERE idcommande='"+preparation.getIdCommande()+"'");
				fonctionGeneraliser.insertAll(connexion, preparation,"Preparation","nextval('preparationSequence')");
				connexion.commit();
				return "Preparation des plats";
			} catch (Exception e) {
				connexion.rollback();
				return e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Il y a une erreur d'information";
	}
	public String updatePreparation(String idPreparation,String etatPreparation,String message) throws Exception {
		if(!etatPreparation.equals("") && !idPreparation.equals("")) {
			fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE preparation SET etatpreparation="+etatPreparation+" WHERE idpreparation='"+idPreparation+"'","");
			return message;
		}
		return "Il y a une erreur d'information";
	}
}

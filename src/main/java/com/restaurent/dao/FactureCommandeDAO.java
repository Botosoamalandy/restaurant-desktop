package com.restaurent.dao;

import com.restaurent.entite.FactureCommande;

public class FactureCommandeDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	public String insertionFactureCommande(Connexion connexion,FactureCommande factureCommande) throws Exception {
		if(factureCommande!=null) {
			fonctionGeneraliser.insertAll(connexion, factureCommande,"FactureCommande","nextval('factureCommandeSequence')");
			return "Insertion avec succé";
		}
		return "Il y a une erreur de saisi";
	}
}

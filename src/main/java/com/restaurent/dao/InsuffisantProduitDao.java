package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.InsuffisantProduit;
import com.restaurent.service.ProduitService;

public class InsuffisantProduitDao {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public InsuffisantProduit setTableauObjectToObject(InsuffisantProduit []elements) {
		InsuffisantProduit element=null;
		for(InsuffisantProduit elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public InsuffisantProduit[] getInsuffisantProduitWithConnexion(String requette) throws Exception {
		InsuffisantProduit objectTmp=new InsuffisantProduit();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new InsuffisantProduit[vectorObject.size()]);
	}
	public InsuffisantProduit[] getInsuffisantProduit() throws Exception {
		return getInsuffisantProduitWithConnexion("select * from InsuffisantProduit");
	}
	public String updateInsuffisantProduit(Connexion connexion,String idProduit,String quaniteRestant) throws Exception {
		if(!idProduit.equals("") && !quaniteRestant.equals("")) {
			fonctionGeneraliser.uptadeAndDelete(connexion, "UPDATE insuffisantproduit SET quantiterestant="+quaniteRestant+",etatinsuffisantproduit=1 WHERE idproduit='"+idProduit+"'");
		}
		return "Il y a une erreur d'information";
	}
	public boolean getVerificationDedoublement(InsuffisantProduit insuffisantProduit) throws Exception {
		InsuffisantProduit []insuffisantProduits=getInsuffisantProduit();int size=insuffisantProduits.length;
		String []fieldNoninclus= {"insuffisantProduit","idCommande"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(insuffisantProduit, insuffisantProduits[i], fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public boolean insertionInsuffisantProduit(Connexion connexion,InsuffisantProduit insuffisantProduit) throws Exception {
		if(insuffisantProduit!=null) {
			ProduitService produitService=new ProduitService();
			if(getVerificationDedoublement(insuffisantProduit)) {
				updateInsuffisantProduit(connexion, insuffisantProduit.getIdProduit(),""+insuffisantProduit.getQuantiteRestant());
			}else {
				fonctionGeneraliser.insertAll(connexion, insuffisantProduit,"InsuffisantProduit","nextval('insuffisantProduitSequence')");
			}
			produitService.updateQuantiteProduit(connexion,insuffisantProduit.getIdProduit(),""+insuffisantProduit.getQuantiteRestant());
			return true;
		}
		return false;
	}
}

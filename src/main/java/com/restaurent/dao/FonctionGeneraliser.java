package com.restaurent.dao;

import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Base64;
import java.util.Vector;

import javax.servlet.http.HttpServletResponse;
import com.restaurent.entite.Dates;

public class FonctionGeneraliser {
	public String caseT(String mot) {
    	String lm="";
    	String reste=mot.substring(1);
    	char s=mot.charAt(0);
    	String m=""+s;
    	lm=m.toUpperCase()+reste;
    	return lm;
    }
	@SuppressWarnings({ "unchecked", "deprecation" })
	public Vector<Object> find(Connexion connexion,Object object, String requette) throws Exception{
		Vector<Object> m=new Vector<Object>();
		Statement statement =connexion.createStatement();
		try {
			ResultSet resultSet = statement.executeQuery(requette);
			//System.out.println(requette);
			Field []field=object.getClass().getDeclaredFields();
			@SuppressWarnings("rawtypes")
			Class classe=object.getClass();
			while(resultSet.next()) {
				Object objectTemporaire=classe.newInstance();
				for(int i=0;i<field.length;i++) {
					Method methodeTemporaire=classe.getMethod("get"+caseT(field[i].getName()));
					Object objectInvokeTemporaire=methodeTemporaire.invoke(object);
					if(objectInvokeTemporaire instanceof Integer) {
						Method methode=classe.getMethod("set"+caseT(field[i].getName()),int.class);
						methode.invoke(objectTemporaire,resultSet.getInt(i+1));
					}
					if(objectInvokeTemporaire instanceof String) {
						Method methode=classe.getMethod("set"+caseT(field[i].getName()),String.class);
						methode.invoke(objectTemporaire,resultSet.getString(i+1));
					}
					if(objectInvokeTemporaire instanceof Double) {
						Method methode=classe.getMethod("set"+caseT(field[i].getName()),double.class);
						methode.invoke(objectTemporaire,resultSet.getDouble(i+1));
					}
					if(objectInvokeTemporaire instanceof Dates) {
						Method methode=classe.getMethod("set"+caseT(field[i].getName()),String.class);
						methode.invoke(objectTemporaire,resultSet.getString(i+1));
					}
				}
				m.add(objectTemporaire);
			}
		}catch (Exception e) {
			statement.execute(" rollback");
			throw new Exception("Une erreur d'insertion = "+e.getMessage());
		}finally {
			statement.close();
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
		return m;
	}
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public void insertAll(Connexion connexion,Object object,String table,String sequence) throws Exception {
		int nombreUpdate = 0;
		String nomTable=table;
		Class classe=object.getClass();
		Field[] field=object.getClass().getDeclaredFields();
		String sql="";
		for(int i=0;i<field.length;i++) {
			Method methodet=classe.getMethod("get"+caseT(field[i].getName()));
			Object objectTemporaire= methodet.invoke(object);
			if(objectTemporaire instanceof String) {
				if(objectTemporaire.equals("SYSDATE")) {
					sql=sql+objectTemporaire;
				}else if(objectTemporaire.equals(sequence)) {
					sql=sql+objectTemporaire;
				}else {
					String op="'"+objectTemporaire+"'";
					sql=sql+op;
				}
			}else if(objectTemporaire instanceof Dates) {
				Dates dates=(Dates)objectTemporaire;
				sql=sql+"'"+dates.getDates()+"'";
			}else {
				sql=sql+objectTemporaire;
			}
			if(i<field.length-1) {
				sql=sql+" , ";
			}
		}
		Statement statement=connexion.createStatement();
		//System.out.println("INSERT INTO "+nomTable+" values("+sql+")");				
		try {
			nombreUpdate=statement.executeUpdate("INSERT INTO "+nomTable+" values("+sql+")");
			statement.execute("COMMIT");	
		} catch (Exception e) {
			statement.execute(" rollback");
			throw new Exception("Une erreur d'insertion = "+e.getMessage());
		}
	}
	@SuppressWarnings("unused")
	public void insertTemporaire(Connexion connexion,String requette) throws Exception {
		Statement statement=connexion.createStatement();
		//System.out.println("INSERT INTO "+nomTable+" values("+sql+")");				
		try {
			int nombreUpdate=statement.executeUpdate(requette);
			statement.execute("COMMIT");	
		} catch (Exception e) {
			statement.execute(" rollback");
			throw new Exception("Une erreur d'insertion = "+e.getMessage());
		}
	}
	public void uptadeAndDelete(Connexion connexion,String requette) throws Exception{
		Statement statement=connexion.createStatement();
		@SuppressWarnings("unused")
		int nombreUpdate = 0;
		//System.out.println(requette);
		try {
			nombreUpdate=statement.executeUpdate(requette);
			statement.execute("COMMIT");	
		}catch (Exception e) {
			statement.execute(" rollback");
			throw new Exception("Une erreur d'update = "+e.getMessage());
		}finally {
			statement.close(); 
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Vector<Object> findWithConnexion(String requette,Object object) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return find(connexion, object, requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public String insertionWithConnexion(Object object,String table,String sequence) throws Exception {
		Connexion connexion=new Connexion();
		try {
			insertAll(connexion,object,table,sequence);
			connexion.commit();
			return "insertion reussi";
		} catch (Exception e) {
			connexion.rollback();
			System.out.println(e.getMessage());
			return e.getMessage();
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public String updateOrDeleteWithConnexion(String requete,String message) throws Exception {
		Connexion connexion=new Connexion();
		try {
			uptadeAndDelete(connexion, requete);
			connexion.commit();
			return message;
		} catch (Exception e) {
			connexion.rollback();
			throw new Exception(e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public void settingCorse(HttpServletResponse response,String contentType) {
		response.setHeader("Access-Control-Allow-Origin", "*");
		response.setHeader("Access-Control-Allow-Methods", "GET,POST, DELETE, PUT");
		response.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
		response.setContentType(contentType+"/html");
	}
	public boolean verificationField(String field,String []fieldNoninclus) {
		int size=fieldNoninclus.length;
		for (int i = 0; i < size; i++) {
			if(field.equalsIgnoreCase(fieldNoninclus[i])) {
				//System.out.println("field :"+field+" = "+fieldNoninclus[i]+" :fieldNoninclus");
				return true;
			}
		}
		return false;
	}
	@SuppressWarnings({ "unused", "rawtypes", "unchecked" })
	public boolean getVerificationIfObjectEqualsOrNot(Object object,Object object2,String []fieldNoninclus) throws Exception {
		if(object.getClass().getName().equals(object2.getClass().getName())) {
			boolean verification=false;
			Class classe=object.getClass(),classe2=object2.getClass();
			Field[] field=object.getClass().getDeclaredFields(),field2=object2.getClass().getDeclaredFields();
			int size=field.length,size2=fieldNoninclus.length;
			for(int i=0;i<size;i++) {
				if(!verificationField(field[i].getName(),fieldNoninclus)) {
					//object
					Method methodet=classe.getMethod("get"+caseT(field[i].getName()));
					Object objectTemporaire= methodet.invoke(object);
					
					//object2
					Method methodet2=classe2.getMethod("get"+caseT(field2[i].getName()));
					Object objectTemporaire2= methodet2.invoke(object2);
					
					if(objectTemporaire instanceof String) {
						String object1String=objectTemporaire.toString(),object2String=objectTemporaire2.toString();
						if(!object1String.equalsIgnoreCase(object2String)) {
							return false;
						}
					}else {
						if(!objectTemporaire.equals(objectTemporaire2)){
							return false;
						}
					}
				}
			}
			return true;
		}
		return false;
	}
	public double changeStringToDouble(String valuer) {
		try {
			return Double.parseDouble(valuer);
		} catch (Exception e) {
			return 0;
		}
	}
	public int changeStringToInt(String valuer) {
		try {
			return Integer.parseInt(valuer);
		} catch (Exception e) {
			return 0;
		}
	}
	public String getTypeImage(String nomImage) {
		String []strings=nomImage.split("\\.");int size=strings.length;
		if(size>1) {
			return strings[size-1];
		}
		return "";
	}
	public String setImageToBase64(String imagePath,String nomImage) throws Exception {
		FileInputStream fileInputStream=new FileInputStream(imagePath);
		try {
			byte[] data=fileInputStream.readAllBytes();
			return "data:image/"+getTypeImage(nomImage)+";base64,"+Base64.getEncoder().encodeToString(data);
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			fileInputStream.close();
		}
		return "";
	}
}

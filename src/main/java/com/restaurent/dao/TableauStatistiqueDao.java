package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.TableauStatistiqueCommande;
import com.restaurent.entite.TableauStatistiqueProduit;

public class TableauStatistiqueDao {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public TableauStatistiqueProduit setTableauObjectToObject(TableauStatistiqueProduit []elements) {
		TableauStatistiqueProduit element=null;
		for(TableauStatistiqueProduit elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public TableauStatistiqueCommande setTableauObjectToObject(TableauStatistiqueCommande []elements) {
		TableauStatistiqueCommande element=null;
		for(TableauStatistiqueCommande elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public TableauStatistiqueProduit[] getTableauStatistiqueProduitWithConnexion(String requette) throws Exception {
		TableauStatistiqueProduit objectTmp=new TableauStatistiqueProduit();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new TableauStatistiqueProduit[vectorObject.size()]);
	}
	public TableauStatistiqueCommande[] getTableauStatistiqueCommandeWithConnexion(String requette) throws Exception {
		TableauStatistiqueCommande objectTmp=new TableauStatistiqueCommande();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new TableauStatistiqueCommande[vectorObject.size()]);
	}
	public TableauStatistiqueCommande[] getTableauStatistiqueCommande(double annee,double mois,int anneeOuMoisOuJour) throws Exception {
		String requette="";
		if(anneeOuMoisOuJour==1) {//jours
			requette="select numeroCommande,'commande ' as label,sum(prixunitaire) as total,EXTRACT(YEAR FROM datecommande) as annee,EXTRACT(MONTH FROM datecommande) as mois, EXTRACT(DAY FROM datecommande) as jours from commandepartable where etatCommande=11 and EXTRACT(YEAR FROM datecommande)="+((int)annee)+" and EXTRACT(MONTH FROM datecommande)="+((int)mois)+" GROUP BY numeroCommande,annee,mois,jours order by annee,mois,jours ASC";
		}else if(anneeOuMoisOuJour==2) {//mois
			requette="select numeroCommande,'commande ' as label,sum(prixunitaire) as total,EXTRACT(YEAR FROM datecommande) as annee,EXTRACT(MONTH FROM datecommande) as mois, 0.0 as jours from commandepartable where etatCommande=11 and EXTRACT(YEAR FROM datecommande)="+((int)annee)+" GROUP BY numeroCommande,annee,mois order by annee,mois ASC";
		}else {//année
			requette="select numeroCommande,'commande ' as label,sum(prixunitaire) as total,EXTRACT(YEAR FROM datecommande) as annee,0.0 as mois, 0.0 as jours from commandepartable where etatCommande=11 GROUP BY numeroCommande,annee order by annee ASC";
		}
		return getTableauStatistiqueCommandeWithConnexion(requette);
	}
	
	public TableauStatistiqueCommande[] getTableauStatistiquePayement(double annee,double mois,int anneeOuMoisOuJour) throws Exception {
		String requette="";
		if(anneeOuMoisOuJour==1) {//jours
			requette="select commande.numeroCommande as numeroCommande,'Payement' as label,sum(montantTotal) as total,EXTRACT(YEAR FROM datePayement) as annee,EXTRACT(MONTH FROM datePayement) as mois, EXTRACT(DAY FROM datePayement) as jours from payement join commande on payement.idCommande=commande.idCommande where etatPayement=11 and EXTRACT(YEAR FROM datePayement)="+annee+" and EXTRACT(MONTH FROM datePayement)="+mois+" GROUP BY numeroCommande,annee,mois,jours order by annee,mois,jours ASC";
		}else if(anneeOuMoisOuJour==2) {//mois
			requette="select commande.numeroCommande as numeroCommande,'Payement' as label,sum(montantTotal) as total,EXTRACT(YEAR FROM datePayement) as annee,EXTRACT(MONTH FROM datePayement) as mois,0.0 as jours from payement join commande on payement.idCommande=commande.idCommande where etatPayement=11 and EXTRACT(YEAR FROM datePayement)="+annee+" GROUP BY numeroCommande,annee,mois order by annee,mois ASC";
		}else {//année
			requette="select commande.numeroCommande as numeroCommande,'Payement' as label,sum(montantTotal) as total,EXTRACT(YEAR FROM datePayement) as annee,0.0 as mois,0.0 as jours from payement join commande on payement.idCommande=commande.idCommande where etatPayement=11 GROUP BY numeroCommande,annee order by annee ASC";
		}
		return getTableauStatistiqueCommandeWithConnexion(requette);
	}
	
	public TableauStatistiqueCommande[] getTableauStatistiquePlats(double annee,double mois,int anneeOuMoisOuJour) throws Exception {
		String requette="";
		if(anneeOuMoisOuJour==1) {//jours
			requette="SELECT nomproduit as numerCommande,'Plats' as label,sum(nombredeproduit*prixUnitaire) as total,EXTRACT(YEAR FROM dateCommande) as annee,EXTRACT(MONTH FROM datecommande) as mois, EXTRACT(DAY FROM datecommande) as jours  FROM commandepartable where etatCommande>1 and EXTRACT(YEAR FROM datecommande)="+annee+" and EXTRACT(MONTH FROM datecommande)="+mois+" GROUP BY nomproduit,annee,mois,jours order by nomproduit,annee,mois,jours ASC";
		}else if(anneeOuMoisOuJour==2) {//mois
			requette="SELECT nomproduit as numerCommande,'Plats' as label,sum(nombredeproduit*prixUnitaire) as total,EXTRACT(YEAR FROM dateCommande) as annee,EXTRACT(MONTH FROM datecommande) as mois,0.0 as jours  FROM commandepartable where etatCommande>1 and EXTRACT(YEAR FROM datecommande)="+annee+" GROUP BY nomproduit,annee,mois order by annee,mois,nomproduit ASC";
		}else {//année
			requette="SELECT nomproduit as numerCommande,'Plats' as label,sum(nombredeproduit*prixUnitaire) as total,EXTRACT(YEAR FROM dateCommande) as annee,0.0 as mois,0.0 as jours  FROM commandepartable where etatCommande>1 GROUP BY nomproduit,annee,mois order by annee,nomproduit ASC";
		}
		return getTableauStatistiqueCommandeWithConnexion(requette);
	}
	
}

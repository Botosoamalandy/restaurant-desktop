package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.Commande;
import com.restaurent.entite.FactureCommande;

public class CommandeDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	public Commande setTableauObjectToObject(Commande []elements) {
		Commande element=null;
		for(Commande elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Commande[] getCommande(Connexion connexion,String requette) throws Exception {
		Commande objectTmp=new Commande();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		Commande [] objectFinale=vectorObject.toArray(new Commande[vectorObject.size()]);
		return objectFinale;
	}
	public Commande[] getCommandeWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getCommande(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Commande[] getAllCommande() throws Exception {
		return getCommandeWithConnexion("select * from commande where idCommande not in (select idCommande from AnnulationCommande)");
	}
	public Commande[] getAllCommandeDesc() throws Exception {
		return getCommandeWithConnexion("select * from commande where idCommande not in (select idCommande from AnnulationCommande) ORDER by numeroOriginale DESC");
	}
	public Commande[] getAllCommandeNonFermerDesc() throws Exception {
		return getCommandeWithConnexion("select * from commande where fermetureCommande=1 and idCommande not in (select idCommande from AnnulationCommande) ORDER by numeroOriginale DESC");
	}
	public Commande getCommandeAPartCommande(String numeroCommandes) throws Exception {
		return setTableauObjectToObject(getCommandeWithConnexion("select * from commande where numeroCommande='"+numeroCommandes+"' and idCommande not in (select idCommande from AnnulationCommande)"));
	}
	public Commande[] getAllCommandesNonFermerDescExact() throws Exception {
		return getCommandeWithConnexion("select commande.idCommande,utilisateur.nomUtilisateur,TablesRestaurant.numeroTables,commande.numeroCommande,commande.numeroOriginale,commande.fermetureCommande,commande.etatCommande,commande.dateCommande from utilisateur join commande on utilisateur.idUtilisateur=commande.idUtilisateur join TablesRestaurant on TablesRestaurant.idTablesRestaurant=commande.idTablesRestaurant where commande.fermetureCommande=1 and idCommande not in (select idCommande from AnnulationCommande) ORDER by commande.numeroOriginale DESC");
	}
	public Commande[] getMesCommandes(String idUtilisateur) throws Exception {
		return getCommandeWithConnexion("select commande.idCommande,utilisateur.nomUtilisateur as idutilisateur,TablesRestaurant.numeroTables as idtablesrestaurant,commande.numeroCommande,commande.numeroOriginale,commande.fermetureCommande,commande.etatCommande,commande.dateCommande from utilisateur join commande on utilisateur.idUtilisateur=commande.idUtilisateur join TablesRestaurant on TablesRestaurant.idTablesRestaurant=commande.idTablesRestaurant where commande.fermetureCommande=1 and utilisateur.idUtilisateur='"+idUtilisateur+"' and idCommande not in (select idCommande from AnnulationCommande)  ORDER by commande.numeroOriginale DESC");
	}
	public Commande[] getMesCommandeAPartUtilisateur(String idUtilisateur) throws Exception {
		return getCommandeWithConnexion("select commande.idCommande,utilisateur.nomUtilisateur as idutilisateur,TablesRestaurant.numeroTables as idtablesrestaurant,commande.numeroCommande,commande.numeroOriginale,commande.fermetureCommande,commande.etatCommande,commande.dateCommande from utilisateur join commande on utilisateur.idUtilisateur=commande.idUtilisateur join TablesRestaurant on TablesRestaurant.idTablesRestaurant=commande.idTablesRestaurant where  utilisateur.idUtilisateur='"+idUtilisateur+"' and idCommande not in (select idCommande from AnnulationCommande)  ORDER by commande.numeroOriginale DESC");
	}
	public Commande[] getCommandeValider() throws Exception {
		return getCommandeWithConnexion("SELECT * FROM commande where etatcommande=11 and idCommande not in (select idCommande from AnnulationCommande) ORDER by numeroOriginale DESC");
	}
	public Commande getCommandeAPartIdCommande(String idCommande) throws Exception {
		return setTableauObjectToObject(getCommandeWithConnexion("SELECT * FROM commande where idCommande='"+idCommande+"' and idCommande not in (select idCommande from AnnulationCommande)"));
	}
	public Commande getCommandeAPartIdCommandeSpeciale(String idCommande) throws Exception {
		return setTableauObjectToObject(getCommandeWithConnexion("SELECT idcommande,utilisateur.nomUtilisateur as idutilisateur, idtablesrestaurant, numerocommande, numerooriginale, fermeturecommande, etatcommande, datecommande FROM commande join utilisateur on commande.idUtilisateur=utilisateur.idUtilisateur where commande.idCommande='4' and idcommande not in(select idcommande from annulationCommande)"));
	}
	public String updateFermetureCommande(String numeroCommandes,int fermeturecommande) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE commande SET fermeturecommande="+fermeturecommande+" WHERE numerocommande='"+numeroCommandes+"'","Validation avec succé");
			return "Validation avec succé";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String updateCommandeToEtatValidation(Connexion connexion,String idCommande) {
		try {
			fonctionGeneraliser.uptadeAndDelete(connexion,"UPDATE commande SET etatcommande=11 WHERE idcommande='"+idCommande+"'");
			return "Annulation reussi";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String updateEtatCommande(Connexion connexion,String idCommande) throws Exception {
		try {
			fonctionGeneraliser.uptadeAndDelete(connexion, "UPDATE commande SET etatcommande=22 WHERE idcommande='"+idCommande+"'");
			return "Update reussi";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String updateEtatcommandeAndInsertFactureCommande(String idCommande,int etatcommande,FactureCommande factureCommande) throws Exception {
		Connexion connexion=new Connexion();
		try {
			FactureCommandeDAO factureCommandeDAO=new FactureCommandeDAO();
			factureCommandeDAO.insertionFactureCommande(connexion, factureCommande);
			fonctionGeneraliser.uptadeAndDelete(connexion,"UPDATE commande SET etatcommande="+etatcommande+" WHERE idcommande='"+idCommande+"'");
			connexion.commit();
			return "Votre commande est validé";
		} catch (Exception e) {
			connexion.rollback();
			return e.getMessage();
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public void updateEtatcommandes(Connexion connexion,String idCommande,int etatcommande) throws Exception {
		fonctionGeneraliser.uptadeAndDelete(connexion, "UPDATE commande SET etatcommande="+etatcommande+" WHERE idcommande='"+idCommande+"'");
	}
	public boolean verificationDedoublement(Commande commande) throws Exception {
		Commande []commandes=getAllCommande();int size=commandes.length;
		String []fieldNoninclus= {"idCommande"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(commande, commandes[i],fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public String insertionCommande(Commande commande) throws Exception {
		if(commande!=null && !verificationDedoublement(commande)) {
			Connexion connexion=new Connexion();
			try {
				fonctionGeneraliser.insertAll(connexion, commande,"Commande","nextval('commandeSequence')");
				return "insertion commande reussi";
			} catch (Exception e) {
				throw new Exception("Erreur :"+e.getMessage());
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Il y a une champs vide";
	}
}

package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.Token;

public class TokenDAO {
	public Token setTableauObjectToObject(Token []elements) {
		Token element=null;
		for(Token elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public String setTokenParametreUrlToTokenNormal(String token) {
		String resultat="";String []strings=token.split(" ");int size=strings.length;
		for (int i = 0; i <size; i++) {
			resultat=resultat+strings[i];
			if(i<size-1) {
				resultat=resultat+"+";
			}
		}
		return resultat;
	}
	public Token[] getToken(Connexion connexion,String requette) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		Token objectTmp=new Token();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		Token [] objectFinale=vectorObject.toArray(new Token[vectorObject.size()]);
		return objectFinale;
	}
	public Token[] getTokenWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getToken(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Token getTokenAPartidToken(String idTokens) throws Exception {
		return setTableauObjectToObject(getTokenWithConnexion("select * from Token where idToken='"+idTokens+"'"));
	}
	public Token[] getAllToken() throws Exception {
		return getTokenWithConnexion("select * from Token");
	}
	public Token getTokenAPartNumeroTokens(String numeroTokens) throws Exception {
		return setTableauObjectToObject(getTokenWithConnexion("select * from Token where numeroToken='"+setTokenParametreUrlToTokenNormal(numeroTokens)+"'"));
	}
	public void insertionTokens(Token token) throws Exception {
		if(token!=null) {
			Connexion connexion=new Connexion();
			FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
			try {
				fonctionGeneraliser.uptadeAndDelete(connexion, "delete from token where idUtilisateur='"+token.getIdUtilisateur()+"'");
				fonctionGeneraliser.insertAll(connexion,token,"token","nextval('tokenSequence')");
				connexion.commit();
			} catch (Exception e) {
				connexion.rollback();
				throw new Exception("Erreur:"+e.getMessage());
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
	}
	public void deleteToken(Token token) throws Exception {
		if(token!=null) {
			Connexion connexion=new Connexion();
			FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
			try {
				fonctionGeneraliser.updateOrDeleteWithConnexion("delete from token idToken='"+token.getIdToken()+"'","Token supprimé");
			} catch (Exception e) {
				connexion.rollback();
				throw new Exception("Erreur:"+e.getMessage());
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
	}
}

package com.restaurent.dao;


import com.restaurent.entite.UtilisateurSupprimer;

public class UtilisateurSupprimerDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public String insertionUtilisateurSupprimer(UtilisateurSupprimer utilisateurSupprimer) {
		try {
			fonctionGeneraliser.insertionWithConnexion(utilisateurSupprimer,"UtilisateurSupprimer","nextval('utilisateurSupprimerSequence')");
			return "Suppression avec succé";
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return e.getMessage();
		}
	}
	public String suppressionUtilisateurSupprimer(String idUtilisateur) {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("Delete from UtilisateurSupprimer where idUtilisateur='"+idUtilisateur+"'","");
			return "Suppression avec succé";
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return e.getMessage();
		}
	}
}

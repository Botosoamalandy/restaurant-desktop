package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.Utilisateur;

public class UtilisateurDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Utilisateur setTableauObjectToObject(Utilisateur []elements) {
		Utilisateur element=null;
		for(Utilisateur elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Utilisateur[] getUtilisateur(Connexion connexion,String requette) throws Exception {
		Utilisateur objectTmp=new Utilisateur();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		Utilisateur [] objectFinale=vectorObject.toArray(new Utilisateur[vectorObject.size()]);
		return objectFinale;
	}
	public Utilisateur[] getUtilisateurWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getUtilisateur(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Utilisateur[] getAllUtilisateur() throws Exception {
		return getUtilisateurWithConnexion("select * from utilisateur where idUtilisateur not in (select idUtilisateur from UtilisateurSupprimer)");
	}
	public Utilisateur getUtilisateurAPartIdUtilisateur(String idUtilisateur) throws Exception {
		return setTableauObjectToObject(getUtilisateurWithConnexion("select * from utilisateur where idUtilisateur='"+idUtilisateur+"' and idUtilisateur not in (select idUtilisateur from UtilisateurSupprimer)"));
	}
	public Utilisateur getUtilisateurAPartNomUtilisateur(String nomUtilisateur) throws Exception {
		return setTableauObjectToObject(getUtilisateurWithConnexion("select * from utilisateur where nomutilisateur='"+nomUtilisateur+"' and idUtilisateur not in (select idUtilisateur from UtilisateurSupprimer)"));
	}
	public String insertionUtilisateur(Utilisateur utilisateur) throws Exception {
		if(utilisateur!=null) {
			Connexion connexion=new Connexion();
			try {
				fonctionGeneraliser.insertAll(connexion, utilisateur,"Utilisateur","nextval('utilisateurSequence')");
				return "insertion utilisateur reussi";
			} catch (Exception e) {
				connexion.rollback();
				return "Insertion utilisateur echoué : "+e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Insertion utilisateur echoué";
	}
	public String updateInformationUtilisateur(String nom,String motDePasse,String idUtilisateur) {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE utilisateur SET nomutilisateur='"+nom+"', motdepasse='"+motDePasse+"' WHERE idutilisateur='"+idUtilisateur+"'","");
			return "Modification avec succé";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String updateMotDePasseUtilisateur(String motDePasse,String idUtilisateur) {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE utilisateur SET  motdepasse='"+motDePasse+"' WHERE idutilisateur='"+idUtilisateur+"'","");
			return "Modification avec succé";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}

package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.Payement;
import com.restaurent.service.CommandeService;

public class PayementDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public Payement setTableauObjectToObject(Payement []elements) {
		Payement element=null;
		for(Payement elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Payement[] getPayementWithConnexion(String requette) throws Exception {
		Payement objectTmp=new Payement();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new Payement[vectorObject.size()]);
	}
	public Payement[] getAllPayement() throws Exception {
		return getPayementWithConnexion("select * from Payement");
	}
	public Payement[] getPayementSpecialeAPartIdCommande(String idCommande) throws Exception {
		return getPayementWithConnexion("SELECT utilisateur.nomUtilisateur as idPayement, commande.numeroCommande as idCommande, montantpayer, montanttotal, montantresteclients,etatpayement, datepayement FROM payement join commande on payement.idCommande =commande.idCommande join utilisateur on commande.idUtilisateur=utilisateur.idUtilisateur where commande.idCommande='"+idCommande+"'and etatPayement <22");
	}
	public Payement[] getAllPayementWithNumeroCommande() throws Exception {
		return getPayementWithConnexion("SELECT idpayement,commande.numerocommande, montantpayer, montanttotal, montantresteclients, etatpayement, datepayement FROM payement join commande on payement.idCommande=commande.idCommande where etatPayement <22");
	}
	public Payement[] getAllPayementWithNumeroCommandeAdministrateur() throws Exception {
		return getPayementWithConnexion("SELECT idpayement,commande.numerocommande, montantpayer, montanttotal, montantresteclients, etatpayement, datepayement FROM payement join commande on payement.idCommande=commande.idCommande");
	}
	public Payement getPayementAPartIdPayement(String idPayement) throws Exception {
		return setTableauObjectToObject(getPayementWithConnexion("select * from Payement where idPayement='"+idPayement+"'"));
	}
	public Payement[] getPayementAPartIdCommande(String idCommande) throws Exception {
		return getPayementWithConnexion("select * from Payement where idCommande='"+idCommande+"' and etatPayement <22");
	}
	public String updatePayementToEtatAnnulation(String idCommande,String idPayement,boolean parPayement) throws Exception {
		Connexion connexion=new Connexion();
		CommandeService commandeService=new CommandeService();
		try {
			commandeService.updateCommandeToEtatValidation(connexion, idCommande);
			if(!parPayement) {
				fonctionGeneraliser.uptadeAndDelete(connexion,"DELETE FROM payement WHERE idCommande='"+idCommande+"'");
			}else {
				fonctionGeneraliser.uptadeAndDelete(connexion,"DELETE FROM payement WHERE idPayement='"+idPayement+"'");
			}
			connexion.commit();
			return "Annulation reussi";
		} catch (Exception e) {
			connexion.rollback();
			return e.getMessage();
		}finally {
			if (connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public String deletePayement(String idPayement) throws Exception {
		try {
			if(!idPayement.equals("")) {
				fonctionGeneraliser.updateOrDeleteWithConnexion("DELETE FROM payement WHERE idPayement='"+idPayement+"'","");
				return "Annulation reussi";
			}else {
				return "Information invalide";
			}
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String updatePayementToEtatSuppression(String idCommande) {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("UPDATE payement SET etatpayement=22 WHERE idcommande='"+idCommande+"'","");
			return "Suppression reussi";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
	public String insertionPayement(Payement payement,boolean confirmation) {
		if(payement!=null) {
			Connexion connexion=new Connexion();
			try {
				CommandeService commandeService=new CommandeService();
				if(confirmation) {
					commandeService.updateEtatCommande(connexion, payement.getIdCommande());
				}
				fonctionGeneraliser.insertAll(connexion,payement,"Payement","nextval('payementSequence')");
				return "Payement reussi";
			} catch (Exception e) {
				System.out.println(e.getMessage());
				return e.getMessage();
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Il y a une information incorrect";
	}

}

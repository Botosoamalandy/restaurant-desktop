package com.restaurent.dao;

import java.sql.DriverManager;
import java.sql.Statement;

public class Connexion {
	java.sql.Connection connection;
	public java.sql.Connection getConnection() throws Exception {
		Class.forName("org.postgresql.Driver");
		return DriverManager.getConnection("jdbc:postgresql://localhost:5432/Restaurant","postgres","root");
	}
	public Statement createStatement() throws Exception {
		Statement statement=getConnection().createStatement();
		return statement;
	}
	public void commit() throws Exception {
		createStatement().execute("COMMIT");
	}
	public void rollback() throws Exception {
		createStatement().execute("ROLLBACK");
	}
	public void closeConnexion() {
		try {
			if(connection!=null && !connection.isClosed()) {
				connection.close();
			}
			
		} catch (Exception e) {
			System.out.println("erreur close connection");
		}
	}
}

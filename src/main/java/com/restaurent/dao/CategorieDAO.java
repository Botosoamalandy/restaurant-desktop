package com.restaurent.dao;

import java.util.Vector;
import com.restaurent.entite.Categorie;

public class CategorieDAO {
	public Categorie setTableauObjectToObject(Categorie []elements) {
		Categorie element=null;
		for(Categorie elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public Categorie[] getCategorie(Connexion connexion,String requette) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		Categorie objectTmp=new Categorie();
		Vector<Object> vectorObject=fonctionGeneraliser.find(connexion, objectTmp,requette);
		Categorie [] objectFinale=vectorObject.toArray(new Categorie[vectorObject.size()]);
		return objectFinale;
	}
	public Categorie[] getCategorieWithConnexion(String requette) throws Exception {
		Connexion connexion=new Connexion();
		try {
			return getCategorie(connexion,requette);
		} catch (Exception e) {
			throw new Exception("Erreur :"+e.getMessage());
		}finally {
			if(connexion!=null) {
				connexion.closeConnexion();
			}
		}
	}
	public Categorie[] getAllCategorie() throws Exception {
		return getCategorieWithConnexion("select * from categorie");
	}
	public boolean verificationDedoublement(Categorie categorie) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		Categorie []categories=getAllCategorie();int size=categories.length;
		String []fieldNoninclus= {"idCategorie"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(categorie, categories[i], fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public String insertionCategorie(Categorie categorie) throws Exception {
		if(categorie!=null && !verificationDedoublement(categorie)) {
			Connexion connexion=new Connexion();
			FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
			try {
				fonctionGeneraliser.insertAll(connexion, categorie,"Categorie", "nextval('categorieSequence')");
				return "insertion avec succé";
			} catch (Exception e) {
				throw new Exception("Erreur :"+e.getMessage());
			}finally {
				if(connexion!=null) {
					connexion.closeConnexion();
				}
			}
		}
		return "Il y a une champs vide ou cet information existe déjà";
	}
}

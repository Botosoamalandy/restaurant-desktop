package com.restaurent.dao;

import java.util.Vector;

import com.restaurent.entite.PlatsDuJour;


public class PlatsDuJourDAO {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public PlatsDuJour setTableauObjectToObject(PlatsDuJour []elements) {
		PlatsDuJour element=null;
		for(PlatsDuJour elementTmp :elements) {
			element=elementTmp;
		}
		return element;
	}
	public PlatsDuJour[] getPlatsDuJourWithConnexion(String requette) throws Exception {
		PlatsDuJour objectTmp=new PlatsDuJour();
		Vector<Object> vectorObject=fonctionGeneraliser.findWithConnexion(requette, objectTmp);
		return vectorObject.toArray(new PlatsDuJour[vectorObject.size()]);
	}
	public PlatsDuJour[] getAllPlatsDuJours() throws Exception {
		return getPlatsDuJourWithConnexion("select * from PlatsDuJour");
	}
	public PlatsDuJour[] getPlatsDuJoursRequetteSpeciale() throws Exception {
		return getPlatsDuJourWithConnexion("select PlatsDuJour.idPlatsDuJour,produitCategorie.nomProduit as idProduit,utilisateur.nomUtilisateur as idUtilisateur,produitCategorie.categorie,produitCategorie.prixUnitaire,produitCategorie.nomImage as descriptions,PlatsDuJour.datePlatsDuJour from produitCategorie join PlatsDuJour on produitCategorie.idProduit=PlatsDuJour.idProduit join utilisateur on PlatsDuJour.idUtilisateur=utilisateur.idUtilisateur");
	}
	public String insertionPlatsDuJours(PlatsDuJour platsDuJour) {
		if(platsDuJour!=null) {
			try {
				fonctionGeneraliser.insertionWithConnexion(platsDuJour,"PlatsDuJour","nextval('platsDuJourSequence')");
				return "Insertion reussi";
			} catch (Exception e) {
				return e.getMessage();
			}
		}
		return "Il y a une fausse information";
	}
	public String suppressionPlatsDuJour(String idPlatsDuJour) {
		try {
			fonctionGeneraliser.updateOrDeleteWithConnexion("DELETE FROM platsdujour WHERE idPlatsDuJour='"+idPlatsDuJour+"'","");
			return "Suppression reussi";
		} catch (Exception e) {
			return e.getMessage();
		}
	}
}

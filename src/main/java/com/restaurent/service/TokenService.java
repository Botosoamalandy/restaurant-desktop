package com.restaurent.service;

import com.restaurent.dao.TokenDAO;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Token;

public class TokenService extends TokenDAO{
	public boolean verificationSiTokenExiste(String tokens) throws Exception {
		Token token=getTokenAPartNumeroTokens(tokens);Dates dates=new Dates();dates.getNowDate();
		if(token!=null) {
			if(!dates.getDates().equals(token.getFinDate().getDates())) {
				return true;
			}
		}
		return false;
	}
	public String getNewToken() {
		String alphabe="efijklmnopqrstuvwxyz0123456789";
		char []tableauChar=alphabe.toCharArray();
		String resutlat="";
		while(resutlat.length()<20) {
			int hasards=(int)(Math.random()*30);
			resutlat=resutlat+tableauChar[hasards];
		}
		return resutlat;
	}
	@SuppressWarnings("deprecation")
	public Token insertionToken(String idUtilisateurs,String passwords) throws Exception {
		try {
			Dates dates=new Dates();dates.getNowDate();dates.setDate(dates.getDate()+1);
			String newToken=passwords+getNewToken();
			Token token=new Token("nextval('tokenSequence')", idUtilisateurs, newToken,dates);
			insertionTokens(token);
			return token;
		} catch (Exception e) {
			throw new Exception(e.getMessage());
		}
	}
	public boolean deconnection(String numeroTokens) throws Exception {
		Token token=getTokenAPartNumeroTokens(numeroTokens);
		try {
			deleteToken(token);
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		}
	}
	public boolean verificationTokenSiValable(String numeroTokens) throws Exception {
		Token token=getTokenAPartNumeroTokens(numeroTokens);
		if(token!=null) {
			Dates dates=new Dates();dates.getNowDate();;
			return token.getFinDate().after(dates);
		}
		return false;
	}
}

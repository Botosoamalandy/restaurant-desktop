package com.restaurent.service;

import java.util.Vector;

import com.restaurent.dao.TableauStatistiqueDao;
import com.restaurent.entite.Json;
import com.restaurent.entite.TableauStatistiqueCommande;

public class TableauStatistiqueService extends TableauStatistiqueDao{
	public Vector<TableauStatistiqueCommande> getFiltreParClassement(TableauStatistiqueCommande []statistiqueCommandes,int anneeOuMoisOuJour,boolean platsOuNon){
		int size=statistiqueCommandes.length;
		Vector<TableauStatistiqueCommande> vector=new Vector<TableauStatistiqueCommande>();
		if(size>0 && !platsOuNon) {//filtre le commande/payement par date
			vector.add(statistiqueCommandes[0]);
			for (int i = 1; i < size; i++) {
				int size2=vector.size();boolean test=false;
				for (int j = 0; j < size2; j++) {
					if(anneeOuMoisOuJour==1) {//Jours
						if(vector.get(j).getJours()==statistiqueCommandes[i].getJours()) {
							vector.get(j).setTotal(vector.get(j).getTotal()+statistiqueCommandes[i].getTotal());test=true;
						}
					}else if(anneeOuMoisOuJour==2) {//mois
						if(vector.get(j).getMois()==statistiqueCommandes[i].getMois()) {
							vector.get(j).setTotal(vector.get(j).getTotal()+statistiqueCommandes[i].getTotal());test=true;
						}
					}else {//annee
						if(vector.get(j).getAnnee()==statistiqueCommandes[i].getAnnee()) {
							vector.get(j).setTotal(vector.get(j).getTotal()+statistiqueCommandes[i].getTotal());test=true;
						}
					}
				}
				if(test==false) {
					vector.add(statistiqueCommandes[i]);
				}
			}
		}else if(size>0 && platsOuNon) {//filtre le plats par nom mais pas par date
			vector.add(statistiqueCommandes[0]);
			for (int i = 1; i < size; i++) {
				int size2=vector.size();boolean test=false;
				for (int j = 0; j < size2; j++) {
					if(vector.get(j).getNumeroCommande().equals(statistiqueCommandes[i].getNumeroCommande())) {
						vector.get(j).setTotal(vector.get(j).getTotal()+statistiqueCommandes[i].getTotal());test=true;
					}
				}
				if(test==false) {
					vector.add(statistiqueCommandes[i]);
				}
			}
		}
		return vector;
	}
	public double getTotalValeur(Vector<TableauStatistiqueCommande> vector) {
		int size=vector.size();double total=0;
		for (int i = 0; i < size; i++) {
			total=total+vector.get(i).getTotal();
		}
		return total;
	}
	public String castIntValeur(double valeur) {
		if(valeur<=0) {
			return "01";
		}else {
			return ""+(int)valeur;
		}
	}
	public Json setTableauStatistiqueCommandeToJson(TableauStatistiqueCommande statistiqueCommande,double total,boolean platsOuNon) {
		Json json=new Json();
		double resultat=(statistiqueCommande.getTotal()*100)/total;
		json.put("valeur",resultat);
		if(platsOuNon) {
			json.put("libelle",statistiqueCommande.getNumeroCommande());
		}else {
			json.put("libelle",""+statistiqueCommande.getLabel()+" : "+castIntValeur(statistiqueCommande.getAnnee())+"/"+castIntValeur(statistiqueCommande.getMois())+"/"+castIntValeur(statistiqueCommande.getJours()));
		}
		json.put("dates",castIntValeur(statistiqueCommande.getAnnee())+"/"+castIntValeur(statistiqueCommande.getMois())+"/"+castIntValeur(statistiqueCommande.getJours()));
		return json;
	}
	public String getFiltre(TableauStatistiqueCommande []statistiqueCommandes,int anneeOuMoisOuJour,boolean platsOuNon) throws Exception {//true annee, false mois
		Json json=new Json();
		Vector<TableauStatistiqueCommande> vector=getFiltreParClassement(statistiqueCommandes, anneeOuMoisOuJour,platsOuNon);
		double valeurTotal=getTotalValeur(vector);Vector<Json> jsons=new Vector<Json>();int size=vector.size();
		for (int i = 0; i < size; i++) {
			jsons.add(setTableauStatistiqueCommandeToJson(vector.get(i),valeurTotal,platsOuNon));
		}
		return json.setJsonToStringJson(jsons);
	}
	public int getAnneeOuMoisOuJour(String classement) {
		if(classement.equals("jours")) {
			return 1;
		}else if(classement.equals("mois")) {
			return 2;
		}else {
			return 3;
		}
	}
	public String getTableauStatistique(String typeDeClassement,String classement,String annee,String mois) {
		if(!typeDeClassement.equals("") && !classement.equals("")) {
			int anneeOuMoisOuJour=getAnneeOuMoisOuJour(classement);
			try {
				System.out.println("typeDeClassement :"+typeDeClassement);
				double annees=Double.parseDouble(annee),moisTemporaire=Double.parseDouble(mois);
				if(typeDeClassement.equals("Plats")) {
					return getFiltre(getTableauStatistiquePlats(annees, moisTemporaire, anneeOuMoisOuJour), anneeOuMoisOuJour,true);
				}else if(typeDeClassement.equals("Payement")) {
					return getFiltre(getTableauStatistiquePayement(annees, moisTemporaire, anneeOuMoisOuJour), anneeOuMoisOuJour,false);
				}else {
					return getFiltre(getTableauStatistiqueCommande(annees, moisTemporaire, anneeOuMoisOuJour), anneeOuMoisOuJour,false);
				}
			} catch (Exception e) {
				return ""+e.getMessage();
			}
			
		}
		return "";
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

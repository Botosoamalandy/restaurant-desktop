package com.restaurent.service;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.dao.PlatsDuJourDAO;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Json;
import com.restaurent.entite.PlatsDuJour;
import com.restaurent.entite.Token;

public class PlatsDuJourService extends PlatsDuJourDAO{
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	
	public boolean verificationDeDoublement(PlatsDuJour platsDuJour) throws Exception {
		PlatsDuJour []platsDuJours=getAllPlatsDuJours();int size=platsDuJours.length;
		String []fieldNoninclus= {"idPlatsDuJour"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(platsDuJour, platsDuJours[i], fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public String insertionPlatsDuJours(String idProduit,String tokenUtilisateur,String categorie,String prixUnitaire,String descriptions) throws Exception {
		String message="Il y a une erreur d'information";
		Json json=new Json();
		if(!idProduit.equals("") && !tokenUtilisateur.equals("") && !categorie.equals("") && !prixUnitaire.equals("") && !descriptions.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(tokenUtilisateur);
			if(token!=null) {
				Dates datePlatsDuJour=new Dates();datePlatsDuJour.getNowDate();
				PlatsDuJour platsDuJour=new PlatsDuJour("nextval('platsDuJourSequence')", idProduit, token.getIdUtilisateur(),categorie,fonctionGeneraliser.changeStringToDouble(prixUnitaire),descriptions, datePlatsDuJour);
				if(!verificationDeDoublement(platsDuJour)) {
					message=insertionPlatsDuJours(platsDuJour);
				}
			}
		}
		json.put("message",message);
		return json.toString();
	}
	public String supprimerPlatsDuJour(String idPlatsDuJour) {
		Json json=new Json();
		json.put("message",suppressionPlatsDuJour(idPlatsDuJour));
		return json.toString();
	}
	public String getPlatsDuJours() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getPlatsDuJoursRequetteSpeciale());
	}
}

package com.restaurent.service;

import com.restaurent.dao.AnnulationCommandeDAO;
import com.restaurent.entite.AnnulationCommande;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Json;

public class AnnulationCommandeService extends AnnulationCommandeDAO{
	public String insertionAnnulationCommandes(String idCommande,int etat) {
		String message="";
		if(!idCommande.equals("") && !idCommande.equals("null")) {
			Dates dateCommande=new Dates();dateCommande.getNowDate();
			AnnulationCommande annulationCommande=new AnnulationCommande("nextval('annulationCommandeSequence')", idCommande,etat, dateCommande);
			message=insertionAnnulationCommande(annulationCommande);
		}else {
			message="Il y a une erreur lors de la suppression du commande";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	public boolean verificationSiIlExiste(String idCommande) throws Exception {
		AnnulationCommande []annulationCommandes=getAllAnnulationCommande();int size=annulationCommandes.length;
		for (int i = 0; i < size; i++) {
			if(annulationCommandes[i].getIdCommande().equals(idCommande)) {
				return true;
			}
		}
		return false;
	}
	public String insertionAnnulationCommandesAdministrateur(String idCommande) throws Exception {
		String message="";
		if(!idCommande.equals("") && !idCommande.equals("null")) {
			if(verificationSiIlExiste(idCommande)) {
				updateAnnulationCommande(idCommande);
			}else {
				insertionAnnulationCommandes(idCommande, 11);
			}	
			message="Suppression reussi";
		}else {
			message="Il y a une erreur lors de la suppression du commande";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
}

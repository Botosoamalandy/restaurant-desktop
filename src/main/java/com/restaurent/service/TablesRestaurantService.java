package com.restaurent.service;

import com.restaurent.dao.TablesRestaurantDAO;
import com.restaurent.entite.Json;

public class TablesRestaurantService extends TablesRestaurantDAO{
	public String getTablesRestaurant() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllTablesRestaurant());
	}
}

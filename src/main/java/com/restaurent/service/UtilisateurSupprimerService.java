package com.restaurent.service;

import com.restaurent.dao.UtilisateurSupprimerDAO;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Json;
import com.restaurent.entite.Token;
import com.restaurent.entite.Utilisateur;
import com.restaurent.entite.UtilisateurSupprimer;

public class UtilisateurSupprimerService extends UtilisateurSupprimerDAO{
	TokenService tokenService=new TokenService();
	UtilisateurService utilisateurService=new UtilisateurService();
	
	public boolean verificationSiCestLuiMeme(String idUtilisateur,String numeroToken) throws Exception {
		Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
		Utilisateur utilisateur=utilisateurService.getUtilisateurAPartIdUtilisateur(idUtilisateur);
		if(token!=null && utilisateur!=null) {
			if(!token.getIdUtilisateur().equals(idUtilisateur) && utilisateur.getEtatUtilisateur()<3) {
				return true;
			}
		}
		return false;
	}
	public String supprimerUtilisateur(String idUtilisateur,String numeroToken) throws Exception {
		String message="Information invalide";
		if(!idUtilisateur.equals("") && !numeroToken.equals("")) {
			if(verificationSiCestLuiMeme(idUtilisateur, numeroToken)) {
				Dates dateDeSuppression=new Dates();dateDeSuppression.getNowDate();
				UtilisateurSupprimer utilisateurSupprimer=new UtilisateurSupprimer("nextval('utilisateurSupprimerSequence')", idUtilisateur, dateDeSuppression);
				message=insertionUtilisateurSupprimer(utilisateurSupprimer);
			}else {
				message="Vous ne pouvez pas supprimer votre propre compte ou il y a une information invalide";
			}
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
}

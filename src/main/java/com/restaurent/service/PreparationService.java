package com.restaurent.service;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.dao.PreparationDao;
import com.restaurent.entite.Dates;
import com.restaurent.entite.FactureCommande;
import com.restaurent.entite.Insuffisant;
import com.restaurent.entite.Json;
import com.restaurent.entite.Preparation;
import com.restaurent.entite.Token;

public class PreparationService extends PreparationDao {
	public boolean verificationDedoublement(Preparation preparation) throws Exception {
		Preparation []preparations=getAllPreparation();int size=preparations.length;
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		String []fieldNoninclus= {"idPreparation"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(preparation, preparations[i], fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public int getNumeroPreparation() throws Exception {
		Preparation []preparations=getAllPreparation();
		return preparations.length+1;
	}
	public String insertePreparation(String idCommande,String numeroCommande,String nombreDePlats,String numeroTable) {
		String message="Il y a une information qui manque";
		if(!idCommande.equals("") && !numeroCommande.equals("") && !nombreDePlats.equals("") && !numeroTable.equals("")) {
			try {
				int nombrePlats=Integer.parseInt(nombreDePlats);
				Dates datePreparation=new Dates();datePreparation.getNowDate();
				Preparation preparation=new Preparation("nextval('preparationSequence')", idCommande, numeroCommande,numeroTable,getNumeroPreparation(), nombrePlats,1, datePreparation);
				if(!verificationDedoublement(preparation)) {
					Dates dateFacture=new Dates();dateFacture.getNowDate();
					FactureCommande factureCommande=new FactureCommande("nextval('factureCommandeSequence')", idCommande, 1, dateFacture);
					message=insertionPreparation(preparation,factureCommande);
				}else {
					message="Cet(te) information existe déjà";
				}
			} catch (Exception e) {
				message=e.getMessage();
			}
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
	public int getEtatPreparationCommandeByIdCommande(String idCommande) throws Exception {
		InsuffisantService insuffisantService=new InsuffisantService();
		Preparation preparation=getPreparationAPartIdCommande(idCommande);
		Insuffisant insuffisant=insuffisantService.getInsuffisantAPartIdCommande(idCommande);
		if(preparation!=null) {
			return preparation.getEtatPreparation();
		}
		if(insuffisant!=null && insuffisant.getIdCommande().equals(idCommande)) {
			return 1;
		}
		return 0;
	}
	public String getAutorisationAnnulation(String idCommande) throws Exception {
		int etat=0;
		if(!idCommande.equals("")) {
			etat=getEtatPreparationCommandeByIdCommande(idCommande);
		}
		Json json=new Json();
		json.put("etat", etat);
		return json.toString();	
	}
	public String getListePreparation() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getPreparationEnCours());	
	}
	public String getListePreparationByIdUtilisateur(String numeroToken) throws Exception {
		Json json=new Json();
		if(!numeroToken.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
			if(token!=null) {
				return json.getJsonToStringGeneraliser(getPreparationPretAPartIdUtilisateur(token.getIdUtilisateur()));
			}
		}
		return json.getJsonToStringGeneraliser(getPreparationEnCours());	
	}
	public String getListePreparationNonSupprimer() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllPreparationNonSupprimer());	
	}
	public String getListePreparationPret() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getPreparationPret());	
	}
	public String commandePret(String idPreparation) throws Exception {
		String message="";
		Json json=new Json();
		try {
			message=updatePreparation(idPreparation,"11","Commande prêt");
		} catch (Exception e) {
			message=e.getMessage();
		}
		json.put("message",message);
		return json.toString();
	}
	public String supprimerPreparation(String idPreparation) throws Exception {
		String message="";
		Json json=new Json();
		try {
			message=updatePreparation(idPreparation,"22","Commande supprimé");
		} catch (Exception e) {
			message=e.getMessage();
		}
		json.put("message",message);
		return json.toString();
	}
}

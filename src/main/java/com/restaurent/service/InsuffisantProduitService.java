package com.restaurent.service;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.dao.InsuffisantProduitDao;
import com.restaurent.entite.Dates;
import com.restaurent.entite.InsuffisantProduit;

public class InsuffisantProduitService extends InsuffisantProduitDao {
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	public InsuffisantProduit[] getInsuffisantProduitServiceByIdProduit(String idProduit,String idCommande){
		String []produit=idProduit.split("#");int size=produit.length;
		if(size>0) {
			InsuffisantProduit []insuffisantProduitsTemporaire=new InsuffisantProduit[size];
			Dates dateInsuffisant=new Dates();dateInsuffisant.getNowDate();
			for (int i = 0; i < size; i++) {
				String []idEtQauntite=produit[i].split("£");int size2=idEtQauntite.length;
				if(size2>1) {
					try {
						int quanite=Integer.parseInt(idEtQauntite[0]);
						insuffisantProduitsTemporaire[i]=new InsuffisantProduit("nextval('insuffisantProduitSequence')", idCommande,idEtQauntite[1], quanite,1, dateInsuffisant);
					} catch (Exception e) {
						return null;
					}
				}else {
					return null;
				}
			}
			return insuffisantProduitsTemporaire;
		}
		return null;
	}
	
}

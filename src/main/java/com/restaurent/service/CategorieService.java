package com.restaurent.service;

import com.restaurent.dao.CategorieDAO;
import com.restaurent.entite.Categorie;
import com.restaurent.entite.Json;

public class CategorieService extends CategorieDAO{
	public String getCategorieToFormatJson() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllCategorie());
	}
	public String insertionCategories(String categorie) throws Exception {
		String message="";
		if(!categorie.equals("")) {
			Categorie categorie2=new Categorie("nextval('categorieSequence')", categorie);
			message=insertionCategorie(categorie2);
		}else {
			message="Il y a une champs vide";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
}

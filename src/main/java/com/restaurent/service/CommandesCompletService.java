package com.restaurent.service;

import com.restaurent.dao.CommandesCompletDao;
import com.restaurent.entite.AnnulationCommande;
import com.restaurent.entite.CommandeTemporaire;
import com.restaurent.entite.CommandesComplet;
import com.restaurent.entite.Insuffisant;
import com.restaurent.entite.Json;
import com.restaurent.entite.Token;

public class CommandesCompletService extends CommandesCompletDao{
	Json json=new Json();
	public String getCommandesCompletByIdCommande(String idCommande) throws Exception {
		return json.putObjectInJson(getAllCommandesCompletAPArtIdCommande(idCommande)).toString();
	}
	public String getCommandesCompletToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getAllCommandesComplet());
	}
	public String getCommandesCompletMesCommandesToFormatJson(String numeroToken) throws Exception {
		TokenService tokenService=new TokenService();
		Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
		if(token!=null) {
			return json.getJsonToStringGeneraliser(getCommandesCompletAPartIdUtilisateur(token.getIdUtilisateur()));
		}
		CommandesComplet []commandesComplets=new CommandesComplet[1];
		commandesComplets[0]=new CommandesComplet();
		return json.getJsonToStringGeneraliser(commandesComplets);
	}
	public String getCommandesCompletNonValiderToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getCommandesCompletAPartFermetureCommandeAndEtatCommande());
	}
	public int getEtatByidCommandeInInsuffisant(CommandesComplet commandesComplet,Insuffisant []insuffisants,int etat) {
		int size=insuffisants.length;
		for (int i = 0; i < size; i++) {
			if(commandesComplet.getIdCommande().equals(insuffisants[i].getIdCommande())) {
				return 1;
			}
		}
		return etat;
	}
	public String getCommandesCompletNonValide() throws Exception {
		CommandesComplet []commandesComplets=getCommandesCompletAPartFermetureCommandeAndEtatCommande();int size=commandesComplets.length;
		CommandeTemporaire []commandeTemporaires=new CommandeTemporaire[size];
		PreparationService preparationService=new PreparationService();
		CommandeParTableService commandeParTableService=new CommandeParTableService();
		InsuffisantService insuffisantService=new InsuffisantService();
		Insuffisant []insuffisants=insuffisantService.getInsuffisant();
		for (int i = 0; i < size; i++) {
			int etat=getEtatByidCommandeInInsuffisant(commandesComplets[i], insuffisants,preparationService.getEtatPreparationCommandeByIdCommande(commandesComplets[i].getIdCommande()));
			commandeTemporaires[i]=new CommandeTemporaire(commandesComplets[i].getIdCommande(),commandesComplets[i].getIdUtilisateur(),commandesComplets[i].getNomUtilisateur(),commandesComplets[i].getMotDePasse(),commandesComplets[i].getEtatUtilisateur(),commandesComplets[i].getDateAjout(),commandesComplets[i].getIdTablesRestaurant(),commandesComplets[i].getNumeroTables(),commandesComplets[i].getTypeDeTables(),commandesComplets[i].getNumeroCommande(),commandeParTableService.getNombrePlatsCommande(commandesComplets[i].getIdCommande()),commandesComplets[i].getFermetureCommande(),commandesComplets[i].getEtatCommande(),etat,commandesComplets[i].getDateCommande());
		}
		return json.getJsonToStringGeneraliser(commandeTemporaires);
	}
	public String getCommandesCompletValiderToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getCommandesCompletAPartFermetureCommandeAndEtatCommandeAndValidation());
	}
	public String getCommandesCompletEnCoursToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getCommandesCompletAPartFermetureCommandeAndEtatCommandeAndEnCours());
	}
	public String getCommandesCompletAdminstrateurToFormatJson() throws Exception {
		CommandesComplet []commandesComplets=getCommandesCommpletSpecialeAdministrateur();
		AnnulationCommandeService annulationCommandeService=new AnnulationCommandeService();
		AnnulationCommande []annulationCommandes=annulationCommandeService.getAllAnnulationCommande();
		int size1=commandesComplets.length,size2=annulationCommandes.length;
		for (int i = 0; i < size1; i++) {
			for (int j = 0; j < size2; j++) {
				if(commandesComplets[i].getIdCommande().equals(annulationCommandes[j].getIdCommande())) {
					if(commandesComplets[i].getEtatCommande()==22) {
						commandesComplets[i].setEtatCommande(44);
					}else {
						commandesComplets[i].setEtatCommande(33);
					}
					if((i+1)<size1) {
						i++;
					}
				}
			}
		}
		return json.getJsonToStringGeneraliser(commandesComplets);
	}
}

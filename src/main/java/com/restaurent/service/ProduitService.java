package com.restaurent.service;

import com.restaurent.dao.ProduitDAO;
import com.restaurent.entite.Json;
import com.restaurent.entite.Produit;

public class ProduitService extends ProduitDAO {
	public String insertionProduits(String idCategorie, String nomProduit, String prixUnitaire, String descriptions, String nomImage, String tva, String remise) {
		String message="Verifiez votre champs car il y a une erreur";
		if(!idCategorie.equals("") && !nomProduit.equals("") && !prixUnitaire.equals("") && !nomImage.equals("") && !tva.equals("") && !remise.equals("")) {
			try {
				double pu=Double.parseDouble(prixUnitaire),tvaTemporaire=Double.parseDouble(tva),remiseTemporaire=Double.parseDouble(remise);
				if(descriptions.equals("")) {
					descriptions="Description vide";
				}
				Produit produit=new Produit("nextval('produitSequence')", idCategorie, nomProduit, pu, descriptions, nomImage, tvaTemporaire, remiseTemporaire,20);
				message=insertionProduit(produit);
				
			} catch (Exception e) {
				message=e.getMessage();
				e.printStackTrace();
			}
		}else {
			message="Il y a un champs vide";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	public String creationNomPhoto(String stringOriginales) {
		TokenService token=new TokenService();
		String stringOriginale=token.getNewToken()+stringOriginales;
		String []strings=stringOriginale.split("_");String finale="";int size=strings.length;
		for (int i = 0; i <size; i++) {
			finale=finale+strings[i];
		}
		return finale;
	}
	public String updateQuantiteEtDescriptionProduitService(String idProduit,String quantite,String description) {
		String message="Il y a une erreur d'information";
		if(!idProduit.equals("") && !quantite.equals("") && !description.equals("")) {
			try {
				int quantites=Integer.parseInt(quantite);
				message=updateQuantiteEtDescriptionProduit(idProduit,""+quantites, description);
			} catch (Exception e) {
				message=e.getMessage();
			}
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
}

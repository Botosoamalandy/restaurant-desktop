package com.restaurent.service;

import com.restaurent.dao.FactureDAO;
import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.entite.Commande;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Facture;
import com.restaurent.entite.Json;
import com.restaurent.entite.Payement;

public class FactureService extends FactureDAO{
	public boolean getVerificationDedoublement(Facture facture) throws Exception {
		FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
		Facture []factures=getAllFacture();int size=factures.length;
		String []fieldNoninclus= {"idFacture","dateFacturePayement"};
		for (int i = 0; i < size; i++) {
			if(fonctionGeneraliser.getVerificationIfObjectEqualsOrNot(facture, factures[i],fieldNoninclus)) {
				return true;
			}
		}
		return false;
	}
	public String insertionFactures(String idCommande) throws Exception {
		String message="Il y a une champs vide";
		if(!idCommande.equals("")) {
			PayementService payementService=new PayementService();
			CommandeService commandeService=new CommandeService();
			Payement []payements=payementService.getPayementAPartIdCommande(idCommande);
			Commande commande=commandeService.getCommandeAPartIdCommande(idCommande);
			if(commande!=null) {
				Dates dateFacturePayement=new Dates();dateFacturePayement.getNowDate();
				Payement payement=payementService.getPayementEtatEleve(payements, commande);
				Facture facture=new Facture("nextval('factureSequence')", commande.getIdCommande(),payement.getMontantPayer(),payement.getMontantTotal(),payement.getMontantResteClients(),payement.getEtatPayement(),payement.getDatePayement(), 1, dateFacturePayement);
				if(!getVerificationDedoublement(facture)) {
					message=insertionFacture(facture);
					System.out.println(message);
				}else {
					message="Cet facture existe déjà";
				}
			}else {
				message="Il y a une erreur d'information";
			}
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	public String supprimerFactureAdministrateur(String idFacture) {
		Json json=new Json();
		json.put("message",suppressionFacture(idFacture,false));
		return json.toString();
	}
	public String supprimerCaissierFacture(String idFacture) {
		Json json=new Json();
		json.put("message",suppressionFacture(idFacture,true));
		return json.toString();
	}
	public String tousLesFactureNonSupprimer() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getFactureNonSupprimer());
	}
	public String getFactureAdministrateurs() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getFactureAdministrateur());
	}
}

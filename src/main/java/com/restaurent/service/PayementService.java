package com.restaurent.service;

import java.util.Vector;

import com.restaurent.dao.PayementDAO;
import com.restaurent.entite.Commande;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Json;
import com.restaurent.entite.Payement;

public class PayementService extends PayementDAO{
	CommandeParTableService commandeParTableService=new CommandeParTableService();
	public Payement[] getInformationPayementIfNullOrNot(String idCommande) throws Exception {
		Payement []payements=getPayementAPartIdCommande(idCommande);int size=payements.length;
		if(size==0) {
			Payement []payement=new Payement[1];
			payement[0]=new Payement("0", idCommande,0,0,0,1,new Dates());
			return payement;
		}
		return payements;
	}
	public double getMontantPayer(Payement []payements) {
		int size=payements.length;double total=0;
		for (int i = 0; i < size; i++) {
			total=total+payements[i].getMontantPayer();
			if(payements[i].getEtatPayement()==11) {
				return payements[i].getMontantPayer();
			}
		}
		return total;
	}
	public double getMontantResteClient(Payement []payements) {
		int size=payements.length;double total=0;
		for (int i = 0; i < size; i++) {
			total=total+payements[i].getMontantResteClients();
		}
		return total;
	}
	public int getEtat(Payement []payements) {
		int size=payements.length;
		for (int i = 0; i < size; i++) {
			if(payements[i].getEtatPayement()==11) {
				return 11; 
			}
		}
		return 1;
	}
	public String getEtatPayementGlobale(Payement []payements) {
		int size=payements.length;
		for (int i = 0; i < size; i++) {
			if(payements[i].getEtatPayement()==11) {
				return "commande payé"; 
			}
		}
		return "commande non payé";
	}
	public Dates getDatesPayement(Payement []payements) {
		int size=payements.length;
		for (int i = 0; i < size; i++) {
			if(payements[i].getEtatPayement()==11) {
				return payements[i].getDatePayement(); 
			}
		}
		Dates dates=new Dates();dates.getNowDate();
		return dates;
	}
	public Object[] calculeReste(double total,double prixTotal,double montantPayer) {
		Object []objects=new Object[2];
		if(total<=0) {
			objects[0]=true;
			objects[1]=montantPayer-prixTotal;
		}else {
			objects[0]=false;
			objects[1]=total;
		}
		return objects;
	}
	public String getInformationPayementByIdCommande(String idCommande) throws Exception {
		Payement []payements=getInformationPayementIfNullOrNot(idCommande);
		double prixTotal=commandeParTableService.getMontantTotal(idCommande),
			   argentResteClient=getMontantResteClient(payements),
			   montantPayer=getMontantPayer(payements),argentRestant=(prixTotal-montantPayer);
		if(argentRestant<=0) {
			argentRestant=0;
		}
		Json json=new Json();
		json.put("commande",((new CommandeService()).getCommandeAPartIdCommande(idCommande)).getNumeroCommande());
		json.put("etat",getEtatPayementGlobale(payements));
		json.put("prixTotal",prixTotal);
		json.put("montantPayer",montantPayer);
		json.put("argentRestant",argentRestant);
		json.put("argentResteClient",argentResteClient);
		json.put("date",getDatesPayement(payements));
		json.put("etatCommandes",getEtat(payements));
		return json.toString();
	}
	public String insertionPayements(String idCommande,String montant) {
		Json json=new Json();
		String message="";
		if(!idCommande.equals("") && !montant.equals("")) {
			try {
				Payement []payements=getInformationPayementIfNullOrNot(idCommande);
				double montantTemporaire=Double.parseDouble(montant),
				       montantPayer=getMontantPayer(payements)+montantTemporaire,
				       prixTotal=commandeParTableService.getMontantTotal(idCommande),
				       total=prixTotal-montantPayer;
				Object []reste=calculeReste(total, prixTotal, montantPayer);
				boolean payerOuPas=(boolean)reste[0];double resteTotal=(double)reste[1];
				Dates datePayement=new Dates();datePayement.getNowDate();
				Payement payement=null;boolean confirmation=false;
				if(payerOuPas) {
					payement=new Payement("nextval('payementSequence')", idCommande,prixTotal,prixTotal,resteTotal,11, datePayement);
					confirmation=true;
				}else {
					payement=new Payement("nextval('payementSequence')", idCommande,montantTemporaire,prixTotal,0,1, datePayement);
					confirmation=false;
				}
				message=insertionPayement(payement,confirmation);
			} catch (Exception e) {
				System.out.println(e.getMessage());
				message=e.getMessage();
			}
		}else {
			message="Il y a une champs vide";
		}
		json.put("message",message);
		return json.toString();
	}
	public double convertToDouble(String montant) {
		try {
			return Double.parseDouble(montant);
		} catch (Exception e) {
			return 0;
		}
	}
	public String dedoublementPayement(String idCommande,String montant) throws Exception {
		Json json=new Json();
		String message="false";
		if(!idCommande.equals("") && !montant.equals("")) {
			Payement []payements=getPayementAPartIdCommande(idCommande);int size=payements.length;
			for (int i = 0; i < size; i++) {
				if(payements[i].getMontantPayer()==convertToDouble(montant)) {
					message="true";break;
				}
			}
		}
		json.put("message",message);
		return json.toString();
	}
	//-----------------------------
	public boolean verificationDedoublementDansVector(Payement payement,Vector<Payement> vector) {
		int size2=vector.size();
		for (int j = 0; j < size2; j++) {
			if(payement.getIdCommande().equals(vector.get(j).getIdCommande())) {
				return true;
			}
		}
		return false;
	}
	public Payement getUtilisateurEnEtat(Payement payement,Payement []payements) {
		int size=payements.length;
		for (int j = 0; j < size; j++) {
			if(payements[j].getIdCommande().equals(payement.getIdCommande()) && payements[j].getEtatPayement()==11) {
				return payements[j];
			}
		}
		return payement;
	}
	public Payement[] changeVectorToObject(Vector<Payement> vector) {
		int size=vector.size();
		Payement []payements=new Payement[size];
		for (int i = 0; i < size; i++) {
			payements[i]=vector.get(i);
		}
		return payements;
	}
	public String getPayementsByidCommande() throws Exception {
		Payement []payements=getAllPayementWithNumeroCommande();int size=payements.length;
		Vector<Payement> vector=new Vector<Payement>();
		for (int i = 0; i < size; i++) {
			if(!verificationDedoublementDansVector(payements[i], vector)) {
				vector.add(getUtilisateurEnEtat(payements[i],payements));
			}
		}
		Json json=new Json();
		return json.getJsonToStringGeneraliser(changeVectorToObject(vector));
	}
	public String getPayementsByidCommandeAdministrateur() throws Exception {
		Payement []payements=getAllPayementWithNumeroCommandeAdministrateur();int size=payements.length;
		Vector<Payement> vector=new Vector<Payement>();
		for (int i = 0; i < size; i++) {
			if(!verificationDedoublementDansVector(payements[i], vector)) {
				vector.add(getUtilisateurEnEtat(payements[i],payements));
			}
		}
		Json json=new Json();
		return json.getJsonToStringGeneraliser(changeVectorToObject(vector));
	}
	public String supprimerPayementAdministrateur(String idPayement) throws Exception {
		System.out.println("idPayement :"+idPayement);
		Json json=new Json();
		String message="Information invalide";
		if(!idPayement.equals("")) {
			message=deletePayement(idPayement);
		}
		json.put("message", message);
		return json.toString();
	}
	//-----------------------------
	public String caissierSupprimerPayement(String idPayement) throws Exception {
		Json json=new Json();
		String message="";
		Payement payement=getPayementAPartIdPayement(idPayement);
		if(payement!=null) {
			message=updatePayementToEtatSuppression(payement.getIdCommande());
		}else {
			message="Il y a une erreur d'information";
		}
		json.put("message",message);
		return json.toString();
	}
	public String annulationPayementParCommande(String idPayement,boolean typeDAnnulation) throws Exception {
		Json json=new Json();
		String message="";
		Payement payement=getPayementAPartIdPayement(idPayement);
		if(payement!=null) {
			message=updatePayementToEtatAnnulation(payement.getIdCommande(),payement.getIdPayement(),typeDAnnulation);
		}else {
			message="Il y a une erreur d'information";
		}
		json.put("message",message);
		return json.toString();
	}
	public String getInformationPayementByIdPayement(String idPayement) throws Exception {
		Json json=new Json();
		Payement payement=getPayementAPartIdPayement(idPayement);
		json.put("message",payement.getIdCommande());
		return json.toString();
	}
	public int getEtat(int etatCommande) {
		if(etatCommande==22) {
			return 11;
		}
		return 1;
	}
	public double getMontantPayerWithCommande(int etatCommande,double montantTotal) {
		if(etatCommande==22) {
			return montantTotal;
		}
		return 0;
	}
	public String getInformationPayementCommandeByIdCommande(String idCommande) throws Exception {
		Payement []payements=getPayementSpecialeAPartIdCommande(idCommande);int size=payements.length;
		Json json=new Json();
		if(size>0) {
			for (int i = 0; i < size; i++) {
				if(payements[i].getEtatPayement()>=11) {
					return  json.putObjectInJson(payements[i]).toString();
				}
			}
			return json.putObjectInJson(payements[0]).toString();
		}else {
			CommandeService commandeService=new CommandeService();
			Commande commande=commandeService.getCommandeAPartIdCommandeSpeciale(idCommande);
			CommandeParTableService commandeParTableService=new CommandeParTableService();
			Payement payement=new Payement(commande.getIdUtilisateur(),commande.getNumeroCommande(), 0,commandeParTableService.getMontantTotal(idCommande),0,getEtat(commande.getEtatCommande()),commande.getDateCommande());
			return json.putObjectInJson(payement).toString();
		}
	}
	public Payement getPayementEtatEleve(Payement []payements,Commande commande) throws Exception {
		int size=payements.length;
		if(size>0) {
			for (int i = 0; i < size; i++) {
				if(payements[i].getEtatPayement()>=11) {
					return payements[i];
				}
			}
			return payements[size-1];
		}else {
			Dates datePayement=new Dates();datePayement.getNowDate();
			CommandeParTableService commandeParTableService=new CommandeParTableService();
			double montantTotal=commandeParTableService.getMontantTotal(commande.getIdCommande());
			return new Payement("0",commande.getIdCommande(),getMontantPayerWithCommande(commande.getEtatCommande(), montantTotal),montantTotal,0,getEtat(commande.getEtatCommande()), datePayement);
		}
	}
}

































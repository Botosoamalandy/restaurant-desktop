package com.restaurent.service;

import com.restaurent.dao.FonctionGeneraliser;
import com.restaurent.dao.ProduitDAO;
import com.restaurent.entite.Json;
import com.restaurent.entite.Produit;
import com.restaurent.entite.ProduitMobile;

public class ProduitMobileServie extends ProduitDAO{
	FonctionGeneraliser fonctionGeneraliser=new FonctionGeneraliser();
	public String getProduitMobile() throws Exception {
		Json json=new Json();
		Produit []produits=getAllProduitSepiciale();int size=produits.length;
		ProduitMobile []produitMobiles=new ProduitMobile[size];
		for (int i = 0; i < size; i++) {
			String imagePath=System.getProperty("user.dir")+"/src/main/resources/static/assets/images/img/"+produits[i].getNomImage();
			produitMobiles[i]=new ProduitMobile(produits[i].getIdProduit(),produits[i].getIdCategorie(),produits[i].getNomProduit(),produits[i].getPrixUnitaire(),produits[i].getDescriptions(),produits[i].getNomImage(),fonctionGeneraliser.setImageToBase64(imagePath,produits[i].getNomImage()),produits[i].getTva(),produits[i].getRemise(),produits[i].getQuantite());
		}
		return json.getJsonToStringGeneraliser(produitMobiles);
	}
}

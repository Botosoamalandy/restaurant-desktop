package com.restaurent.service;

import com.restaurent.dao.CommandeParTableDAO;
import com.restaurent.entite.CommandeParTable;
import com.restaurent.entite.Json;

public class CommandeParTableService extends CommandeParTableDAO{
	public String getCommandeParTableByNumeroCommande(String numeroCommande) throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getCommandeParTableAPartNumroCommande(numeroCommande));
	}
	public String getCommandeParTableByIdCommande(String idCommande) throws Exception {
		Json json=new Json();
		if(!idCommande.equals("") && !idCommande.equals("null")) {
			return json.getJsonToStringGeneraliser(getCommandeParTableAPartIdCommande(idCommande));
		}
		return json.putObjectInJson(new CommandeParTable()).toString();
	}
	public double getMontantTotal(String idCommande) throws Exception {
		CommandeParTable []commandeParTables=getCommandeParTableAPartIdCommande(idCommande);
		int size=commandeParTables.length;
		if(size>0) {
			double total=0;
			for (int i = 0; i < size; i++) {
				total=total+(commandeParTables[i].getPrixTotal()*commandeParTables[i].getNombreDeProduit());
			}
			return total;
		}
		return 0;
	}
	public int getNombrePlatsCommande(String idCommande) throws Exception {
		CommandeParTable []commandeParTables=getCommandeParTableAPartIdCommande(idCommande);
		return commandeParTables.length;
	}
	public String supprimerCommandeParTable(String idCommandeOrIdCommandeParTable,boolean commandeTableOrCommande) throws Exception {
		String message="Il y a une erreur d'information";
		if(!idCommandeOrIdCommandeParTable.equals("")) {
			message=deleteCommandeParTableByIdCommandeOrByIdInsuffisant(idCommandeOrIdCommandeParTable, idCommandeOrIdCommandeParTable, commandeTableOrCommande);
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
}

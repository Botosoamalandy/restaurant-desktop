package com.restaurent.service;

import com.restaurent.dao.CommandeTableDAO;
import com.restaurent.entite.Commande;
import com.restaurent.entite.CommandeTable;
import com.restaurent.entite.Json;

public class CommandeTableService extends CommandeTableDAO {
	public String insertionCommandeTables(String idProduit,String nombres,String prixUnitaires,String commande) {
		String message="";
		if(!idProduit.equals("") && !nombres.equals("") && !prixUnitaires.equals("") && !commande.equals("")) {
			try {
				int nombre=Integer.parseInt(nombres);
				double prixUnitaire=Double.parseDouble(prixUnitaires);
				CommandeService commandeService=new CommandeService();
				Commande commandeobject=commandeService.getCommandeAPartCommande(commande);
				if(!commandeobject.getNumeroCommande().equals("") || commandeobject.getNumeroCommande()!=null) {
					CommandeTable commandeTable=new CommandeTable("nextval('commandeTableSequence')", commandeobject.getIdCommande(), idProduit, nombre, prixUnitaire, 1);
					message=insertionCommandeTable(commandeTable);
				}else {
					message="Cet commande n\'existe pas";
				}
			} catch (Exception e) {
				message=e.getMessage();
			}
		}else {
			message="Il y a une champs vide";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	public String suppressionEtUpdateCommandeTable(String idCommandeTable,String quantite,boolean update) {
		Json json=new Json();
		String message="";
		if(!idCommandeTable.equals("") && !quantite.equals("")) {
			try {
				int quantites=Integer.parseInt(quantite);
				message=supprimerEtUpdateCommandeTable(idCommandeTable, quantites, update);
			} catch (Exception e) {
				message="Le quantite est invalide";
			}
		}else {
			message="Information invalide";
		}
		json.put("message", message);
		return json.toString();
	}
	public String supprimerCommandeTableByNumeroCommande(String numeroCommande) throws Exception {
		Json json=new Json();
		String message="Information invalide";
		if(!numeroCommande.equals("")) {
			CommandeService commandeService=new CommandeService();
			Commande commande=commandeService.getCommandeAPartCommande(numeroCommande);
			if(commande!=null) {
				message=supprimerCommandeTableAPartIdCommande(commande.getIdCommande());
			}else {
				message="La commande n'existe pas";
			}
		}
		json.put("message", message);
		return json.toString();
	}
}

package com.restaurent.service;

import com.restaurent.dao.CommandeDAO;
import com.restaurent.entite.Commande;
import com.restaurent.entite.Dates;
import com.restaurent.entite.FactureCommande;
import com.restaurent.entite.Json;
import com.restaurent.entite.Token;

public class CommandeService extends CommandeDAO{
	public Commande creationNumeroDeCommande(Commande commande) throws Exception {
		Commande []commandes=getAllCommandeDesc();int size=commandes.length;
		if(size>=1) {
			Commande newCommande=commandes[0];
			commande.setNumeroOriginale(newCommande.getNumeroOriginale()+1);
			commande.setNumeroCommande("commande"+commande.getIdUtilisateur()+""+commande.getIdTablesRestaurant()+""+(newCommande.getNumeroOriginale()+1));
			return commande;
		}else {
			commande.setNumeroOriginale(1000);
			commande.setNumeroCommande("commande"+commande.getIdUtilisateur()+""+commande.getIdTablesRestaurant()+"1000");
			return commande;
		}
	}
	public String insertionCommandes(String numeroTable,String tokenUtilisateur) throws Exception {
		String message="";
		if(!numeroTable.equals("") && !tokenUtilisateur.equals("")) {
			Dates dates=new Dates();dates.getNowDate();
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(tokenUtilisateur);
			Commande commande=creationNumeroDeCommande(new Commande("nextval('commandeSequence')", token.getIdUtilisateur(),numeroTable,"",10,1,1, dates));
			message=insertionCommande(commande);
		}else {
			message="Il y a une champs vide";
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString(); 
	}
	public String getMesCommandesActuel(String numeroToken) throws Exception {
		TokenService tokenService=new TokenService();
		Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
		Json json=new Json();
		if(token!=null) {
			return json.getJsonToStringGeneraliser(getMesCommandes(token.getIdUtilisateur()));
		}
		return json.putObjectInJson(new Commande()).toString();
	}
	public String getMesCommandesByIdUtilisateur(String numeroToken) throws Exception {
		TokenService tokenService=new TokenService();
		Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
		Json json=new Json();
		if(token!=null) {
			return json.getJsonToStringGeneraliser(getMesCommandeAPartUtilisateur(token.getIdUtilisateur()));
		}
		return json.putObjectInJson(new Commande()).toString();
	}
	public String getCommandeActuel() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllCommandesNonFermerDescExact());
	}
	public String getVerificationSiCommandeFermerOuPas(String numeroCommande) throws Exception {
		Commande commande=getCommandeAPartCommande(numeroCommande);
		Json json =new Json();String message="not";
		if(commande!=null) {
			if(commande.getFermetureCommande()==1) {
				message="true";
			}else {
				message="false";
			}
		}
		json.put("message",message);
		return json.toString();
	}
	public String updateFermetureCommandes(String numeroCommande) throws Exception {
		Json json=new Json();
		json.put("message",updateFermetureCommande(numeroCommande,11));
		return json.toString();
	}
	public String updateEtatcommandes(String idCommande) throws Exception {
		Json json=new Json();Dates dateFacture=new Dates();dateFacture.getNowDate();
		FactureCommande factureCommande=new FactureCommande("nextval('factureCommandeSequence')", idCommande, 1, dateFacture);
		json.put("message",updateEtatcommandeAndInsertFactureCommande(idCommande,11,factureCommande));
		return json.toString();
	}
	public String getCommandeValides() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getCommandeValider());
	}
	public String getTousLesCommande() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllCommande());
	}
	public String getidCommandeAPartNumeroCommande(String numeroCommande) throws Exception {
		Json json=new Json();
		Commande commande=getCommandeAPartCommande(numeroCommande);
		json.put("idCommande",commande.getIdCommande());
		return json.toString();
	}
	public String getidCommandeByNumeroCommande(String numeroCommande) throws Exception {
		Json json=new Json();
		Commande commande=getCommandeAPartCommande(numeroCommande);
		return json.putObjectInJson(commande).toString();
	}
}

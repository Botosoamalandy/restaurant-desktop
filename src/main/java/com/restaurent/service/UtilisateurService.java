package com.restaurent.service;

import com.restaurent.dao.UtilisateurDAO;
import com.restaurent.entite.Crypte;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Json;
import com.restaurent.entite.Token;
import com.restaurent.entite.Utilisateur;

public class UtilisateurService extends UtilisateurDAO {
	public int definirEtatUtilisateurSiDesktopOuMobile(boolean desktopOrMobile) {
		if(desktopOrMobile) {
			return 1;
		}
		return 0;
	}
	public String login(String nom,String motDePasse,boolean desktopOrMobile) throws Exception{
		Json json=new Json();
		try {
			if(!nom.equals("") && !motDePasse.equals("")) {
				Crypte crypte=new Crypte();
				TokenService tokenService=new TokenService();
				Utilisateur []utilisateurs=getAllUtilisateur();int size=utilisateurs.length;
				for (int i = 0; i < size; i++) {
					String passwords=crypte.decryptagePasswords(utilisateurs[i].getNomUtilisateur(),utilisateurs[i].getMotDePasse());
					if(utilisateurs[i].getNomUtilisateur().equals(nom) && passwords.equals(motDePasse) && utilisateurs[i].getEtatUtilisateur()>definirEtatUtilisateurSiDesktopOuMobile(desktopOrMobile)) {
						return json.putObjectInJson(tokenService.insertionToken(utilisateurs[i].getIdUtilisateur(),utilisateurs[i].getMotDePasse())).toString();
					}
				}
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return json.putObjectInJson(new Token()).toString();
	}
	public String loginAdministrateur(String nom,String motDePasse) throws Exception{
		Json json=new Json();
		try {
			if(!nom.equals("") && !motDePasse.equals("")) {
				Crypte crypte=new Crypte();
				TokenService tokenService=new TokenService();
				Utilisateur []utilisateurs=getAllUtilisateur();int size=utilisateurs.length;
				for (int i = 0; i < size; i++) {
					String passwords=crypte.decryptagePasswords(utilisateurs[i].getNomUtilisateur(),utilisateurs[i].getMotDePasse());
					if(utilisateurs[i].getNomUtilisateur().equals(nom) && passwords.equals(motDePasse) && utilisateurs[i].getEtatUtilisateur()==3) {
						return json.putObjectInJson(tokenService.insertionToken(utilisateurs[i].getIdUtilisateur(),utilisateurs[i].getMotDePasse())).toString();
					}
				}
			}
		}catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return json.putObjectInJson(new Token()).toString();
	}
	public boolean verificationDedoublement(String nom,int etat,String motDePasse) throws Exception {
		Utilisateur []utilisateurs=getAllUtilisateur();int size=utilisateurs.length;
		for (int i = 0; i < size; i++) {
			if(nom.equals(utilisateurs[i].getNomUtilisateur()) && etat==utilisateurs[i].getEtatUtilisateur() && motDePasse.equals(utilisateurs[i].getMotDePasse())) {
				return true;
			}
		}
		return false;
	}
	public String insertionUtilisateurService(String nom,String etat,String motDePasse) {
		Json json=new Json();String message="";
		if(!nom.equals("") && !etat.equals("") && !motDePasse.equals("")) {
			try {
				Crypte crypte=new Crypte();
				String passwords=crypte.cryptagePasswords(nom, motDePasse);
				Dates dateAjout=new Dates();dateAjout.getNowDate();
				int etatUtilisateur=Integer.parseInt(etat);
				if(!verificationDedoublement(nom, etatUtilisateur, passwords)) {
					Utilisateur utilisateur=new Utilisateur("nextval('utilisateurSequence')", nom, passwords, etatUtilisateur, dateAjout);
					message=insertionUtilisateur(utilisateur);
				}else {
					message="Cet(te) utilisateur existe déjà";
				}
			}catch (Exception e) {
				message=e.getMessage();
			}
		}
		json.put("message",message);
		return json.toString();
	}
	public String getListeUtilisateur() throws Exception {
		Json json=new Json();
		return json.getJsonToStringGeneraliser(getAllUtilisateur());
	}
	public String setMotDePasseUtilisateur(String motDePasse) {
		String resultat="";char []passwords=motDePasse.toCharArray();
		int size=passwords.length,size2=(int)(size/2);
		for (int i = 0; i < size; i++) {
			if(i>=(size2-1)) {
				resultat=resultat+"*";
			}else {
				resultat=resultat+passwords[i];
			}
		}
		return resultat;
	}
	public String getInformationUtilisateur(String numeroToken) throws Exception {
		Json json=new Json();
		if(!numeroToken.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
			if(token!=null) {
				Utilisateur utilisateur=getUtilisateurAPartIdUtilisateur(token.getIdUtilisateur());
				json.put("nom",utilisateur.getNomUtilisateur());
				json.put("etat",utilisateur.getEtatUtilisateur());
				json.put("date",utilisateur.getDateAjout().getDatesComplet());
				Crypte crypte=new Crypte();
				json.put("password",crypte.decryptagePasswords(utilisateur.getNomUtilisateur(),utilisateur.getMotDePasse()));
				json.put("motDePasse",setMotDePasseUtilisateur(crypte.decryptagePasswords(utilisateur.getNomUtilisateur(),utilisateur.getMotDePasse())));
			}
		}
		return json.toString();
	}
	public String updateUtilisateur(String nouveauNom,String nouveauMotDePasse,String numeroToken,String motDePasseActuel) throws Exception {
		String message="Il y a une erreur d'information";
		if(!nouveauNom.equals("") && !nouveauMotDePasse.equals("") && !numeroToken.equals("") && !motDePasseActuel.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
			if(token!=null) {
				Crypte crypte=new Crypte();
				Utilisateur utilisateur=getUtilisateurAPartIdUtilisateur(token.getIdUtilisateur());
				String passwords=crypte.decryptagePasswords(utilisateur.getNomUtilisateur(),utilisateur.getMotDePasse());
				if((!utilisateur.getNomUtilisateur().equals(nouveauNom) && passwords.equals(motDePasseActuel)) || (!passwords.equals(nouveauMotDePasse) && passwords.equals(motDePasseActuel)) ) {
					message=updateInformationUtilisateur(nouveauNom,crypte.cryptagePasswords(nouveauNom, nouveauMotDePasse),utilisateur.getIdUtilisateur());
				}else {
					message="Il n'y a pas de changement d'information ou votre mot de passe est incorrect";
				}
			}
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	public String motDePasseOublier(String nom,String dateAjout,String nouveauMotDePasse) throws Exception {
		String message="Il y a une erreur d'information";
		if(!nom.equals("") && !dateAjout.equals("") && !nouveauMotDePasse.equals("")) {
			Utilisateur utilisateur=getUtilisateurAPartNomUtilisateur(nom);
			try {
				Dates ajout=new Dates(); ajout.setDates(dateAjout);
				if(utilisateur!=null && utilisateur.getNomUtilisateur().equals(nom) && ajout.getDates().equals(utilisateur.getDateAjout().getDates())) {
					Crypte crypte=new Crypte();
					String passwords=crypte.cryptagePasswords(utilisateur.getNomUtilisateur(), nouveauMotDePasse);
					System.out.println("nom :"+nom+"="+utilisateur.getNomUtilisateur()+"+ id : "+utilisateur.getIdUtilisateur()+" MDP: "+nouveauMotDePasse+" => "+passwords);
					message=updateMotDePasseUtilisateur(passwords,utilisateur.getIdUtilisateur());
				}else if(!ajout.getDates().equals(utilisateur.getDateAjout().getDates())){
					message="Votre date d'ajout ne corresponds pas à cet(te) utilisateur";
				}else {
					message="Le nom d'utilisateur n'existe pas";
				}
			} catch (Exception e) {
				message=""+e.getMessage();
			}
		}
		Json json=new Json();
		json.put("message",message);
		return json.toString();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

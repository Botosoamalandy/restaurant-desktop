package com.restaurent.service;

import com.restaurent.dao.ProduitCategorieDAO;
import com.restaurent.entite.Json;

public class ProduitCategorieService extends ProduitCategorieDAO {
	Json json=new Json();
	
	public String getProduitCategorieToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getAllProduitCategorie());
	}
	public String getProduitCategoriePlatsDuJourToFormatJson() throws Exception {
		return json.getJsonToStringGeneraliser(getProduitCategoriePlatsDuJour());
	}
}

package com.restaurent.service;

import com.restaurent.dao.InsuffisantDao;
import com.restaurent.entite.Dates;
import com.restaurent.entite.Insuffisant;
import com.restaurent.entite.InsuffisantProduit;
import com.restaurent.entite.Json;
import com.restaurent.entite.Token;

public class InsuffisantService extends InsuffisantDao {
	
	public String inserteInsuffisant(String idProduit,String idCommande,String idUtilisateur,String numeroCommande) throws Exception {
		String message="Il y a une erreur d'information";
		if(!idProduit.equals("") && !idCommande.equals("") && !idUtilisateur.equals("") && !numeroCommande.equals("")) {
			Dates dateInsuffisant=new Dates();dateInsuffisant.getNowDate();
			InsuffisantProduitService insuffisantProduitService=new InsuffisantProduitService();
			InsuffisantProduit []insuffisantProduits=insuffisantProduitService.getInsuffisantProduitServiceByIdProduit(idProduit, idCommande);
			Insuffisant insuffisant=new Insuffisant("nextval('insuffisantSequence')", idUtilisateur, idCommande, numeroCommande, insuffisantProduits.length, 1, dateInsuffisant);
			message=insertInsuffisantEtProduit(insuffisant, insuffisantProduits);
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
	public String getListeInsuffisantByIdUtilisateur(String numeroToken) throws Exception {
		Json json=new Json();
		if(!numeroToken.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
			if(token!=null) {
				return json.getJsonToStringGeneraliser(getInsuffisantAPartIdUtilisateur(token.getIdUtilisateur()));
			}
		}
		return json.putObjectInJson(new Insuffisant()).toString();
	}
	public String getCompterInsuffisantByIdUtilisateur(String numeroToken) throws Exception {
		Json json=new Json();
		int count=0;
		if(!numeroToken.equals("")) {
			TokenService tokenService=new TokenService();
			Token token=tokenService.getTokenAPartNumeroTokens(numeroToken);
			if(token!=null) {
				count=getInsuffisantAPartIdUtilisateur(token.getIdUtilisateur()).length;
			}
		}
		json.put("count", count);
		return json.toString();
	}
	public String supprimerInsuffisant(String idInsuffisant,String idCommande) throws Exception {
		String message="Il y a une erreur d'information";
		if(!idInsuffisant.equals("") && !idCommande.equals("") ) {
			CommandeParTableService commandeParTableService=new CommandeParTableService();
			commandeParTableService.supprimerCommandeParTable(idCommande, true);
			message=deleteInsuffiisant(idInsuffisant);
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
	public String supprimerInsuffisantEtModifierCommande(String idInsuffisant,String idCommande) throws Exception {
		String message="Il y a une erreur d'information";
		if(!idInsuffisant.equals("") && !idCommande.equals("") ) {
			message=deleteInsuffiisantAndUpdateCommande(idInsuffisant, idCommande);
		}
		Json json=new Json();
		json.put("message", message);
		return json.toString();
	}
}

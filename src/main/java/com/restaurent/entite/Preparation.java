package com.restaurent.entite;

public class Preparation {
	String idPreparation="";
	String idCommande="";
	String numeroCommande="";
	String numeroTables="";
	int numeroPreparation=0;
	int nombreDePlats=0;
	int etatPreparation=0;
	Dates datePreparation=new Dates();
	public Preparation() {}
	public Preparation(String idPreparation, String idCommande, String numeroCommande, String numeroTables,int numeroPreparation, int nombreDePlats, int etatPreparation, Dates datePreparation) {
		this.idPreparation = idPreparation;
		this.idCommande = idCommande;
		this.numeroCommande = numeroCommande;
		this.numeroTables = numeroTables;
		this.numeroPreparation = numeroPreparation;
		this.nombreDePlats = nombreDePlats;
		this.etatPreparation = etatPreparation;
		this.datePreparation = datePreparation;
	}

	public String getIdPreparation() {
		return idPreparation;
	}
	public void setIdPreparation(String idPreparation) {
		this.idPreparation = idPreparation;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public String getNumeroTables() {
		return numeroTables;
	}
	public void setNumeroTables(String numeroTables) {
		this.numeroTables = numeroTables;
	}
	public int getNumeroPreparation() {
		return numeroPreparation;
	}
	public void setNumeroPreparation(int numeroPreparation) {
		this.numeroPreparation = numeroPreparation;
	}
	public int getNombreDePlats() {
		return nombreDePlats;
	}
	public void setNombreDePlats(int nombreDePlats) {
		this.nombreDePlats = nombreDePlats;
	}
	public int getEtatPreparation() {
		return etatPreparation;
	}
	public void setEtatPreparation(int etatPreparation) {
		this.etatPreparation = etatPreparation;
	}
	public Dates getDatePreparation() {
		return datePreparation;
	}
	public void setDatePreparation(String datePreparation) {
		this.datePreparation.setDates(datePreparation);
	}
	
}

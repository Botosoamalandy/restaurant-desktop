package com.restaurent.entite;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;
import java.util.Vector;
public class Json extends HashMap<String, Object>{
	private static final long serialVersionUID = 4185759267522893546L;
	public static String jsonArray(Json[] jsons) {
		String res = "[";
		for (int i = 0; i < jsons.length; i++) {
			res += jsons[i];
			res += i + 1 < jsons.length ? "," : ""; 
		}
		res += "]";
		return res;
	}
	@Override
	public String toString() {
		String json = "{";
        if (!this.isEmpty()) { 
        	Set<String> set = this.keySet();
        	int index = 0;
        	for (Iterator<String> iterator = set.iterator(); iterator.hasNext();) {
    			String key = iterator.next();
    			if(this.get(key) instanceof Integer || this.get(key) instanceof Long || this.get(key) instanceof Double) {
    				json += "\""+key+"\" : "+this.get(key)+"";
    			}else if(this.get(key) instanceof Dates){
    				Dates dates=(Dates)this.get(key);
    				json += "\""+key+"\" : \""+dates.getDatesComplet()+"\"";
    			}else {
    				if(formatJsonOrNo(this.get(key).toString())) {
    					json += "\""+key+"\" : "+this.get(key)+"";
    				}else {
    					json += "\""+key+"\" : \""+this.get(key)+"\"";
    				}
    			}
    			json += index + 1 < set.size() ? " , " : "}";  
    			index++;
    		}
        	return json;
        } 
        return "{}";
	}
	public String setJsonToStringJson(Vector<Json> jsons) {
		Json []jsons2=new Json[jsons.size()];
		for(int i=0;i<jsons.size();i++) {
			jsons2[i]=jsons.get(i);
		}
		String newJson=jsonArray(jsons2);
		return newJson;
	}
	public String caseT(String mot) {
    	String reste=mot.substring(1);
    	char premierLettreEnChar=mot.charAt(0);
    	String premierLettreEnString=""+premierLettreEnChar;
    	return premierLettreEnString.toUpperCase()+reste;
    }
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Json putObjectInJson(Object object) throws Exception {
		Json json=new Json();
		Field []field=object.getClass().getDeclaredFields();
		Class classe=object.getClass();int size2=field.length;
		for (int j = 0; j < size2; j++) {
			Method methodeTemporaire=classe.getMethod("get"+caseT(field[j].getName()));
			Object objectInvokeTemporaire=methodeTemporaire.invoke(object);
			json.put(field[j].getName(), objectInvokeTemporaire);			
		}
		return json;
	}
	
	public String getJsonToStringGeneraliser(Object []objects) throws Exception{
		int size=objects.length;
		Json jsonTmp=new Json();
		Vector<Json> jsons=new Vector<Json>();
		for (int i = 0; i < size; i++) {
			if(objects[i]!=null) {
				jsons.add(putObjectInJson(objects[i]));
			}
		}
		return jsonTmp.setJsonToStringJson(jsons);
	}
	public String getJsonVectorTableau(Vector<Object[]> vector,String [] values) throws Exception {
		int size=vector.size();
		if(size==values.length) {
			Vector<Json> jsons=new Vector<Json>();
			for (int i = 0; i < size; i++) {
				
				Json json=new Json();json.put(values[i],getJsonToStringGeneraliser(vector.get(i)));
				jsons.add(json);
			}
			return setJsonToStringJson(jsons);
		}
		return "";
	}
	public boolean formatJsonOrNo(String valeur) {
		if(valeur.length()>0) {
			if(valeur.charAt(0)=='[' && valeur.charAt(valeur.length()-1)==']') {
				return true;
			}
		}
		return false;
	}
}

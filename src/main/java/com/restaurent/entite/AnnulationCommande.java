package com.restaurent.entite;

public class AnnulationCommande {
    String idAnnulationCommande="";
	String idCommande="";
	int etatAnnulation=0;
	Dates dateCommande=new Dates();
	public AnnulationCommande() {}
	public AnnulationCommande(String idAnnulationCommande, String idCommande, int etatAnnulation, Dates dateCommande) {
		this.idAnnulationCommande = idAnnulationCommande;
		this.idCommande = idCommande;
		this.etatAnnulation = etatAnnulation;
		this.dateCommande = dateCommande;
	}
	public String getIdAnnulationCommande() {
		return idAnnulationCommande;
	}
	public void setIdAnnulationCommande(String idAnnulationCommande) {
		this.idAnnulationCommande = idAnnulationCommande;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public int getEtatAnnulation() {
		return etatAnnulation;
	}
	public void setEtatAnnulation(int etatAnnulation) {
		this.etatAnnulation = etatAnnulation;
	}
	public Dates getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(String dateCommande) {
		this.dateCommande.setDates(dateCommande);
	}
	
}

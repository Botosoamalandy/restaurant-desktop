package com.restaurent.entite;

public class TablesRestaurant {
	String idTablesRestaurant="";
	String numeroTables="";
	String typeDeTables="";
	public TablesRestaurant() {}
	public TablesRestaurant(String idTablesRestaurant, String numeroTables, String typeDeTables) {
		this.idTablesRestaurant = idTablesRestaurant;
		this.numeroTables = numeroTables;
		this.typeDeTables = typeDeTables;
	}
	public String getIdTablesRestaurant() {
		return idTablesRestaurant;
	}
	public void setIdTablesRestaurant(String idTablesRestaurant) {
		this.idTablesRestaurant = idTablesRestaurant;
	}
	public String getNumeroTables() {
		return numeroTables;
	}
	public void setNumeroTables(String numeroTables) {
		this.numeroTables = numeroTables;
	}
	public String getTypeDeTables() {
		return typeDeTables;
	}
	public void setTypeDeTables(String typeDeTables) {
		this.typeDeTables = typeDeTables;
	}
}

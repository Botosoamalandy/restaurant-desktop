package com.restaurent.entite;

public class UtilisateurSupprimer {
	String idUtilisateurSupprimer="";
	String idUtilisateur="";
	Dates dateDeSuppression=new Dates();
	public UtilisateurSupprimer() {}
	public UtilisateurSupprimer(String idUtilisateurSupprimer, String idUtilisateur, Dates dateDeSuppression) {
		this.idUtilisateurSupprimer = idUtilisateurSupprimer;
		this.idUtilisateur = idUtilisateur;
		this.dateDeSuppression = dateDeSuppression;
	}
	public String getIdUtilisateurSupprimer() {
		return idUtilisateurSupprimer;
	}
	public void setIdUtilisateurSupprimer(String idUtilisateurSupprimer) {
		this.idUtilisateurSupprimer = idUtilisateurSupprimer;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public Dates getDateDeSuppression() {
		return dateDeSuppression;
	}
	public void setDateDeSuppression(String dateDeSuppression) {
		this.dateDeSuppression.setDates(dateDeSuppression);
	}
	
}

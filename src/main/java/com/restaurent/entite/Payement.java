package com.restaurent.entite;

public class Payement {
	String idPayement="";
	String idCommande="";
	double montantPayer=0;
	double montantTotal=0;
	double montantResteClients=0;
	int etatPayement=0;
	Dates datePayement=new Dates();
	public Payement() {}
	
	public Payement(String idPayement, String idCommande, double montantPayer, double montantTotal,double montantResteClients, int etatPayement,Dates datePayement) {
		this.idPayement = idPayement;
		this.idCommande = idCommande;
		this.montantPayer = montantPayer;
		this.montantTotal = montantTotal;
		this.montantResteClients=montantResteClients;
		this.etatPayement = etatPayement;
		this.datePayement = datePayement;
	}

	public String getIdPayement() {
		return idPayement;
	}
	public void setIdPayement(String idPayement) {
		this.idPayement = idPayement;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public double getMontantPayer() {
		return montantPayer;
	}
	public void setMontantPayer(double montantPayer) {
		this.montantPayer = montantPayer;
	}
	public double getMontantTotal() {
		return montantTotal;
	}
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getMontantResteClients() {
		return montantResteClients;
	}

	public void setMontantResteClients(double montantResteClients) {
		this.montantResteClients = montantResteClients;
	}

	public int getEtatPayement() {
		return etatPayement;
	}
	public void setEtatPayement(int etatPayement) {
		this.etatPayement = etatPayement;
	}
	public Dates getDatePayement() {
		return datePayement;
	}
	public void setDatePayement(String datePayement) {
		this.datePayement.setDates(datePayement);
	}
	
}

package com.restaurent.entite;

public class Produit {
	String idProduit="";
	String idCategorie="";
	String nomProduit="";
	double prixUnitaire=0;
	String descriptions="";
	String nomImage="";
	double tva=0;
	double remise=0; 
	int quantite=0;
	public Produit() {}
	public Produit(String idProduit, String idCategorie, String nomProduit, double prixUnitaire, String descriptions,String nomImage, double tva, double remise,int quantite) {
		this.idProduit = idProduit;
		this.idCategorie = idCategorie;
		this.nomProduit = nomProduit;
		this.prixUnitaire = prixUnitaire;
		this.descriptions = descriptions;
		this.nomImage = nomImage;
		this.tva = tva;
		this.remise = remise;
		this.quantite=quantite;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public String getIdCategorie() {
		return idCategorie;
	}
	public void setIdCategorie(String idCategorie) {
		this.idCategorie = idCategorie;
	}
	public String getNomProduit() {
		return nomProduit;
	}
	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}
	public double getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public String getNomImage() {
		return nomImage;
	}
	public void setNomImage(String nomImage) {
		this.nomImage = nomImage;
	}
	public double getTva() {
		return tva;
	}
	public void setTva(double tva) {
		this.tva = tva;
	}
	public double getRemise() {
		return remise;
	}
	public void setRemise(double remise) {
		this.remise = remise;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
}

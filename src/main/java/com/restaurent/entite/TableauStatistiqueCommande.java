package com.restaurent.entite;

public class TableauStatistiqueCommande {
	String numeroCommande="";
	String label="";
	double total=0;
	double annee=0;
	double mois=0;
	double jours=0;
	public TableauStatistiqueCommande() {}
	public TableauStatistiqueCommande(String numeroCommande,String label, double total, double annee, double mois, double jours) {
		this.numeroCommande = numeroCommande;
		this.label=label;
		this.total = total;
		this.annee = annee;
		this.mois = mois;
		this.jours = jours;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public double getAnnee() {
		return annee;
	}
	public void setAnnee(double annee) {
		this.annee = annee;
	}
	public double getMois() {
		return mois;
	}
	public void setMois(double mois) {
		this.mois = mois;
	}
	public double getJours() {
		return jours;
	}
	public void setJours(double jours) {
		this.jours = jours;
	}
	
	
	
}

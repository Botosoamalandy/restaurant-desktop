package com.restaurent.entite;

public class TableauStatistiqueProduit {
	String idProduit="";
	String nomProduit="";
	double total=0;
	public TableauStatistiqueProduit() {}
	public TableauStatistiqueProduit(String idProduit, String nomProduit, double total) {
		this.idProduit = idProduit;
		this.nomProduit = nomProduit;
		this.total = total;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public String getNomProduit() {
		return nomProduit;
	}
	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	
	
}

package com.restaurent.entite;

public class InsuffisantProduit {
	String insuffisantProduit="";
	String idCommande="";
	String idProduit="";
	int quantiteRestant=0;
	int etatInsuffisantProduit=0;
	Dates dateInsuffisant=new Dates();
	public InsuffisantProduit() {}
	public InsuffisantProduit(String insuffisantProduit, String idCommande, String idProduit, int quantiteRestant,int etatInsuffisantProduit, Dates dateInsuffisant) {
		this.insuffisantProduit = insuffisantProduit;
		this.idCommande=idCommande;
		this.idProduit = idProduit;
		this.quantiteRestant = quantiteRestant;
		this.etatInsuffisantProduit = etatInsuffisantProduit;
		this.dateInsuffisant = dateInsuffisant;
	}
	public String getInsuffisantProduit() {
		return insuffisantProduit;
	}
	public void setInsuffisantProduit(String insuffisantProduit) {
		this.insuffisantProduit = insuffisantProduit;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public int getQuantiteRestant() {
		return quantiteRestant;
	}
	public void setQuantiteRestant(int quantiteRestant) {
		this.quantiteRestant = quantiteRestant;
	}
	public int getEtatInsuffisantProduit() {
		return etatInsuffisantProduit;
	}
	public void setEtatInsuffisantProduit(int etatInsuffisantProduit) {
		this.etatInsuffisantProduit = etatInsuffisantProduit;
	}
	public Dates getDateInsuffisant() {
		return dateInsuffisant;
	}
	public void setDateInsuffisant(String dateInsuffisant) {
		this.dateInsuffisant.setDates(dateInsuffisant);
	}
	
}

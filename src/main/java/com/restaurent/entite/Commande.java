package com.restaurent.entite;

public class Commande {
	String idCommande="";
	String idUtilisateur="";
	String idTablesRestaurant="";
	String numeroCommande="";
	int numeroOriginale=0;
	int fermetureCommande=0;
	int etatCommande=0;
	Dates dateCommande=new Dates();
	public Commande() {}
	public Commande(String idCommande, String idUtilisateur, String idTablesRestaurant, String numeroCommande,int numeroOriginale,int fermetureCommande, int etatCommande, Dates dateCommande) {
		this.idCommande = idCommande;
		this.idUtilisateur = idUtilisateur;
		this.idTablesRestaurant = idTablesRestaurant;
		this.numeroCommande = numeroCommande;
		this.numeroOriginale=numeroOriginale;
		this.fermetureCommande = fermetureCommande;
		this.etatCommande = etatCommande;
		this.dateCommande = dateCommande;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getIdTablesRestaurant() {
		return idTablesRestaurant;
	}
	public void setIdTablesRestaurant(String idTablesRestaurant) {
		this.idTablesRestaurant = idTablesRestaurant;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public int getNumeroOriginale() {
		return numeroOriginale;
	}
	public void setNumeroOriginale(int numeroOriginale) {
		this.numeroOriginale = numeroOriginale;
	}
	public int getFermetureCommande() {
		return fermetureCommande;
	}
	public void setFermetureCommande(int fermetureCommande) {
		this.fermetureCommande = fermetureCommande;
	}
	public int getEtatCommande() {
		return etatCommande;
	}
	public void setEtatCommande(int etatCommande) {
		this.etatCommande = etatCommande;
	}
	public Dates getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(String dateCommande) {
		this.dateCommande.setDates(dateCommande);
	}
}

package com.restaurent.entite;

public class Token {
	String idToken="";
    String idUtilisateur="";
    String numeroToken="";
	Dates finDate=new Dates();
	public Token() {}
	public Token(String idToken, String idUtilisateur, String numeroToken, Dates finDate) {
		this.idToken = idToken;
		this.idUtilisateur = idUtilisateur;
		this.numeroToken = numeroToken;
		this.finDate = finDate;
	}
	public String getIdToken() {
		return idToken;
	}
	public void setIdToken(String idToken) {
		this.idToken = idToken;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getNumeroToken() {
		return numeroToken;
	}
	public void setNumeroToken(String numeroToken) {
		this.numeroToken = numeroToken;
	}
	public Dates getFinDate() {
		return finDate;
	}
	public void setFinDate(String finDate) {
		this.finDate.setDates(finDate);
	}

	
	
}

package com.restaurent.entite;

public class FactureCommande {
	String idFactureCommandeTable="";
	String idCommande="";
	int etatFacture=0;
	Dates dateFacture=new Dates();
	public FactureCommande() {}
	public FactureCommande(String idFactureCommandeTable, String idCommande,int etatFacture,Dates dateFacture) {
		this.idFactureCommandeTable = idFactureCommandeTable;
		this.idCommande = idCommande;
		this.etatFacture = etatFacture;
		this.dateFacture = dateFacture;
	}
	public String getIdFactureCommandeTable() {
		return idFactureCommandeTable;
	}
	public void setIdFactureCommandeTable(String idFactureCommandeTable) {
		this.idFactureCommandeTable = idFactureCommandeTable;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public int getEtatFacture() {
		return etatFacture;
	}
	public void setEtatFacture(int etatFacture) {
		this.etatFacture = etatFacture;
	}
	public Dates getDateFacture() {
		return dateFacture;
	}
	public void setDateFacture(String dateFacture) {
		this.dateFacture.setDates(dateFacture);
	}
	
}

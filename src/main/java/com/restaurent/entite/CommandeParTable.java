package com.restaurent.entite;

public class CommandeParTable {
	String idCommande="";
	String idUtilisateur="";
	String nomUtilisateur="";
	String motDePasse="";
	int etatUtilisateur=0;
	Dates dateAjout=new Dates();
	String idTablesRestaurant="";
	String numeroTables="";
	String typeDeTables="";
	String numeroCommande="";
	int numeroOriginale=0;
	int fermetureCommande=0;
	int etatCommande=0;
	Dates dateCommande=new Dates();
	String idCommandeTable="";
	String idProduit="";
	String idCategorie="";
	String categorie="";
	String nomProduit="";
	double prixUnitaire=0;
	String descriptions="";
	String nomImage="";
	double tva=0;
	double remise=0;
	int quantite=0;
	int nombreDeProduit=0;
	double prixTotal=0;
	int etatCommandeTable=0;
	public CommandeParTable() {}
	public CommandeParTable(String idCommande, String idUtilisateur, String nomUtilisateur, String motDePasse,int etatUtilisateur, Dates dateAjout, String idTablesRestaurant, String numeroTables, String typeDeTables,String numeroCommande, int numeroOriginale, int fermetureCommande, int etatCommande, Dates dateCommande,String idCommandeTable, String idProduit, String idCategorie, String categorie, String nomProduit,double prixUnitaire, String descriptions, String nomImage, double tva, double remise,int quantite, int nombreDeProduit,double prixTotal, int etatCommandeTable) {
		this.idCommande = idCommande;
		this.idUtilisateur = idUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
		this.etatUtilisateur = etatUtilisateur;
		this.dateAjout = dateAjout;
		this.idTablesRestaurant = idTablesRestaurant;
		this.numeroTables = numeroTables;
		this.typeDeTables = typeDeTables;
		this.numeroCommande = numeroCommande;
		this.numeroOriginale = numeroOriginale;
		this.fermetureCommande = fermetureCommande;
		this.etatCommande = etatCommande;
		this.dateCommande = dateCommande;
		this.idCommandeTable = idCommandeTable;
		this.idProduit = idProduit;
		this.idCategorie = idCategorie;
		this.categorie = categorie;
		this.nomProduit = nomProduit;
		this.prixUnitaire = prixUnitaire;
		this.descriptions = descriptions;
		this.nomImage = nomImage;
		this.tva = tva;
		this.remise = remise;
		this.quantite=quantite;
		this.nombreDeProduit = nombreDeProduit;
		this.prixTotal = prixTotal;
		this.etatCommandeTable = etatCommandeTable;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public int getEtatUtilisateur() {
		return etatUtilisateur;
	}
	public void setEtatUtilisateur(int etatUtilisateur) {
		this.etatUtilisateur = etatUtilisateur;
	}
	public Dates getDateAjout() {
		return dateAjout;
	}
	public void setDateAjout(String dateAjout) {
		this.dateAjout.setDates(dateAjout);
	}
	public String getIdTablesRestaurant() {
		return idTablesRestaurant;
	}
	public void setIdTablesRestaurant(String idTablesRestaurant) {
		this.idTablesRestaurant = idTablesRestaurant;
	}
	public String getNumeroTables() {
		return numeroTables;
	}
	public void setNumeroTables(String numeroTables) {
		this.numeroTables = numeroTables;
	}
	public String getTypeDeTables() {
		return typeDeTables;
	}
	public void setTypeDeTables(String typeDeTables) {
		this.typeDeTables = typeDeTables;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public int getNumeroOriginale() {
		return numeroOriginale;
	}
	public void setNumeroOriginale(int numeroOriginale) {
		this.numeroOriginale = numeroOriginale;
	}
	public int getFermetureCommande() {
		return fermetureCommande;
	}
	public void setFermetureCommande(int fermetureCommande) {
		this.fermetureCommande = fermetureCommande;
	}
	public int getEtatCommande() {
		return etatCommande;
	}
	public void setEtatCommande(int etatCommande) {
		this.etatCommande = etatCommande;
	}
	public Dates getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(String dateCommande) {
		this.dateCommande.setDates(dateCommande);
	}
	public String getIdCommandeTable() {
		return idCommandeTable;
	}
	public void setIdCommandeTable(String idCommandeTable) {
		this.idCommandeTable = idCommandeTable;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public String getIdCategorie() {
		return idCategorie;
	}
	public void setIdCategorie(String idCategorie) {
		this.idCategorie = idCategorie;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public String getNomProduit() {
		return nomProduit;
	}
	public void setNomProduit(String nomProduit) {
		this.nomProduit = nomProduit;
	}
	public double getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public String getNomImage() {
		return nomImage;
	}
	public void setNomImage(String nomImage) {
		this.nomImage = nomImage;
	}
	public double getTva() {
		return tva;
	}
	public void setTva(double tva) {
		this.tva = tva;
	}
	public double getRemise() {
		return remise;
	}
	public void setRemise(double remise) {
		this.remise = remise;
	}
	public int getNombreDeProduit() {
		return nombreDeProduit;
	}
	public void setNombreDeProduit(int nombreDeProduit) {
		this.nombreDeProduit = nombreDeProduit;
	}
	public double getPrixTotal() {
		return prixTotal;
	}
	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}
	public int getEtatCommandeTable() {
		return etatCommandeTable;
	}
	public void setEtatCommandeTable(int etatCommandeTable) {
		this.etatCommandeTable = etatCommandeTable;
	}
	public int getQuantite() {
		return quantite;
	}
	public void setQuantite(int quantite) {
		this.quantite = quantite;
	}
	
}

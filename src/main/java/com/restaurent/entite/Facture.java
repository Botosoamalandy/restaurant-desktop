package com.restaurent.entite;

public class Facture {
    String idFacture="";
    String idCommande="";
	double montantPayer=0;
	double montantTotal=0;
	double montantResteClients=0;
	int etatPayement=0;
	Dates datePayement=new Dates();
	int etatFacturePayement=0;
	Dates dateFacturePayement=new Dates();
	public Facture() {}
	public Facture(String idFacture, String idCommande, double montantPayer, double montantTotal,double montantResteClients, int etatPayement, Dates datePayement, int etatFacturePayement,Dates dateFacturePayement) {
		this.idFacture = idFacture;
		this.idCommande = idCommande;
		this.montantPayer = montantPayer;
		this.montantTotal = montantTotal;
		this.montantResteClients = montantResteClients;
		this.etatPayement = etatPayement;
		this.datePayement = datePayement;
		this.etatFacturePayement = etatFacturePayement;
		this.dateFacturePayement = dateFacturePayement;
	}
	public String getIdFacture() {
		return idFacture;
	}
	public void setIdFacture(String idFacture) {
		this.idFacture = idFacture;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public double getMontantPayer() {
		return montantPayer;
	}
	public void setMontantPayer(double montantPayer) {
		this.montantPayer = montantPayer;
	}
	public double getMontantTotal() {
		return montantTotal;
	}
	public void setMontantTotal(double montantTotal) {
		this.montantTotal = montantTotal;
	}
	public double getMontantResteClients() {
		return montantResteClients;
	}
	public void setMontantResteClients(double montantResteClients) {
		this.montantResteClients = montantResteClients;
	}
	public int getEtatPayement() {
		return etatPayement;
	}
	public void setEtatPayement(int etatPayement) {
		this.etatPayement = etatPayement;
	}
	public Dates getDatePayement() {
		return datePayement;
	}
	public void setDatePayement(String datePayement) {
		this.datePayement.setDates(datePayement);
	}
	public int getEtatFacturePayement() {
		return etatFacturePayement;
	}
	public void setEtatFacturePayement(int etatFacturePayement) {
		this.etatFacturePayement = etatFacturePayement;
	}
	public Dates getDateFacturePayement() {
		return dateFacturePayement;
	}
	public void setDateFacturePayement(String dateFacturePayement) {
		this.dateFacturePayement.setDates(dateFacturePayement);
	}
	
}

package com.restaurent.entite;

public class PlatsDuJour {
	String idPlatsDuJour="";
	String idProduit="";
	String idUtilisateur="";
	String categorie="";
	double prixUnitaire=0;
	String descriptions="";
	Dates datePlatsDuJour=new Dates();
	public PlatsDuJour() {}
	public PlatsDuJour(String idPlatsDuJour, String idProduit, String idUtilisateur, String categorie,double prixUnitaire, String descriptions, Dates datePlatsDuJour) {
		this.idPlatsDuJour = idPlatsDuJour;
		this.idProduit = idProduit;
		this.idUtilisateur = idUtilisateur;
		this.categorie = categorie;
		this.prixUnitaire = prixUnitaire;
		this.descriptions = descriptions;
		this.datePlatsDuJour = datePlatsDuJour;
	}
	public String getIdPlatsDuJour() {
		return idPlatsDuJour;
	}
	public void setIdPlatsDuJour(String idPlatsDuJour) {
		this.idPlatsDuJour = idPlatsDuJour;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	public double getPrixUnitaire() {
		return prixUnitaire;
	}
	public void setPrixUnitaire(double prixUnitaire) {
		this.prixUnitaire = prixUnitaire;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public Dates getDatePlatsDuJour() {
		return datePlatsDuJour;
	}
	public void setDatePlatsDuJour(String datePlatsDuJour) {
		this.datePlatsDuJour.setDates(datePlatsDuJour);
	}
	
}

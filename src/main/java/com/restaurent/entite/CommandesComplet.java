package com.restaurent.entite;

public class CommandesComplet {
	String idCommande="";
	String idUtilisateur="";
	String nomUtilisateur="";
	String motDePasse="";
	int etatUtilisateur=0;
	Dates dateAjout=new Dates();
	String idTablesRestaurant="";
	String numeroTables="";
	String typeDeTables="";
	String numeroCommande="";
	int numeroOriginale=0;
	int fermetureCommande=0;//11
	int etatCommande=0;//1
	Dates dateCommande=new Dates();
	public CommandesComplet() {}
	public CommandesComplet(String idCommande, String idUtilisateur, String nomUtilisateur, String motDePasse,int etatUtilisateur, Dates dateAjout, String idTablesRestaurant, String numeroTables, String typeDeTables,String numeroCommande,int numeroOriginale, int fermetureCommande, int etatCommande, Dates dateCommande) {
		this.idCommande = idCommande;
		this.idUtilisateur = idUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
		this.etatUtilisateur = etatUtilisateur;
		this.dateAjout = dateAjout;
		this.idTablesRestaurant = idTablesRestaurant;
		this.numeroTables = numeroTables;
		this.typeDeTables = typeDeTables;
		this.numeroCommande = numeroCommande;
		this.numeroOriginale=numeroOriginale;
		this.fermetureCommande = fermetureCommande;
		this.etatCommande = etatCommande;
		this.dateCommande = dateCommande;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public int getEtatUtilisateur() {
		return etatUtilisateur;
	}
	public void setEtatUtilisateur(int etatUtilisateur) {
		this.etatUtilisateur = etatUtilisateur;
	}
	public Dates getDateAjout() {
		return dateAjout;
	}
	public void setDateAjout(String dateAjout) {
		this.dateAjout.setDates(dateAjout);
	}
	public String getIdTablesRestaurant() {
		return idTablesRestaurant;
	}
	public void setIdTablesRestaurant(String idTablesRestaurant) {
		this.idTablesRestaurant = idTablesRestaurant;
	}
	public String getNumeroTables() {
		return numeroTables;
	}
	public void setNumeroTables(String numeroTables) {
		this.numeroTables = numeroTables;
	}
	public String getTypeDeTables() {
		return typeDeTables;
	}
	public void setTypeDeTables(String typeDeTables) {
		this.typeDeTables = typeDeTables;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public int getNumeroOriginale() {
		return numeroOriginale;
	}
	public void setNumeroOriginale(int numeroOriginale) {
		this.numeroOriginale = numeroOriginale;
	}
	public int getFermetureCommande() {
		return fermetureCommande;
	}
	public void setFermetureCommande(int fermetureCommande) {
		this.fermetureCommande = fermetureCommande;
	}
	public int getEtatCommande() {
		return etatCommande;
	}
	public void setEtatCommande(int etatCommande) {
		this.etatCommande = etatCommande;
	}
	public Dates getDateCommande() {
		return dateCommande;
	}
	public void setDateCommande(String dateCommande) {
		this.dateCommande.setDates(dateCommande);
	}
	
}

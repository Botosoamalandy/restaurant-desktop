package com.restaurent.entite;

public class Utilisateur {
	String idUtilisateur="";
	String nomUtilisateur="";
	String motDePasse="";
	int etatUtilisateur=0;
	Dates dateAjout=new Dates();
	public Utilisateur() {}
	public Utilisateur(String idUtilisateur, String nomUtilisateur, String motDePasse, int etatUtilisateur,Dates dateAjout) {
		this.idUtilisateur = idUtilisateur;
		this.nomUtilisateur = nomUtilisateur;
		this.motDePasse = motDePasse;
		this.etatUtilisateur = etatUtilisateur;
		this.dateAjout = dateAjout;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getNomUtilisateur() {
		return nomUtilisateur;
	}
	public void setNomUtilisateur(String nomUtilisateur) {
		this.nomUtilisateur = nomUtilisateur;
	}
	public String getMotDePasse() {
		return motDePasse;
	}
	public void setMotDePasse(String motDePasse) {
		this.motDePasse = motDePasse;
	}
	public int getEtatUtilisateur() {
		return etatUtilisateur;
	}
	public void setEtatUtilisateur(int etatUtilisateur) {
		this.etatUtilisateur = etatUtilisateur;
	}
	public Dates getDateAjout() {
		return dateAjout;
	}
	public void setDateAjout(String dateAjout) {
		this.dateAjout.setDates(dateAjout);
	}
	
}

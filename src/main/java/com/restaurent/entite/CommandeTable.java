package com.restaurent.entite;

public class CommandeTable {
	String idCommandeTable="";
	String idCommande="";
	String idProduit="";
	int nombreDeProduit=0;
	double prixTotal=0;
	int etatCommandeTable=0;
	public CommandeTable() {}
	public CommandeTable(String idCommandeTable, String idCommande, String idProduit, int nombreDeProduit,double prixTotal, int etatCommandeTable) {
		this.idCommandeTable = idCommandeTable;
		this.idCommande = idCommande;
		this.idProduit = idProduit;
		this.nombreDeProduit = nombreDeProduit;
		this.prixTotal = prixTotal;
		this.etatCommandeTable = etatCommandeTable;
	}
	public String getIdCommandeTable() {
		return idCommandeTable;
	}
	public void setIdCommandeTable(String idCommandeTable) {
		this.idCommandeTable = idCommandeTable;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getIdProduit() {
		return idProduit;
	}
	public void setIdProduit(String idProduit) {
		this.idProduit = idProduit;
	}
	public int getNombreDeProduit() {
		return nombreDeProduit;
	}
	public void setNombreDeProduit(int nombreDeProduit) {
		this.nombreDeProduit = nombreDeProduit;
	}
	public double getPrixTotal() {
		return prixTotal;
	}
	public void setPrixTotal(double prixTotal) {
		this.prixTotal = prixTotal;
	}
	public int getEtatCommandeTable() {
		return etatCommandeTable;
	}
	public void setEtatCommandeTable(int etatCommandeTable) {
		this.etatCommandeTable = etatCommandeTable;
	}
	
}

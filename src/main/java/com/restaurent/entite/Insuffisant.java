package com.restaurent.entite;

public class Insuffisant {
	String idInsuffisant="";
	String idUtilisateur="";
	String idCommande="";
	String numeroCommande="";
	int nombreDeProduit=0;
	int etatInsuffisant=0;
	Dates dateInsuffisant=new Dates();
	
	public Insuffisant() {}
	public Insuffisant(String idInsuffisant, String idUtilisateur, String idCommande, String numeroCommande, int nombreDeProduit, int etatInsuffisant, Dates dateInsuffisant) {
		this.idInsuffisant = idInsuffisant;
		this.idUtilisateur=idUtilisateur;
		this.idCommande = idCommande;
		this.numeroCommande = numeroCommande;
		this.nombreDeProduit = nombreDeProduit;
		this.etatInsuffisant = etatInsuffisant;
		this.dateInsuffisant = dateInsuffisant;
	}
	
	public String getIdInsuffisant() {
		return idInsuffisant;
	}
	public void setIdInsuffisant(String idInsuffisant) {
		this.idInsuffisant = idInsuffisant;
	}
	public String getIdUtilisateur() {
		return idUtilisateur;
	}
	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}
	public String getIdCommande() {
		return idCommande;
	}
	public void setIdCommande(String idCommande) {
		this.idCommande = idCommande;
	}
	public String getNumeroCommande() {
		return numeroCommande;
	}
	public void setNumeroCommande(String numeroCommande) {
		this.numeroCommande = numeroCommande;
	}
	public int getNombreDeProduit() {
		return nombreDeProduit;
	}
	public void setNombreDeProduit(int nombreDeProduit) {
		this.nombreDeProduit = nombreDeProduit;
	}
	public int getEtatInsuffisant() {
		return etatInsuffisant;
	}
	public void setEtatInsuffisant(int etatInsuffisant) {
		this.etatInsuffisant = etatInsuffisant;
	}
	public Dates getDateInsuffisant() {
		return dateInsuffisant;
	}
	public void setDateInsuffisant(String dateInsuffisant) {
		this.dateInsuffisant.setDates(dateInsuffisant);
	}
	
}
